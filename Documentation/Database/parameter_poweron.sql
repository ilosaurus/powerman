-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2018 at 03:49 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `power_monitoring`
--

-- --------------------------------------------------------

--
-- Table structure for table `parameter_poweron`
--

CREATE TABLE `parameter_poweron` (
  `id` int(11) NOT NULL,
  `parameter` varchar(45) DEFAULT NULL,
  `value` varchar(255) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `desc` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parameter_poweron`
--

INSERT INTO `parameter_poweron` (`id`, `parameter`, `value`, `kode`, `status`, `desc`) VALUES
(1, 'Input Voltage', '0', 'inputvoltage', '0', 'Input Listrik Dari PLN'),
(2, 'Battery Charge', '30', 'batterycharge', '1', 'Kapasitas Persentase Penggunaan Baterai UPS'),
(3, 'UPS Runtime', '50', 'upsruntime', '0', 'Waktu yang dibutuhkan sebelum UPS mati');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `parameter_poweron`
--
ALTER TABLE `parameter_poweron`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `parameter_poweron`
--
ALTER TABLE `parameter_poweron`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
