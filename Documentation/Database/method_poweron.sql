-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2018 at 03:49 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


--
-- Database: `power_monitoring`
--

-- --------------------------------------------------------

--
-- Table structure for table `method_poweron`
--

CREATE TABLE `method_poweron` (
  `id` int(11) NOT NULL,
  `metode` varchar(45) DEFAULT NULL,
  `kode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `method_poweron`
--

INSERT INTO `method_poweron` (`id`, `metode`, `kode`) VALUES
(1, 'SSH Connection', 'ssh'),
(2, 'iLO Connection', 'ilo'),
(3, 'WOL Connection', 'wol');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `method_poweron`
--
ALTER TABLE `method_poweron`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `method_poweron`
--
ALTER TABLE `method_poweron`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;