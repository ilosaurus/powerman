<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model
{

  public function get_data($table, $where = '', $order_by = '', $group_by = '', $limit = '', $offset = '')
  {
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////

    if ($order_by) {
      $field = $order_by[0];
      $condition = $order_by[1];
      $this->db->order_by($field, $condition);
    }
    if ($group_by) {
        $this->db->group_by($group_by);
    }
    if ($limit) {
      $this->db->limit($limit);
    }
    if ($offset) {
      $this->db->offset($offset);
    }

    $query = $this->db->get($table);
		return $query->result();
  }

  public function insert_data($table, $data, $req = FALSE)
  {
    $this->db->insert($table, $data);
    if ($req) {
      return $this->db->insert_id();
    }
  }

  public function update_data($table, $data ,$where = '')
  {
    $this->db->set($data);
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////
    $this->db->update($table);
  }

  public function delete_data($table, $where = '')
  {
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////
    $this->db->delete($table);
  }

  public function cek_data($table, $where = ''){
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////
    $query = $this->db->get($table);
    return $query->num_rows();
  }

  public function get_row($tabel, $row, $where = '', $order_by = '', $group_by = '', $limit = '', $offset = ''){
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////
    if ($order_by) {
      $field = $order_by[0];
      $condition = $order_by[1];
      $this->db->order_by($field, $condition);
    }
    if ($group_by) {
        $this->db->group_by($group_by);
    }
    if ($limit) {
      $this->db->limit($limit);
    }
    if ($offset) {
      $this->db->offset($offset);
    }
    
    $query = $this->db->get($tabel);
    if($query->num_rows() > 0){
      return $query->row()->$row;
    }else{
      return "data tidak ada";
    }
  }

  public function query($query, $num_rows = FALSE){
    $query = $this->db->query($query);

    if($num_rows){
      return $query->num_rows();
    }else{
      return $query->result();
    }
  }

}
