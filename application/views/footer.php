
<!-- Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12 no-padding">
				<div class="col-md-8 footer-widget">
					<h6><span>Perpustakaan B.J. Habibie</span></h6>
					<p align="justify">UPT. Perpustakaan B.J. Habibie Politeknik Negeri Ujung Pandang merupakan unit yang berperan penting dalam mendukung pelaksanaan Tri Darma Perguruan tinggi, yakni pendidikan dan pengajaran, penelitian, dan pengabdian masyarakat di Politeknik. Ketersediaan sumber-sumber informasi, fasilitas, layanan perpustakaan yang memadai dan berkualitas merupakan faktor utama  optimalisasi peran Perpustakaan terhadap pelaksanaan Tri Darma perguruan tinggi. </p>

				</div>

				<?php
					$where = array(array('status','1'));
					$data_link = $this->M_crud->get_data('man_link', $where);
					if ($data_link) {
				?>
				<div class="col-md-4 footer-widget">
					<h6><span>Link</span></h6>
					<ul class="tags-list">
						<?php foreach ($data_link as $data_link) {  ?>
			      <li><a href="//<?php echo $data_link->url ?>"><?php echo $data_link->judul ?></a></li>
			      <?php } ?>
					</ul>
				</div>
				<?php } ?>

<!--
				<div class="col-md-3 footer-widget">
					<h6><span>Recent Twittes</span></h6> <div id="tweets" class="tweet"></div>
				</div>


				<div class="col-md-3 footer-widget">
					<h6><span>Flickr</span></h6>
					<ul id="flickr" class="thumbs"></ul>
				</div>
			-->
			</div>
		</div>
	</div>
</footer>

<!-- Footer -->


<!-- Footer - Copyright -->
<div class="footer-bottom">
	<a class="back-top" href="#"><i class="fa fa-chevron-up"></i></a>
	<div class="container">
		<div class="row-fluid">
			<div class="col-md-6">
				<p>Copyright <?php echo date("Y"); ?> .All Rights Reserved / Powered By <a href="#">UPT Perpustakaan PNUP</a></p>
			</div>
			<div class="col-md-6">
				<ul class="top-contact">
					<li><i class="fa fa-phone"></i>(0411)-585368, 585367, 585368</li>
					<li><i class="fa fa-envelope"></i> pnup@poliupg.ac.id</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- Footer - Copyright -->
</div>

<!-- JavaScript -->

<script src="<?php echo base_url() ?>assets/js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rs-plugin/rs.home.js"></script>
<script src="<?php echo base_url() ?>assets/js/superTabs.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script src="<?php echo base_url() ?>assets/js/jquery.akordeon.js"></script>
<script src="<?php echo base_url() ?>assets/js/jflickrfeed.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/tab.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.isotope.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.mobilemenu.js"></script>
<script src="<?php echo base_url() ?>assets/js/magnific-popup/jquery.magnific-popup.js"></script>
<script src="<?php echo base_url() ?>assets/js/main.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url() ?>assets/tfeed/jquery.tweet.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAJeOwFQDwZmewJS3Yx3wsaVWOAxFEDT8M"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.gmap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/contact.js"></script>

</body>
</html>
