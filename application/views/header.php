<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Perpustakaan B. J. Habibie, Perpus PNUP, Perpustakaan PNUP, Perpustakaan Politeknik Negeri Ujung Pandang, Politeknik Negeri Ujung Pandang" />
	<meta name="description" content="Sistem Informasi Perpustakaan B. J. Habibie PNUP">
	<meta name="author" content="Muhammad Ilham Syarifuddin">

	<title>Perpustakaan BJ Habibie PNUP</title>

	<!-- Mobile Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicons -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/img/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/img/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/img/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/img/apple-touch-icon-144x144.png">
	<!-- Web Fonts  -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100italic,100,300,300italic,400italic,500,700,700italic,900italic,900,500italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Arimo:400,700' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="<?php echo base_url() ?>admin/assets/image/pnup.jpg" />
	<!--[if lt IE 9]>
	<script src="js/libs/respond.min.js"></script>
	<![endif]-->

	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">

	<!-- FontAwesome icons CSS -->
	<link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	<!-- Theme Styles CSS-->
	<link href="<?php echo base_url() ?>assets/css/styles.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/accordion.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/js/owl-carousel/owl.carousel.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/js/owl-carousel/owl.theme.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/js/rs-plugin/css/settings.css" rel="stylesheet" />
	<link href="<?php echo base_url() ?>assets/js/flexslider/flexslider.css" rel="stylesheet">
	<link rel="Stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery_ui.css" />
	<link rel="Stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/superTabs.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/isotope.css">

	<link href="<?php echo base_url() ?>assets/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/datatables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/js/flexslider/jquery.flexslider.js"></script>
<script src="<?php echo base_url() ?>assets/js/owl-carousel/owl.carousel.js"></script>
	<!--[if lt IE 9]>
	<script src="js/libs/html5.js"></script>
	<![endif]-->

</head>
<body>
<div class="body">

<!-- Header -->
<header style="" class="header6">
         <div  style="margin-bottom:28px;" class="col-md-12">
					 <img src="<?php echo base_url() ?>assets/images/PERPUS2_b.png" class="img-responsive logo" style="width:380px"  alt="PERPUS">
         </div>


	 <div style="background-color:#484747; " >
		 <style media="screen">
		 	#topnav a {
		 		color:#ffffff
		 	}
			#topnav  a:hover {
				color:#ffffff;
			}
		 </style>
			<nav style="" id='topnav'>
				 <ul class="top-menu">
						<li class=''>
							 <a style="" href='<?php echo base_url() ?>'><span>Beranda</span></a>
						</li>
						<li class=''>
							 <a href='<?php echo base_url() ?>berita'><span>Berita</span></a>
						</li>
						<li class='has-sub'>
							 <a href='#'><span>Profil</span></a>
							 <ul>
								 <?php
								 $data_profil = $this->M_crud->get_data('man_profil');
								 foreach ($data_profil as $data_profil) {
								 ?>
									<li><a href='<?php echo base_url() ?>profil/p/<?php echo $data_profil->url ?>'><span><?php echo ucwords($data_profil->judul); ?></span></a></li>
								<?php } ?>
							 </ul>
						</li>
						<li class="has-sub">
								<a href='#'><span>Layanan</span></a>
								<ul>
									<li><a href="./shop.html">OPAC </a></li>
									<?php
									 $data_layanan = $this->M_crud->get_data('man_layanan');
									 foreach ($data_layanan as $data_layanan) {
									?>
									<li><a href="<?php echo base_url() ?>layanan/p/<?php echo $data_layanan->url ?>"><?php echo ucwords($data_layanan->judul); ?></a></li>
									<?php } ?>
								</ul>
						</li>

						<li class="has-sub">
							 <a href='#'><span>Corner</span></a>
							 <ul>
								 <?php
								 $data_corner = $this->M_crud->get_data('man_corner');
								 foreach ($data_corner as $data_corner) {
								 ?>
									<li><a href="<?php echo base_url() ?>corner/p/<?php echo $data_corner->url ?>"><?php echo ucwords($data_corner->judul) ?></a></li>
									<?php } ?>
							 </ul>
						</li>

						<li class="has-sub">
							 <a href='#'><span>E-Journal</span></a>
							 <ul>
								 <?php
								 $data_ejournal = $this->M_crud->get_data('man_ejournal');
								 foreach ($data_ejournal as $data_ejournal) {
								 ?>
									<li><a target="_blank" href="//<?php echo $data_ejournal->url ?>"><?php echo $data_ejournal->judul ?></a></li>
								<?php } ?>
							 </ul>
						</li>

						<li class=''>
							 <a href='<?php echo base_url() ?>gallery'><span>Galeri</span></a>
						</li>

						<li class=''>
							 <a href='<?php echo base_url() ?>download'><span>Download</span></a>
						</li>

						<li class=''>
							 <a target="_blank" href='<?php echo base_url() ?>kontak'><span>Kontak Kami</span></a>
						</li>
				 </ul>
			</nav>

	 </div>

</header>
<!-- Header -->
