<div class="col-md-4 sidebar">
  <?php
    $where = array(array('status','1'));
    $data_profil = $this->M_crud->get_data('man_profil', $where);
    if ($data_profil) {
  ?>
  <div class="side-widget">
    <h5><span>Profil</span></h5>
    <ul class="category">
      <?php foreach ($data_profil as $data_profil) {  ?>
      <li><a href="<?php echo base_url().'profil/p/'.$data_profil->url ?>"><?php echo $data_profil->judul ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>

  <?php
    $where = array(array('status','1'));
    $data_ejournal = $this->M_crud->get_data('man_ejournal', $where);
    if ($data_ejournal) {
  ?>
  <div class="clear"></div>
  <div class="space20"></div>
  <div class="side-widget">
    <h5><span>E-Journal</span></h5>
    <ul class="category">
      <?php foreach ($data_ejournal as $data_ejournal) {  ?>
      <li><a href="//<?php echo $data_ejournal->url ?>"><?php echo $data_ejournal->judul ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>

  <?php
    $where = array(array('status','1'));
    $data_link = $this->M_crud->get_data('man_link', $where);
    if ($data_link) {
  ?>
  <div class="clear"></div>
  <div class="space20"></div>
  <div class="side-widget">
    <h5><span>Link </span></h5>
    <ul class="category">
      <?php foreach ($data_link as $data_link) {  ?>
      <li><a href="//<?php echo $data_link->url ?>"><?php echo $data_link->judul ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>

  <div class="clear"></div>
  <div class="space20"></div>

  <div class="side-widget">
    <h5><span>Kutipan</span></h5>
    <blockquote><?php echo ucwords('Salah satu kunci kebahagiayaan adalah gunakan uangmu untuk pengalaman buka gunakan uangmu untuk keinginan.') ?></blockquote>
    <cite>B. J. Habibie  </cite>
  </div>

  <div class="clear"></div>
  <div class="space20"></div>


</div>
