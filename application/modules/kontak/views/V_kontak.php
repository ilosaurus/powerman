<br>
<div class="page-head">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Kontak Kami</h3>
            </div>
            <div class="col-md-6">
              <ol class="breadcrumb">
                <li><a href="#">Perpustakaan B.J. Habibie </a></li>
                <li><a href="#">Kontak</a></li>
              </ol>
            </div>
        </div>
    </div>
</div>
<br>
<!-- Page-head -->
<div class="container">
    <div class="row">
        <div class="col-md-12 map-wrap">
            <div class="content-img">
                <div class="map">
                    <div class="gmap">
                        <div id="map_addresses" class="map"></div>
                    </div>
                    <div class="shadow4"></div>
                </div>
                <div class="shadow-left-big"></div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="Section-title"><span>Info Kontak</span></h2>
            <div class="contact-info">
                <div class="space20"></div>
                <h4>Politeknik Negeri Ujung Pandang</h4>
                <h4>Phone: 1.800.555.6789</h4>
                <h4>Fax: 1.800.555.6789</h4>
                <h4><a target="_blank" href="//library.poliupg.ac.id">library.poliupg.ac.id</a></h4>
            </div>
        </div>
    </div>
</div>
<br>


<div class="container home-works">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="section-title"><span>Partner</span></h2>
        </div>
        <div id="client-content" class="owl-carousel">
            <div class="item client-content">
                <img onclick="window.open('http://www.dikti.go.id/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/3_ristekdikti.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://www.poliupg.ac.id')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/6_pnup.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://www.pnri.go.id/beranda/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/4_perpunas.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://lpse.poliupg.ac.id/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/1_lpse.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://elearning.poliupg.ac.id')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/2_elearning.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://umpn.poliupg.ac.id/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/5_umpn.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://pmdk.politeknik.or.id/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/7_pmdk.png" alt=""/>
            </div>

        </div>
    </div>
    <hr>
</div>
