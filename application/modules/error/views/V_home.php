<br>
<!-- Slider -->
 <div class="fullwidthbanner-container" >
	<div class="fullwidthbanner">
		<ul>

      <?php foreach ($data_slide as $data_slide) { ?>
        <li data-transition="random" data-slotamount="7" data-masterspeed="700">
  				<img  src="<?php echo base_url() ?>admin/uploads/gambar_slide/<?php echo $data_slide->gambar ?>" alt="slide" data-fullwidthcentering="true">
          <!--
          <div class="tp-caption large_black randomrotate carousel-caption-inner" data-x="0" data-y="316" data-speed="1500" data-start="1400" data-easing="easeInOutBack">
  					<p>
  					  <?php
                echo character_limiter($data_slide->caption, 150);
              ?>
  					</p>
  				</div>
        -->
          <div class="tp-caption large_black lfl carousel-caption-inner" data-x="-80" data-y="370" data-speed="370" data-start="1100" data-easing="easeInOutQuad">
            <h6><?php echo ucwords($data_slide->judul); ?></h6>
          </div>

  			</li>
      <?php } ?>

		</ul>
	</div>
</div>
<!-- Slider -->
<!-- Services -->
<div class="section">
	<div class="container">
		<div class="col-lg-4 col-md-4">
			<div class="services1">
				<span class="service-ico1"></span>
				<h3>Repositori PNUP</h3>
				<p>Informasi Tentang Disertasi, Tesis, Skripsi, Hasil Penelitian, Majalah dan Jurnal International.</p>
				<a target="_blank" href="//repository.poliupg.ac.id">Kunjungi &rarr;</a>
			</div>
		</div>

		<div class="col-lg-4 col-md-4">
			<div class="services1">
				<span class="service-ico2"></span>
				<h3>OPAC</h3>
				<p>Koleksi Buku, Karya Ilmiah (S1,S2,S3), Makalah, Referensi Bahan Ajar, dan Jurnal Hasil Penelitian.</p>
				<a target="_blank" href="//library.poliupg.ac.id/opac">Kunjungi &rarr;</a>
			</div>
		</div>

		<div class="col-lg-4 col-md-4">
			<div class="services1">
				<span class="service-ico3"></span>
				<h3>Keanggotaan</h3>
				<p>Lorem ipsum dolor slo onsec  designs tueraliquet Morbi nec In Curabitur nel dolor slo onsec designs</p>
				<a target="_blank" href="<?php echo base_url() ?>layanan/p/keanggotaan">Kunjungi &rarr;</a>
			</div>
		</div>
	</div>
</div>
<!-- Services -->


<!-- Area Berita -->
<section id="portfolio" class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <h2 class="section-title"><span>Berita Terbaru </span></h2>
    </div>
    <div class="col-md-12 masonry-blog no-padding">
      <div class="isotope">
        <div class="portfolio-inner">
          <div id="folio" class="isotope col-md-12 no-padding">

            <?php foreach ($data_berita as $data_berita){ ?>

            <div class="folio-item col-md-4 isotope-item">
							<div class="home-blogpost masonry-post ">
								<img style="height: 220px;" class="img-responsive post-thumb" src="<?php echo base_url().'admin/uploads/gambar_sampul/'. $data_berita->gambar_sampul; ?>" alt=""/>
								<div class="post-format-image"><i class="fa fa-picture-o"></i></div>
								<div class="home-blogpost-info3">
									<h4><a href="<?php echo base_url() ?>berita/read/<?php echo $this->Crypt->en($data_berita->id) ?>"><?php echo ucwords($data_berita->judul) ?></a></h4>
									<!-- <div class="meta">Dec 12,2012 | Oleh <a href="#">Admin</a> </div> --><hr>
									<p id="deskripsi_berita"><?php $text = character_limiter($data_berita->deskripsi, 270); echo strip_tags($text);?></p>
								</div>
								<a href="<?php echo base_url() ?>berita/read/<?php echo $this->Crypt->en($data_berita->id) ?>" class="rmore">Selengkapnya &rarr;</a>
							</div>
						</div>

            <?php } ?>


          </div>
          <div class="col-md-12 col-lg-12">
              <center>
                <a href="<?php echo base_url(); ?>berita" class="button-blue  padd30">Selengkapnya</a>
              </center>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
<br><br>
<!--  Area Berita -->

<div class="container home-works">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="section-title"><span>Partner</span></h2>
        </div>
        <div id="client-content" class="owl-carousel">
            <div class="item client-content">
                <img onclick="window.open('http://www.dikti.go.id/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/3_ristekdikti.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://www.poliupg.ac.id')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/6_pnup.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://www.pnri.go.id/beranda/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/4_perpunas.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://lpse.poliupg.ac.id/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/1_lpse.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://elearning.poliupg.ac.id')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/2_elearning.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://umpn.poliupg.ac.id/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/5_umpn.png" alt=""/>
            </div>
            <div class="item client-content">
                <img onclick="window.open('http://pmdk.politeknik.or.id/')" style="width:160px;height:46px" src="<?php echo base_url(); ?>assets/images/7_pmdk.png" alt=""/>
            </div>

        </div>
    </div>
    <hr>
</div>
<!-- Content wrap -->


<!-- Call-to-action Box -->

<!-- Call-to-action Box -->
