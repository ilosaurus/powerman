<br>
<!-- Page-Head -->

<div class="page-head">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3><?php echo $modul; ?></h3>
			</div>
			<div class="col-md-6">
				<ol class="breadcrumb">
					<li>Perpustakaan B.J. Habibie </li>
					<li class="active"><a href="#"><?php echo $modul; ?></a></li>
					<li><a href="#"><?php echo ucwords($title); ?></a></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<!-- Page-Head -->

<div class="space20"></div>


<!-- Blogpost -->
<div class="container">
    <div class="row">
        <div class="col-md-12 no-padding">
          <?php foreach ($data_layanan as $data_layanan) { ?>
            <article class="post col-md-12">
                <div class="post-info">
                    <h3><?php echo ucwords($data_layanan->judul) ?></h3>
                    <hr>
                    <div class="deskripsi">
                      <?php echo $data_layanan->deskripsi; ?>
                    </div>
                </div>


                <div class="post-meta">
                    <div class="meta-right">
                        <div class="meta-info">
													<span><a href="#"><?php echo $this->tanggal_indo(date('Y-m-d', strtotime($waktu))); ?></a></span><em>/</em>
                          <span>Posted by <a href="#">Admin</a></span>
                        </div>
                    </div>
                </div>
            </article>
            <?php } ?>
        </div>
    </div>
</div>


<!-- Blogpost -->

<!-- START NEWS SECTION -->
<!-- <?php
$where = array(array('status','1'));
$order_by = array('upload_date', 'desc');
$data_berita =  $this->M_crud->get_data('man_berita',$where, $order_by,null, 5,null );
if ($data_berita) {
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 no-padding">
			<h2 class="section-title"><span>Berita Terbaru</span></h2>
			<div id="contentslider" class="flexslider">
				<ul class="slides">

						<?php

						foreach ($data_berita as $data_berita) {
						?>

						<div class="col-md-6">
							<div class="home-blogpost2">
								<div class="home-post-date">3<span>December</span></div>
								<div class="home-blogpost-info2">
									<h5><a href="<?php echo base_url() ?>berita/read/<?php echo $this->Crypt->en($data_berita->id) ?>"><?php echo $data_berita->judul; ?></a></h5>
									<p><?php $text = character_limiter($data_berita->deskripsi, 150); echo strip_tags($text); ?></p>
								</div>
							</div>
						</div>

						<?php } ?>

				</ul>
			</div>
		</div>

		<div class="col-md-12 col-lg-12 col-sm-6 col-xs-6">
			<center><h4><a class="btn button-blue" type="button" href="<?php echo base_url() ?>berita">Berita Selengkapnya</a></h4></center>
		</div>
	</div>
</div>
<?php } ?>
<br> -->

<!-- END NEWS SECTION -->
