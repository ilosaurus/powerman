<br>
<!-- Page-Head -->

<div class="page-head">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3><?php echo 'Galeri' ?></h3>
			</div>
			<div class="col-md-6">
				<ol class="breadcrumb">
					<li>Perpustakaan B.J. Habibie </li>
					<li class="active"><a href="#">Galeri</a></li>
					<!-- <li><a href="#"><?php //echo ucwords($title); ?></a></li> -->
				</ol>
			</div>
		</div>
	</div>
</div>

<!-- Page-Head -->

<div class="space20"></div>

<section id="portfolio" class="container no-padding">
    <div class="col-md-12 no-padding">
        <!-- Start Filter -->

        <!-- End Filter -->
    </div>
    <div class="portfolio-inner nport">
				<div id="folio" class="isotope col-md-12 no-padding">
					<?php foreach ($data_gallery as $data_gallery) {  ?>
						<div class="folio-item col-md-3 no-padding isotope-item photos">
                <div class="item works-content">
									<?php $idnya = 'show'.$data_gallery->id; ?>
                    <div  onclick="show_img('<?php echo $idnya ?>')" class="works-overlay">
                        <img  class="img-responsive" src="<?php echo base_url().'admin/uploads/gambar_gallery/'. $data_gallery->gambar; ?>" style="height: 160px;"alt=""/>
                        <div>
                            <div class="zoom">
                                <div class="zoom-info">
                                    <a id="<?php echo $idnya ?>" style="display:none" class="lightbox-popup" href="<?php echo base_url().'admin/uploads/gambar_gallery/'. $data_gallery->gambar; ?>"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5><?php echo $data_gallery->judul ?></h5>
                </div>
            </div>
						<?php } ?>
        </div>
				<script type="text/javascript">function show_img(id) {
					$('body,html').animate({scrollTop: 150 }, 900);
					var idnya = '#'+id;$(idnya).click();
					//$('body,html').animate({scrollTop: 100}, 800);
				}</script>

    </div>
</section>
<!-- Blogpost -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
					<center><?php
				    echo $this->pagination->create_links();
				  ?></center>
        </div>
    </div>
</div>
<!-- Blogpost -->
