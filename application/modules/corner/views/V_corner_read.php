<br>
<!-- Page-Head -->

<div class="page-head">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3><?php echo ucwords($modul); ?></h3>
			</div>
			<div class="col-md-6">
				<ol class="breadcrumb">
					<li>Perpustakaan B.J. Habibie </li>
					<li class="active"><a href="#"><?php echo ucwords($modul);?></a></li>
					<li><a href="#"><?php echo ucwords($title); ?></a></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<!-- Page-Head -->

<div class="space20"></div>


<!-- Blogpost -->
<div class="container">
    <div class="row">
        <div class="col-md-12 no-padding">
          <?php foreach ($data_corner as $data_corner) { ?>
            <article class="post col-md-12">
                <div class="post-info">
                    <h3><?php echo ucwords($data_corner->judul) ?></h3>
                    <hr>
                    <div class="deskripsi">
                      <?php echo $data_corner->deskripsi; ?>
                    </div>
                </div>


                <div class="post-meta">
                    <div class="meta-right">
                        <div class="meta-info">
													<span><a href="#"><?php echo $this->tanggal_indo(date('Y-m-d', strtotime($waktu))); ?></a></span><em>/</em>
                          <span>Posted by <a href="#">Admin</a></span>
                        </div>
                    </div>
                </div>

            </article>

            <?php } ?>
        </div>


    </div>
</div>
<!-- Blogpost -->
