<br>
<!-- Page-Head -->

<div class="page-head">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3><?php echo ucwords($modul); ?></h3>
			</div>
			<div class="col-md-6">
				<ol class="breadcrumb">
					<li>Perpustakaan B.J. Habibie </li>
					<li class="active"><a href="#"><?php echo ucwords($modul); ?></a></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<!-- Page-Head -->

<div class="space20"></div>


<!-- Blogpost -->
<div class="container">
    <div class="row">
        <div class="col-md-12 no-padding">
					<table class="table table-bordered table-hover " id="example" width="100%">
						<thead>
							<tr>
								<th width="70%"><center>File</center></th>
								<th width="25%"><center>Download</center></th>
            </tr>
						</thead>
						<tbody>
							<?php $count = 1; foreach ($data_download as $data_download) { ?>
								<tr>
                	<th><center><?php echo $data_download->judul; ?></center></th>
									<th><center><a type="button" href="<?php echo base_url().'admin/uploads/file_download/'.$data_download->file; ?>" download>Download</a></center></th>
            		</tr>
		          <?php $count++;  } ?>
						</tbody>
					</table>

        </div>
    </div>
</div>
<div class="space20"></div>

<!-- Blogpost -->
<script src="<?php echo base_url() ?>assets/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/datatables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable({
			 paging: false,
			 "bInfo" : false
		});
	});
</script>
