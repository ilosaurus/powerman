<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class download extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->load->library('pagination');
	}

	public function index()
	{
		$where = array(array('status','1'));
		$data['modul'] = 'download';
		$data['title'] = $this->M_crud->get_row('man_download','judul', $where);
		$data['waktu'] = $this->M_crud->get_row('man_download','upload_date', $where);
		$data['data_download'] = $this->M_crud->get_data('man_download', $where);
		$this->load->view('header');
		$this->load->view('V_download', $data);
		$this->load->view('footer');
	}

	public function p($url = null)
	{
		if ($url != null) {
			$where = array(array('url',$url));
			$cek_url = $this->M_crud->cek_data('man_download', $where);
			if ($cek_url) {
				$data['modul'] = 'download';
				$data['title'] = $this->M_crud->get_row('man_download','judul', $where);
				$data['waktu'] = $this->M_crud->get_row('man_download','upload_date', $where);
				$data['data_download'] = $this->M_crud->get_data('man_download', $where);
				$this->load->view('header');
				$this->load->view('V_download_read', $data);
				$this->load->view('recentnews');
				$this->load->view('footer');
			}else {
				redirect(base_url().'error','refresh');
			}
		}else {
			redirect(base_url().'error','refresh');
		}
	}






}
