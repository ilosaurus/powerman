$(document).ready(function(){

$('#map_addresses').gMap({
	address: "Politeknik Negeri Ujung Pandang - Perpustakaan",
	zoom: 15,
	arrowStyle: 2,
	controls: {
       panControl: true,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: true,
        overviewMapControl: false
    },

		markers:[
		{
			address: "Politeknik Negeri Ujung Pandang - Perpustakaan",
			popup: true
		}
		]
	});
	});
