#!/bin/bash


# where the server list sits
#servers="10.0.12.110 10.0.12.120 10.0.12.90 10.0.12.7 10.0.12.180"
servers="10.0.12.110 10.0.12.180 10.0.12.90"

# Function to check if the server responds to ping
function testServer() {
    if ping -c1 $ip>/dev/null 2>&1; then
	return 0
    else
	return 1
    fi
}

for ip in $servers; do
    # halt the remote server
#    ssh root@$ip 'poweroff -r +5 & disown'
   ssh -o "ServerAliveInterval 2" root@$ip poweroff
    count=0
	while testServer $servers; do
        sleep 1; echo -n "."
        count=$(( $count + 1 )) # count up to 30
        if [ "$count" -gt 5 ]; then # if the server did not shutdown in 30 seconds, something is broken
    	    echo -n "Server did not shutdown properly!"
	    break;
	fi
    	done
	echo ""
done
