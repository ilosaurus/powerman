# iLO 3 Configuration

## A. Disable HP Splash Screen
    1. Dengan menekan f9 untuk masuk di config bios
    2. Pilih Advanced Options -> Power-On LOgo
    3. Lalu pilih disabled
    4. **F10** To Exit

## B. Setting iLO 3 Ip Address
    1. Booting dan tekan F8 untuk masuk di setting iLO || Jika sudah mendisabled spalsh screen ditunggu booting dan tekan **Ctrl+S**
    2. Pilih Menu Network di Menu Bar, kemudian pilih menu **DNS/DHCP**
    3. Pilih option Off di DNS Enable untuk menonaktifkan DHCP
    4. Tekan **F1** untuk masuk di menu advancednya dan masukkan Ip Address secara statis
    5. iLO 3 akan di restart dan disimpan

## C. iLO 3 Activation Keys
    1. iLO 3 -> **35SCT-5KKSJ-5GXVD-ZM75K-PW8ZH** || **35SCT5KKSJ5GXVDZM75KPW8ZH**
    2. iLO 2 -> **35DPH-SVSXJ-HGBJN-C7N5R-2SS4W** || **35SCR-RYLML-CBK7N-TD3B9-GGBW2**
    3. iLO 1 -> **35DRP-7B3TX-78VVM-7KX4Y-XS74X** || **247RH-ZPJ8S-7B17D-FCE55-DDD17**
    4. iLO MP -> **32Q8Y-XZVGQ-4SGJB-4KY3R-M9ZBN**

## D. Menggunakan iLO 3 


## E. Dokumentasi iLO Data Center PNUP
    1. Proxmox 39 {ILO 3} || 10.0.12.39 || 10.0.3.39 || {admin_ilo1, admin_39} || {admin_ilo1, admin_39} || {status, sudah di configure}
    2. Proxmox 93 {ILO 3} || 10.0.12.93 || 10.0.3.93 || {admin_ilo2, admin_93} || {admin_ilo2, admin_93} || {status, sudah di configure}
    3. Proxmox 80 {ILO 2} || 10.0.12.80 || 10.0.3.80 || {admin_80} || {admin_80} || Open in Internet Explorer


### Urgently 
    1. Ingat install acpid jika power off lewat ilo not working --> apt-get install acpid, service acpid start
    2. WOL menggunakan eth untuk power control di cek dulu
    3. Untuk iLO 2 access sshnya menggunakan  ssh -o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha1 admin_80@10.0.3.80
    4. Untuk iLo 2 access ssh tanpa password menggunakan -->  sshpass -p "admin_80" ssh -o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha1 admin_80@10.0.3.80
    5. Refrensi access ssh iLO 2 --> http://stdout-dev-null.blogspot.co.id/2018/01/ilo-ssh-received-disconnect-from-xxxx.html
    6. Refrensi WOL --> https://www.howtogeek.com/70374/how-to-geek-explains-what-is-wake-on-lan-and-how-do-i-enable-it/
    7. 


## iLO Latest Firmware 
    http://pingtool.org/latest-hp-ilo-firmwares/
