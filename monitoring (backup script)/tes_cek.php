<?php
	date_default_timezone_set("Asia/Makassar");
	include "koneksi.php";
	include "apc_core.php";

	$cek_par_off = "select * from parameter_poweron where status = '1' ";
	$get_data_old = "select * from ups_info where id = '1' ";
	$result_par_off = mysql_fetch_assoc(mysql_query($cek_par_off));
	$result_data_old = mysql_fetch_assoc(mysql_query($get_data_old));
	
	///////// INISIASI KONDISI PENGUJIAN //////////////////////////
	// $system_test_sql = "select * from system_test where id = '1' ";
	// $result = mysql_query($system_test_sql);
	// while ($row = mysql_fetch_assoc($result)) {
    //     $input_voltage_tes = $row['input_voltage'];
    //     $battery_capacity_tes = $row['battery_capacity'];
    //     $battery_runtime_tes = $row['battery_runtime'];
    //     $current_status_tes = $row['status'];
    // }
	// $status_tes = $current_status_tes; // status
	// $battery_capacity = $battery_capacity_tes; // battery capacity
	// $input_voltage = $input_voltage_tes;
	// $battery_runtime = $battery_runtime_tes;
	///////////////////////////////////////////////////////////////

	// inisialisasi data yang di butuhkan // 
	$status_tes = $status; // comment if testing
	$action_script = '';
	if ($status_tes == "ONBATT") { // kondisi listrik pln mati
		$action_script = "poweroff";
	}else {
		if ($status_tes == "ONLINE") { // kondisi listrik pln hidup
			$action_script = "poweron";
		}else {
			$action_script = NULL;
		}
	}

	////////////////////////////////////////////////////////////////////
	define("input_voltage", (int)hapus_koma($input_voltage)); 
	define("battery_runtime", (int)hapus_koma($battery_runtime));
	define("battery_capacity", (int)hapus_koma($battery_capacity));
	define("current_status", $status_tes);
	define("action_script", $action_script);
	$par_monitor_server;
	$par_last_exec;
	$par_cek_status = 1;
	$array_server_status = [];



	// echo "bcapacity -> ".battery_capacity."\n";
	// echo "input vltoge -> ".input_voltage."\n";;
	// echo "currnt status -> ".current_status."\n";;
	
	// cek_last_execution();
	// cek_parameter();
	// cek_parameter($kode_parameter, $value_parameter); 
	// echo $kode_parameter." - ".$value_parameter;
	// $parameter_now =  cek_parameter(); 
	// echo "\n Status Parameter : "; var_dump($parameter_now);
	// echo "\n Last Exec : "; var_dump(cek_last_execution());
	// // echo battery_capacity."\n";
	// // echo action_script;
	// echo "\n Monitor Server : "; var_dump(monitor_server());
	


	if (cek_last_execution() === TRUE && cek_parameter() === TRUE) {
		
		echo "\n\n-------------------------------\n";
		echo " -- ".date('Y-m-d H:i:s')." -- ";
		echo "\nStatus UPS -> ".$status_tes." ";
		echo "\nUPS Runtime -> ".battery_runtime."";
		echo "\nUPS Capacity -> ".battery_capacity."";
		echo "\nStatus Last Execution -> "; var_dump(cek_last_execution()) ;echo "\n";
		echo "\nStatus Paramater -> "; var_dump(cek_parameter()) ;echo "\n";
		echo "\nkondisi monitor server :" ; var_dump(monitor_server()) ;echo "\n";
		echo "\nSkrip Bisa Dijalankan";
		echo "\nAction Script -> ";var_dump(action_script);
		echo "\n-------------------------------\n\n";
		$query="UPDATE ups_info SET last_execution=NOW() WHERE id = 1 ";
		mysql_query($query);
		// $query2="UPDATE ups_info SET last_execution2=NOW()+ interval 15 minute WHERE id = 1 ";
		// mysql_query($query2);
		if (action_script == 'poweroff') {
			$sql_insert_log_poweroff = "INSERT INTO system_log (log,log_type,datetime) VALUES  ('POWER OFF SCRIPT EXECUTION', 'SCRIPT_STATUS',NOW());";
			mysql_query($sql_insert_log_poweroff);
			exec("php /var/www/monitoring/poweringoff.php >> /var/www/monitoring/debug_poweringoff", $output_tes,  $return_var);

		} else {
			if (battery_capacity == 100) {
				echo "not running auto script, cause ups is 100% capacity";
			} else {
				$sql_insert_log_poweron = "INSERT INTO system_log (log,log_type,datetime) VALUES  ('POWER ON SCRIPT EXECUTION', 'SCRIPT_STATUS',NOW());";
				mysql_query($sql_insert_log_poweron);
				exec("php /var/www/monitoring/poweringon.php >> /var/www/monitoring/debug_poweringon", $output_tes,  $return_var);
			}
		}
		

	} else {
		
		echo "\n\n--------------  |||||  -----------------\n";
		echo " -- ".date('Y-m-d H:i:s')." -- ";
		echo "\nStatus UPS -> ".$status_tes." ";
		echo "\nUPS Runtime -> ".battery_runtime." ";
		echo "\nUPS Capacity -> ".battery_capacity." ";
		echo "\n-------      -----  ----      ------";
		echo "\nStatus Last Execution -> "; var_dump(cek_last_execution()) ;
		echo "\nStatus Paramater -> "; var_dump(cek_parameter()) ;
		echo "\nkondisi monitor server -> " ; var_dump(monitor_server()) ;
		echo "\nSkrip Belum Bisa Dijalankan\n";
		echo "---------------- |||||   --------\n\n";
		// var_dump(action_script);
	}
	


	function hapus_koma($string){
		$koma = '.';
		if (strpos($string, $koma)) {
			$string = substr($string, 0, -2);
		} else {
			$string = $string;
		}
		return $string;
	}

	function monitor_server()
	{
		global $par_monitor_server;
		global $array_server_status;
		global $par_cek_status;

		$sql_cek_server = "select * from server where id_urutan_sop != 0 ";
		$result = mysql_query($sql_cek_server);
		$count = 1;
		while ($row = mysql_fetch_assoc($result)) {
			$array_server_status[$count] =  $row['server_status'];
			$count++;
		}

		if (current_status == "ONBATT") {
			 $par_cek_status = 1;
		} 

		if (current_status == "ONLINE") {
			 $par_cek_status = 0;
		}

		if (in_array($par_cek_status, $array_server_status )) {
			$par_monitor_server = true;
		} else {
			$par_monitor_server = false;
		}
		
		return $par_monitor_server;
	}

	function cek_parameter()
	{
		$kode_pam = '';
		$value_pam = '';
		$parameter_query_off = "select * from parameter_poweron where status = '1' ";
		$parameter_query_on = "select * from parameter_poweron where status_on = '1' ";
		$result_par_off = mysql_fetch_assoc(mysql_query($parameter_query_off));
		$result_par_on = mysql_fetch_assoc(mysql_query($parameter_query_on));

		if (current_status == 'ONBATT') {
			$kode_pam = $result_par_off['kode'];
			$value_pam = $result_par_off['value'];
		} else {
			if (current_status == 'ONLINE') {
				$kode_pam = $result_par_on['kode'];
				$value_pam = $result_par_on['value_on'];
			} else {
				$kode_pam = NULL;
				$value_pam = NULL;
			}
		}
		

		// echo "Kode Parameter - > ".$kode_pam."\n";
		// echo "value Parameter -> ".$value_pam."\n";
		
		// echo "Value Sekarang -> ".battery_capacity."\n";
		// echo "Status Sekarang - >".current_status."\n";
		// $value_now = cek_value($value_pam, battery_capacity, current_status);
		// echo "Valuenya : ";var_dump($value_now);

			if ($kode_pam == 'inputvoltage') {
				$value_now = cek_value($value_pam, input_voltage, current_status);
				if ($value_pam === input_voltage || $value_now !== FALSE) {
					$parameter = TRUE;
				}else{
					$parameter = FALSE;
				}
			}
		
			if ($kode_pam == 'batterycharge') {
				$value_now = cek_value($value_pam, battery_capacity, current_status);
				if ($value_pam === battery_capacity || $value_now !== FALSE) {
					$parameter = TRUE;
				}else{
					$parameter = FALSE;
				}
			}
		
			if ($kode_pam == 'upsruntime') {
				$value_now = cek_value($value_pam, battery_runtime, current_status);
				if ($value_pam === battery_runtime || $value_now !== FALSE) {
					$parameter = TRUE;
				}else{
					$parameter = FALSE;
				}
			}
		
		// echo "Paremeternya : ";var_dump(battery_capacity);echo "value status nya : ";var_dump($value_now);
		return $parameter;
	}

	function cek_value($value_set, $value_now, $status)
	{
		if ($status == 'ONBATT') {
			if ($value_set >= $value_now ) {
				$value_status = TRUE;
			}else {
				$value_status = FALSE;
			}
		} else {
			if ($status == 'ONLINE') {
				if ($value_set <= $value_now) {
					$value_status = TRUE;
				}else {
					$value_status = FALSE;
				}
			}
		}
		return $value_status;
	}

	function cek_last_execution(){
		global $par_last_exec;
		$get_waktu_lama = "select * from ups_info where id = '1' ";
		$result_waktu_lama = mysql_fetch_assoc(mysql_query($get_waktu_lama));
		$selisih = (int)$result_waktu_lama['selisih'];
		$last_exec = $result_waktu_lama['last_execution'];

		$datetime1 = new DateTime();
		$datetime2 = new DateTime("$last_exec");


		$interval = $datetime1->diff($datetime2);

		$interval_year = (int)$interval->y;
		$interval_month = (int)$interval->m;
		$interval_day = (int)$interval->d;
		$interval_hour = (int)$interval->h;
		$interval_minutes = (int)$interval->i;


		if (current_status == "ONBATT") {
			$selisih2 = (int)$result_waktu_lama['poweroff_time'];
	   	} 

	   	if (current_status == "ONLINE") {
			$selisih2 = (int)$result_waktu_lama['poweron_time'];
	   	}
		
		
		
		if ($interval_year === 0 && $interval_month === 0 && $interval_day === 0 && $interval_hour === 0) {
			if ($interval_minutes > $selisih) {
				$execnya = TRUE; // kondisi true ketika interval menit dari selisig di database dgn waktu skrg lebih dari selisih , berart kit bsa mengksekusi script
			} else {
				$execnya = FALSE; // kondisi false ketika selisih dibawah interfal menit, brrti jangan ekseskusi script karnea trlalu cepat
			}
			if ($interval_minutes > $selisih2) {
				$execnya2 = TRUE;
			} else {
				$execnya2 = FALSE;
			}
		}else {

			// $execnya = "diluar jam ini";
			$execnya2 = TRUE;
			$execnya = TRUE; // kondisi false ketika selisih dibawah interfal menit, brrti jangan ekseskusi script karnea trlalu cepat
		}

		
		$monitor_server = monitor_server();
		if ($execnya === TRUE && $monitor_server === TRUE) {
			$par_last_exec2 = TRUE;
		}else {
			$par_last_exec2 = FALSE;
		}

		if ($execnya2 === TRUE || $par_last_exec2 === TRUE) {
			$par_last_exec = TRUE;
		} else {
			$par_last_exec = FALSE;
		}

		return $par_last_exec;
	}


	


?>
