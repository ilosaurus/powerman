<?php
exec('sudo apcaccess', $output, $return_var);
$jumlah_data = count($output);
// echo $jumlah_data;
if ($jumlah_data != 49 && $jumlah_data != 50) {
	echo "error";
}else {
	for($i=0; $i < $jumlah_data; $i++) {
		$array2 = explode(":", $output[$i]); // memisahkan data di antara karakter titik2:
		for ($x=0; $x < 2; $x++) {
			$array3[$i][$x] = $array2[$x];
		}
		//$array4[$i] = $array3[$i][1];  // value dari data, trim digunakan untuk menghapus spasi
		//$array5[$i] = $array3[$i][0]; // tittle dari data
		$title = str_replace(' ', '', $array3[$i][0]);
		$array6[$title] = trim($array3[$i][1]);
	}
	$array6["LOADPCT"] = rtrim($array6["LOADPCT"]," Percent Load Capacity"); 
	$array6["BCHARGE"] =  rtrim($array6["BCHARGE"]," Percent"); 
	$array6["TIMELEFT"] =  rtrim($array6["TIMELEFT"]," Minutes"); 
	$array6["LINEV"] =  rtrim($array6["LINEV"]," Volts");
	$array6["MAXLINEV"] =  rtrim($array6["MAXLINEV"]," Volts");
	$array6["MINLINEV"] =  rtrim($array6["MINLINEV"]," Volts");
	$array6["OUTPUTV"] =  rtrim($array6["OUTPUTV"]," Volts");
	$array6["BATTV"] =  rtrim($array6["BATTV"]," Volts");
	$array6["HITRANS"] =  rtrim($array6["HITRANS"]," Volts");
	$array6["LOTRANS"] =  rtrim($array6["LOTRANS"]," Volts");
	$array6["NOMOUTV"] =  rtrim($array6["NOMOUTV"]," Volts");
	$array6["ENDAPC"] =  str_replace(" ","|",$array6["ENDAPC"]);
	$array6["LINEFREQ"] =  rtrim($array6["LINEFREQ"]," Hz");
	$array6["ITEMP"] =  rtrim($array6["ITEMP"]," C Internal");
	$input_voltage = $array6['LINEV'];
	$output_voltage = $array6['OUTPUTV'];
	$battery_capacity = $array6['BCHARGE'];
	$battery_runtime = $array6['TIMELEFT'];
	$load_capacity = $array6["LOADPCT"];
	$battery_voltage = $array6["BATTV"];
	$line_freq = $array6["LINEFREQ"];
	$ups_temperatur = $array6["ITEMP"];
	$update_date = date('Y-m-d H:i:s');
	$ups_model = $array6["MODEL"];
	$ups_serial_number = $array6["SERIALNO"];
	$status = $array6["STATUS"];
}

?>
