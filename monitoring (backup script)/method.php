<?php 
    function method_ssh_conn($server_address, $server_username, $server_password, $server_ssh_port, $action)
	{
		$script_ssh = 'sudo sshpass -p "'.$server_password.'" ssh '.$server_username.'@'.$server_address.' -p    '.$server_ssh_port.'  "'.$action.'" ' ;
        echo $script_ssh."\n";
        exec($script_ssh, $output,  $return_var);
    }
    
    function method_ssh_ilo($ilo_address, $ilo_username, $ilo_password,$ilo_version, $action)
	{
		if ($ilo_version != '2') { /// ini script untuk ilo 3 & 4
			$script = 'sudo sshpass -p "'.$ilo_password.'" ssh '.$ilo_username.'@'.$ilo_address.' '.$action.'';
			exec($script, $output,  $return_var);
		}else{ // ini script untuk ilo 2
			$script = 'sudo sshpass -p "'.$ilo_password.'" ssh -o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha1 '.$ilo_username.'@'.$ilo_address.' '.$action.' ';
			exec($script, $output,  $return_var);
        }
    }
    
    function method_wol($mac_address_server)
	{
		$script = 'sudo wakeonlan '.$mac_address_server.'';
        exec($script, $output,  $return_var);
	}

	function method_ssh_imm($server_address, $server_username, $server_password,  $action)
	{
		if ($action == 'off') {
			$script = 'sudo sshpass -p "'.$server_password.'" ssh -tt '.$server_username.'@'.$server_address.' < /var/www/monitoring/command_off';
		} else {
			$script = 'sudo sshpass -p "'.$server_password.'" ssh -tt '.$server_username.'@'.$server_address.' < /var/www/monitoring/command_on';
		}
        exec($script, $output,  $return_var);
    }

    function get_cookie($ip_address, $username, $password)
	{
		$url = "https://".$ip_address."/nuova";
		$xml = "<aaaLogin inName='".$username."' inPassword='".$password."' />";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_POST, TRUE );
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml );
		curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE );
	  	$output = curl_exec($ch);
		curl_close($ch);
		$xml_parse = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA); 
		$array = json_decode(json_encode((array)$xml_parse), TRUE);
		return $array['@attributes']['outCookie'];
    }
    
    
    function method_api_ucs($server_address, $action)
	{
		if ($action) {
			// action -> up || down || cycling //
			$ip_address = $server_address; // server address cimc 
			$get_cookie = get_cookie($ip_address, "admin", "password");
			$url = "https://".$ip_address."/nuova";
			$xml = '
				<configConfMo cookie="'.$get_cookie.'" inHierarchical="false" dn="sys/rack-unit-1" > 
					<inConfig>
						<computeRackUnit adminPower="'.$action.'" dn="sys/rack-unit-1">
						</computeRackUnit>
					</inConfig>
				</configConfMo>
			';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_URL, $url );
			curl_setopt($ch, CURLOPT_POST, TRUE );
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE );
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml );
			curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE );
			// curl_setopt($ch, CURLOPT_VERBOSE, TRUE); // DEBUG
			$output = curl_exec($ch);
			curl_close($ch);
			$xml_parse = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA); 
			$array = json_decode(json_encode((array)$xml_parse), TRUE);
			// echo '<pre>';print_r($array);echo '</pre>';
			echo "success";
		} else {
			echo "error";
		}
		
		
	}
?>