<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {
	public function require_login($param = FALSE){
		$session = $this->session->userdata('admin_powerman');
		if($param){
			if(!$session){
				redirect(base_url('login'));
			}
		}else{
			if($session){
				redirect(base_url('home'));
			}
		}
	}

	public function get_session($param = null){
		$session = $this->session->userdata('admin_powerman');
		return $session[$param];
		// if($param){
		// 	if($param == 'status'){
		// 		if($session[$param] == 1){
		// 			return 'Super Admin';
		// 		}else if($session[$param] == 2){
		// 			return 'Admin';
		// 		}else{
		// 			return 'Pegawai';
		// 		}
		// 	}else{
		// 		return $session[$param];
		// 	}
		// }else{
		// 	return $session;
		// }
	}

	public function umur($tgl_lahir)
    {
		$biday = new DateTime($tgl_lahir);
		$today = new DateTime();

		$diff = $today->diff($biday);

		return $diff->y;
    }

    public function umur_bulan($tgl_lahir)
    {
		$biday = new DateTime($tgl_lahir);
		$today = new DateTime();

		$diff = $today->diff($biday);

		return $diff->m;
    }

    public function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2].' '.$bulan[ (int)$split[1] ].' '.$split[0];
	}
}
