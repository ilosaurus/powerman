<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>POWERMAN</title>
    <!-- Favicon-->
    <!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> -->
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url() ?>assets/backend/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="<?php echo base_url() ?>assets/backend/plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="<?php echo base_url() ?>assets/backend/plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="<?php echo base_url() ?>assets/backend/css/style.css" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url() ?>assets/backend/css/themes/all-themes.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/backend/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <!-- JQuery Nestable Css -->
    <link href="<?php echo base_url() ?>assets/backend/plugins/nestable/jquery-nestable.css" rel="stylesheet" />
    <script src="<?php echo base_url() ?>assets/backend/plugins/jquery/jquery.min.js"></script>
    <link href="<?php echo base_url() ?>assets/backend/chartist-js-master/dist/chartist.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/backend/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!-- Justgage area -->
    <script src="<?php echo base_url() ?>assets/backend/justgage/justgage.js"></script>
    <script src="<?php echo base_url() ?>assets/backend/justgage/raphael-2.1.4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/backend/js/jquery.alphanum.js"></script>
    <!-- Jquery Mask -->
    <script src="<?php echo base_url() ?>assets/backend/plugins/jquery/jquery.mask.min.js"></script>
    <!-- <script src="https://diginc.us/examples/js/jquery.sha256.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>


</head>