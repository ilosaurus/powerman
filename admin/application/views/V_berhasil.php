<section role="main" class="content-body">
	<header class="page-header">
		<h2>BERHASIL</h2>
	</header>
	<!-- start: page -->
	<div class="row">
		<div class="col-md-12">
			<section class="panel">
				<header class="panel-heading">

					<h2 class="panel-title">BERHASIL</h2>
				</header>
				<div class="panel-body">

					<div class="alert alert-success" style="text-align: center;">
						Terima Kasih! <strong><?php echo $param ?></strong> berhasil di<?php echo $aksi ?>. <br>
						Halaman akan otomatis kembali dalam 5 detik.
					</div>
				</div>
				<footer class="panel-footer">
					<a href="<?php echo base_url($modul) ?>" class="btn btn-primary">Kembali</a>
				</footer>
			</section>
		</div>
	</div>
</section>
<script type="text/javascript">
	setTimeout(function(){
		location.href = '<?php echo base_url($modul) ?>';
	}, 5000);
</script>