<body class="theme-red">
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->

    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url() ?>">UPS Monitoring</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url() ?>assets/backend/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php 
                        if ($_SESSION['admin_powerman']['name']) {
                            echo $_SESSION['admin_powerman']['name'];
                        } else {
                            echo "Powerman User";
                        }
                    ?>
                    </div>
                    <div class="email">
                    <?php 
                        if ($_SESSION['admin_powerman']['email']) {
                            echo $_SESSION['admin_powerman']['email'];
                        } else {
                            echo "Powermanuseremail";
                        } 
                    ?>
                    </div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="<?php echo base_url() ?>logout"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li <?php if($active_nav){if($active_nav == "home"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                        <a href="<?php echo base_url() ?>">
                            <i class="material-icons">track_changes</i>
                            <span>Monitor UPS</span>
                        </a>
                    </li>
                    <li <?php if($active_nav){if($active_nav == "servers"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                        <a href="<?php echo base_url() ?>servers">
                            <i class="material-icons">dns</i>
                            <span>Server List</span>
                        </a>
                    </li>
                    <li <?php if($active_nav){if($active_nav == "par_on" || $active_nav == "par_off" || $active_nav == "cat" || $active_nav == "method" || $active_nav == "urutan" || $active_nav == "log_autoscript" || $active_nav == "log_notif" || $active_nav == "user"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                        <a href="javascript:void(0);" class="menu-toggle"> 
                            <i class="material-icons">settings</i>
                            <span>Setting</span>
                        </a>
                        <ul class="ml-menu">
                            <li <?php if($active_nav){if($active_nav == "par_on"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                                <a href="<?php echo base_url() ?>powercontrol/parameteron">Parameter Power On</a>
                            </li>
                            <li <?php if($active_nav){if($active_nav == "par_off"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                                <a href="<?php echo base_url() ?>powercontrol/parameteroff">Parameter Power Off</a>
                            </li>
                            <li <?php if($active_nav){if($active_nav == "urutan"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                                <a href="<?php echo base_url() ?>servers/urutan">Urutan Powering Off/On</a>
                            </li>
                            <li <?php if($active_nav){if($active_nav == "cat"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                                <a href="<?php echo base_url() ?>powercontrol/category">Server Category Delay</a>
                            </li>   
                            <li <?php if($active_nav){if($active_nav == "method"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                                <a href="<?php echo base_url() ?>powercontrol/method">Power Control Method</a>
                            </li>  
                            <li <?php if($active_nav){if($active_nav == "log_autoscript"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                                <a href="<?php echo base_url() ?>log/autoscript">Autoscript Log</a>
                            </li>  
                            <li <?php if($active_nav){if($active_nav == "log_notif"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                                <a href="<?php echo base_url() ?>log/notif">SMS Notif Log</a>
                            </li>              
                        </ul>
                    </li>
                    <li <?php if($active_nav){if($active_nav == "grafik"){ ?>  class="active" <?php }else{ ?> class="" <?php }} ?>  >
                        <a href="<?php echo base_url() ?>grafik">
                            <i class="material-icons">timeline</i>
                            <span>Grafik</span>
                        </a>
                    </li>
                    <!-- <li class="">
                        <a href="<?php echo base_url() ?>grafik">
                            <i class="material-icons">home</i>
                            <span>Grafik</span>
                        </a>
                    </li> -->
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <!-- <div class="legal">
                <div class="copyright">
                    &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div> -->
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->

    </section>