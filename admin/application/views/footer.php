<script src="<?php echo base_url() ?>assets/backend/plugins/nestable/jquery.nestable.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/node-waves/waves.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-countto/jquery.countTo.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/raphael/raphael.min.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<script src="<?php echo base_url() ?>assets/backend/js/admin.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/chartjs/Chart.bundle.js"></script>
</body>
</html>

<?php 
    if ($this->session->flashdata('success')) {
        echo "<script> 
        

        swal('Success', 'Import Data Success', 'success');
        
        </script>
        ";
    } else {
        echo "<script> console.log('no flash data'); </script>";
    }
?>