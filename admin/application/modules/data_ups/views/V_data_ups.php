<style>
#g1, #g2, #g3 {
        height:240px;
        display: inline-block;
        margin-top: -70px;
}
</style>
    
<section class="content">
    <!-- <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW TASKS</div>
                        <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">help</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW TICKETS</div>
                        <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">forum</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW COMMENTS</div>
                        <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person_add</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW VISITORS</div>
                        <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
        </div> -->
    <!-- #END# Widgets -->
    <div class="row .m-b-0 .m-t-0">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="card ">
                <div class="bg-red header">
                    <h2>UPS Load</h2>
                </div>
                <div class="body .m-b-0 .m-t-0 .p-t-0 .p-b-0">
                    <center>
                        <div id="g1"></div>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Battery Charge</h2>
                </div>
                <div class="body">
                    <center>
                        <div id="g2"></div>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>UPS Runtime</h2>
                </div>
                <div class="body">
                    <center>
                        <div id="g3"></div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
var g1, g2, g3, g4, datanya;

$(document).ready(function(){
    var g1 = new JustGage({
        id: "g1",
        value: 0,
        decimals : 1,
        min: 0,
        max: 100,
        label: "%",
        relativeGaugeSize: true
    });

    var g2 = new JustGage({
        id: "g2",
        value: 0,
        decimals : 1,
        min: 0,
        max: 100,
        label: "%",
        relativeGaugeSize: true,
        customSectors: [
            { color : "red", lo : 0, hi : 50 },
            { color : "yellow", lo : 51, hi : 70 },
            { color: "#b2d509", lo : 70, hi: 100 }]
    });

    var g3 = new JustGage({
        id: "g3",
        value: 0,
        decimals : 1,
        min: 0,
        max: 240,
        label: "Minutes",
        relativeGaugeSize: true,
        customSectors: [
            { color : "red", lo : 0, hi : 50 },
            { color : "yellow", lo : 51, hi : 70 },
            { color: "#b2d509", lo : 70, hi: 100 }]
    });
    
    setInterval(function(){
        $.get("<?php echo base_url() ?>home/load/", function(data, status){
            datanya = data;
            var data_array = datanya.split("|");
            var TIMELEFT = data_array[0];
            var BCHARGE = data_array[1];
            var LOADPCT = data_array[2];
            var LINEV = data_array[3];
            var OUTPUTV = data_array[4];
            g1.refresh(LOADPCT);
            g2.refresh(BCHARGE);
            g3.refresh(TIMELEFT);
        });
    }, 1200);

});

</script>