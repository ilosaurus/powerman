<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('MENU_AKTIF', 'Data_ups');
date_default_timezone_set('Asia/Makassar');

class Data_ups extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
	}
	
	public function load()
	{
		$data = $this->load_data();
		$someArray = json_decode($data, true);
		echo $someArray['TIMELEFT'].'|'.$someArray['BCHARGE'].'|'.$someArray['LOADPCT'].'|'.$someArray['LINEV'].'|'.$someArray['OUTPUTV'].'|'.$someArray['STATUS'];
		// echo rand(1, 100); 
	}

	public function global_data($parameter)
	{
		$data = $this->load_data();
		$someArray = json_decode($data, true);
		echo $someArray[$parameter];
	}

	public function err()
	{
		$this->load->view('header');
		$this->load->view('V_error');
		$this->load->view('footer');
	}

	public function load_data()
	{
		$url = "http://10.0.12.249:8989/monitoring/apc.php";
		// $url = "http://118.98.121.237:8989/monitoring/apc.php";
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	public function load_data_grafik()
	{
		$where = array(
			array('update_date >= ',"'2018-01-06 10:00:01'"),
			array('and', 'update_date <= ', "'2018-01-06 11:00:01'")
		);

		$data = $this->M_crud->get_data('data_ups', $where);
		$param1 = 'input_voltage';
		$param2 = 'output_voltage, id, input_voltage, status';

		$input_voltage = $this->M_crud->get_data_trafik($param1);
		$output_voltage = $this->M_crud->get_data_trafik($param2);
		$x = 0;
		$koma = '.';
		foreach ($output_voltage as $key => $value) {
			if (strpos($value->input_voltage, $koma)) {
				$value->input_voltage = substr($value->input_voltage, 0, -2);
			} else {
				$value->input_voltage = $value->input_voltage;
			}

			if (strpos($value->output_voltage, $koma)) {
				$value->output_voltage = substr($value->output_voltage, 0, -2);
			} else {
				$value->output_voltage = $value->output_voltage;
			}
		}

		// print_r($output_voltage);
		// $tesarray = (array) $output_voltage;
		echo json_encode($output_voltage );
	}

	public function datagrafik($interval = null)
	{
		if ($interval != null || $interval != 0) {
			$parameter = 'output_voltage, id, input_voltage, update_date';
			$interval = (int)$interval;
			$data_waktu_terakhir = $this->M_crud->get_last_row('data_ups', 'update_date', 'id', 'desc');
			$waktu_terakhir = date( "Y-m-d H:i", strtotime("$data_waktu_terakhir"));
			$waktu_lalu = date( "Y-m-d H:i", strtotime("$waktu_terakhir")-(60*$interval) );
			$dari = $waktu_lalu;
			$ke = $waktu_terakhir;
			$data_grafik = $this->M_crud->get_data_grafik_perwaktu($parameter, $dari, $ke);
			$x = 0;
			$koma = '.';
			$sum_input =0;
			foreach ($data_grafik as $key => $value) {
				$date = $value->update_date;
				$date = strtotime($date);
				$date_fix = date("H:i", $date);
				$value->id = $date_fix;

				if (strpos($value->input_voltage, $koma)) {
					$value->input_voltage = (int)ceil($value->input_voltage);
				} else {
					$value->input_voltage = (int)$value->input_voltage;
				}

				if (strpos($value->output_voltage, $koma)) {
					$value->output_voltage = (int)ceil($value->output_voltage);
				} else {
					$value->output_voltage = (int)$value->output_voltage;
				}
				$x++;
			}

			// $average = $sum_input / $x;
			// echo  ceil($average);
			// echo "<pre>";
			// print_r($data_grafik);
			// echo "</pre>";

			echo json_encode($data_grafik );
		} else {
			echo "0";
		}
		
		
	}

	public function datagrafikload($interval = null)
	{
		if ($interval != null || $interval != 0) {
			$parameter = 'load_capacity, id, battery_voltage, battery_capacity, battery_runtime, update_date';
			$interval = (int)$interval;
			$data_waktu_terakhir = $this->M_crud->get_last_row('data_ups', 'update_date', 'id', 'desc');
			$waktu_terakhir = date( "Y-m-d H:i", strtotime("$data_waktu_terakhir"));
			$waktu_lalu = date( "Y-m-d H:i", strtotime("$waktu_terakhir")-(60*$interval) );
			$dari = $waktu_lalu;
			$ke = $waktu_terakhir;
			$data_grafik = $this->M_crud->get_data_grafik_perwaktu($parameter, $dari, $ke);
			$x = 0;
			$koma = '.';
			$sum_input =0;

			// print_r($data_grafik);
			foreach ($data_grafik as $key => $value) {
				$date = $value->update_date;
				$date = strtotime($date);
				$date_fix = date("H:i", $date);
				$value->id = $date_fix;

				if (strpos($value->battery_runtime, $koma)) {
					$value->battery_runtime = (int)ceil($value->battery_runtime);
				} else {
					$value->battery_runtime = (int)$value->battery_runtime;
				}

				if (strpos($value->battery_capacity, $koma)) {
					$value->battery_capacity = (int)ceil($value->battery_capacity);
				} else {
					$value->battery_capacity = (int)$value->battery_capacity;
				}

				if (strpos($value->battery_voltage, $koma)) {
					$value->battery_voltage = (int)ceil($value->battery_voltage);
				} else {
					$value->battery_voltage = (int)$value->battery_voltage;
				}

				if (strpos($value->load_capacity, $koma)) {
					$value->load_capacity = (int)ceil($value->load_capacity);
				} else {
					$value->load_capacity = (int)$value->load_capacity;
				}
				$x++;
			}

			// $average = $sum_input / $x;
			// echo  ceil($average);
			// echo "<pre>";
			// print_r($data_grafik);
			// echo "</pre>";

			echo json_encode($data_grafik );
		} else {
			echo "0";
		}
		
		
	}

	public function decrypt($string){
		echo $this->Crypt->de($string);
	}

	public function encrypt($string){
		echo $this->Crypt->de($string);
	}

	public function tes_api_proxmox()
	{
		$id_server = '6';
		require(getcwd()."/pve2_api.class.php"); //include API Proxmox
		$where_data_server = array(
			array("id_server", $id_server)
		);
		// error_reporting(0); 
		$data_vm = $this->M_crud->get_data('proxmox', $where_data_server);
		// $vmid = $data_vm[0]->id_vm;
		// $jenis_vm = $data_vm[0]->jenis;
		// $id_server_parent = $data_vm[0]->id_server_parent;
		// $where_data_server_parent = array(
		// 	array("id", $id_server_parent)
		// );
		// $data_server_parent = $this->M_crud->get_data('server', $where_data_server_parent);
		// $address = $data_server_parent[0]->server_address;
		// $username = $data_server_parent[0]->server_user;
		// $password = $data_server_parent[0]->server_password;
		// echo $id_server_parent;
		print_r($data_vm);
		
		// $pve2 = new PVE2_API("".$address."", "".$username."", "pam", "".$password."");
		// if ($pve2->login()) {
		// 	//Get first node name.
		// 	$nodes = $pve2->get_node_list();
		// 	$first_node = $nodes[0];
		// 	$status = 'start';
		// 	error_reporting(0); 
		// 	$pve2->post("/nodes/".$first_node."/".$jenis_vm."/".$vmid."/status/".$status);
		// 	echo "success|Send API Power On VM Proxmox";
		// 	// die(json_encode(array('success' => 1)));	
		// } else {
		// 	echo "error|Error Login API Proxmox";
		// 	// die(json_encode(array('success' => 0, 'message' => 'Login Proxmox Gagal')));
		// }
	}

	public function datatimeoff($dari, $ke)
	{
		$total_hour = array();
		// $dari = "2018-03-24 00:00";
		// $ke = "2018-03-24 23:55";
		$ONBATT = $this->M_crud->get_data_spec_date('data_ups', 'update_date', $dari, $ke, 'ONBATT');
		$ONLINE = $this->M_crud->get_data_spec_date('data_ups', 'update_date', $dari, $ke, 'ONLINE');
		$DATA_ONBATT = json_encode($ONBATT);
		$DATA_ONLINE = json_encode($ONLINE);
		$total_hour['DATA_ONLINE'] = json_decode($DATA_ONLINE, true);
		$total_hour['DATA_ONBATT'] = json_decode($DATA_ONBATT, true);
		echo "<pre>";print_r($total_hour);echo "</pre>";
	}

	public function timeoff($dari,$ke)
	{
		// $dari = "2018-03-24 00:00";
		// $ke = "2018-03-24 23:55";
		$ONBATT = $this->M_crud->get_data_spec_date('data_ups', 'update_date', $dari, $ke, 'ONBATT');
		$ONLINE = $this->M_crud->get_data_spec_date('data_ups', 'update_date', $dari, $ke, 'ONLINE');
		// $DATA_ONBATT = json_encode($ONBATT);
		// $DATA_ONLINE = json_encode($ONLINE);
		$total_hour = array();
		$total_hour['ONLINE'] =  count($ONLINE); // in minutes
		$total_hour['ONBATT'] =  count($ONBATT); // in minutes
		// $total_hour['DATA_ONLINE'] = json_decode($DATA_ONLINE, true);
		// $total_hour['DATA_ONBATT'] = json_decode($DATA_ONBATT, true);
		return $total_hour;
	}

	public function timeoff_all($dari,$ke)
	{
		$data = $this->M_crud->get_data_spec_date_all('data_ups', 'update_date', $dari, $ke);
		return $data;
	}

	public function timediff($dt1,$dt2)
	{
		// $datetime1 = new DateTime("2018-03-24 00:00:00");
		// $datetime2 = new DateTime("2018-03-24 23:55:00");
		$datetime1 = new DateTime($dt1);
		$datetime2 = new DateTime($dt1);
		$interval = $datetime1->diff($datetime2);
		$menit = $interval->h * 60;
		$total_menit = $menit + $interval->i;
		echo $total_menit;
	}


	public function timemonth($bulan, $tahun)
	{
		$hasil = array();
		$day = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
		$date1 = $tahun."-".$bulan."-";
		for ($i=0; $i < $day; $i++) { 
			$x = $i+1;
			$dari = $tahun."-".$bulan."-".$x." 00:00:00";
			$ke = $tahun."-".$bulan."-".$x." 23:55:00";
			$hasil[$i] = $this->timeoff($dari, $ke);
		}
		print_r(json_encode($hasil));
	}

	public function currday()
	{
		$bulan = date('m');
		$tahun = date('Y');
		$hari = date('d');
		$result = array();
		$dari = $tahun."-".$bulan."-".$hari." 00:00:00";
		$ke = $tahun."-".$bulan."-".$hari." 23:55:00";
		$hasil = $this->timeoff($dari, $ke);
		$result['ONLINE'] = $hasil['ONLINE'] * 5;
		$result['ONBATT'] = $hasil['ONBATT'] * 5;
		// $result['DATA_ONLINE'] = $hasil['DATA_ONLINE'];
		// $result['DATA_ONBATT'] = $hasil['DATA_ONBATT'];
		print_r(json_encode($result));
	}

	public function currmonth()
	{
		$bulan = date('m');
		$tahun = date('Y');
		$hari = date('d');
		$hasil = array();
		$day = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
		$date1 = $tahun."-".$bulan."-";
		for ($i=0; $i < $day; $i++) {
			if ($i != $hari) {
				$x = $i+1;
				$dari = $tahun."-".$bulan."-".$x." 00:00:00";
				$ke = $tahun."-".$bulan."-".$x." 23:55:00";
				$hasil[$i] = $this->timeoff($dari, $ke);
			} else {
				break;
			}
		}
		print_r(json_encode($hasil));
	}

	public function get_last_week()
	{
		$ONBATT = $this->M_crud->get_data_last_week('data_ups', 'update_date', 'ONBATT');
		$ONLINE = $this->M_crud->get_data_last_week('data_ups', 'update_date',  'ONLINE');
		$total_hour = array();
		$total_hour['ONLINE'] =  count($ONLINE) * 5; // in minutes
		$total_hour['ONBATT'] =  count($ONBATT) * 5; // in minutes
		print_r(json_encode($total_hour));
	}

	public function get_average_spec($bulan, $tahun){
		$tgl_akhir = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
		$dari = $tahun."-".$bulan."-01 00:00:00";
		$ke = $tahun."-".$bulan."-".$tgl_akhir." 23:55:00";
		$DATA = json_encode($this->M_crud->get_data_spec_date_all('data_ups', 'update_date', $dari, $ke));
		$data_bulan =  json_decode($DATA, true);
		$total_data_battery_capacity = 0;
		$total_data_runtime = 0;
		$total_data_load = 0;
		$total_data_input = 0;
		$avg_battery_capacity = 0;
		$avg_runtime = 0;
		$avg_load = 0;
		$avg_input = 0;
		for ($i=0; $i < count($data_bulan); $i++) { 
			$total_data_battery_capacity = $total_data_battery_capacity + $data_bulan[0]["battery_capacity"];
			$total_data_runtime = $total_data_runtime + $data_bulan[0]["battery_runtime"];
			$total_data_load = $total_data_load + $data_bulan[0]["load_capacity"];
			$total_data_input = $total_data_input + $data_bulan[0]["input_voltage"];
		}
		$average['battery_capacity'] = $total_data_battery_capacity/count($data_bulan);
		$average['battery_runtime'] = $total_data_runtime/count($data_bulan);
		$average['load_capacity'] = $total_data_load/count($data_bulan);
		$average['input_voltage'] = $total_data_input/count($data_bulan);
		print_r(json_encode($average));
	}

	public function get_average($field)
	{
		$array_average = array();
		///////////////////////////////// CURRENT DAY /////////////////////////////////////////
		$bulan = date('m');
		$tahun = date('Y');
		$hari = date('d');
		$result = array();
		$dari_cd = $tahun."-".$bulan."-".$hari." 00:00:00";
		$ke_cd = $tahun."-".$bulan."-".$hari." 23:55:00";
		$DATA_CD = json_encode($this->M_crud->get_data_spec_date_all('data_ups', 'update_date', $dari_cd, $ke_cd));
		$data_current_day =  json_decode($DATA_CD, true);
		$total_load_cd = 0;
		$avg_cd = 0;
		for ($i=0; $i < count($data_current_day); $i++) { 
			$total_load_cd = $total_load_cd + $data_current_day[0][$field];
		}
		$array_average['avg_cd'] = $total_load_cd/count($data_current_day);
		///////////////////////////////// CURRENT DAY /////////////////////////////////////////

		///////////////////////////////// LAST WEEK /////////////////////////////////////////
		$DATA_LW = json_encode($this->M_crud->get_data_last_week_all('data_ups', 'update_date'));
		$data_last_week = json_decode($DATA_LW, true);
		$total_load_lw = 0;
		$avg_lw = 0;
		for ($i=0; $i < count($data_last_week); $i++) { 
			$total_load_lw = $total_load_lw + $data_last_week[0][$field];
		}
		$array_average['avg_lw'] =  $total_load_lw/count($data_last_week);
		///////////////////////////////// LAST WEEK /////////////////////////////////////////

		///////////////////////////////// CURRENT_MONTH /////////////////////////////////////////
		$dari_cm = $tahun."-".$bulan."-01 00:00:00";
		$ke_cm = $tahun."-".$bulan."-".$hari." 23:55:00";
		$DATA_CM = json_encode($this->M_crud->get_data_spec_date_all('data_ups', 'update_date', $dari_cm, $ke_cm));
		$data_current_month =  json_decode($DATA_CM, true);
		$total_load_cm = 0;
		$avg_cm = 0;
		for ($i=0; $i < count($data_current_month); $i++) { 
			$total_load_cm = $total_load_cm + $data_current_month[0][$field];
		}
		$array_average['avg_cm'] = $total_load_cm/count($data_current_month);
		///////////////////////////////// CURRENT_MONTH /////////////////////////////////////////

		///////////////////////////////// LAST MONTH /////////////////////////////////////////
		if ($bulan == 1) {
			$bulan_lalu = 12;
			$tahun = $tahun-1;
		}else {
			$bulan_lalu = $bulan-1;
			$tahun = $tahun;
		}
		$day = cal_days_in_month(CAL_GREGORIAN,$bulan_lalu,$tahun);
		$dari_lm = $tahun."-".$bulan_lalu."-01 00:00:00";
		$ke_lm = $tahun."-".$bulan_lalu."-".$day." 23:55:00";
		$DATA_LM = json_encode($this->M_crud->get_data_spec_date_all('data_ups', 'update_date', $dari_lm, $ke_lm));
		$data_last_month =  json_decode($DATA_LM, true);
		$total_load_lm = 0;
		$avg_lm = 0;
		for ($i=0; $i < count($data_last_month); $i++) { 
			$total_load_lm = $total_load_lm + $data_last_month[0][$field];
		}
		$array_average['avg_lm'] = $total_load_lm/count($data_last_month);
		///////////////////////////////// LAST MONTH /////////////////////////////////////////
		print_r(json_encode($array_average));
	}

	public function poweronbatt($bulan, $tahun)
	{
		
		$tgl_akhir = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
		$dari = $tahun."-".$bulan."-01 00:00:00";
		$ke = $tahun."-".$bulan."-".$tgl_akhir." 23:55:00";
		$data = $this->M_crud->get_data_spec_date_all('data_ups', 'update_date', $dari, $ke);
		$count = 0;
		$count_onbatt =  0;
		$cek = FALSE;
		$total_hasil = 0;
		$total = array();
		$array_onbatt = array();
		$array_baru = array();
		for ($i=0; $i < count($data); $i++) { 
			
			if ($data[$i]->status == 'ONBATT') {
				if ($cek == FALSE) {
					$cek = TRUE;
					$count++;
				}
				
				if (empty($array_onbatt[$i-1])) {
					$total_hasil = 0;
					$count_onbatt = 0;
					$array_baru[$count-1] = $data[$i]->update_date;
					$total[$count-1]['waktu'] =  $data[$i]->update_date;
				}
				$array_onbatt[$i]['count'] = $count_onbatt+1;
				$array_onbatt[$i]['waktu'] =  $data[$i]->update_date;
				$count_onbatt++;
				$total[$count-1]['jumlah_row'] = $total_hasil + $count_onbatt;
				$total[$count-1]['menit'] = $total[$count-1]['jumlah_row']*5;
				$penambahan = ($total[$count-1]['jumlah_row'])*5;
				$date = date_create($total[$count-1]['waktu']);
				date_add($date, date_interval_create_from_date_string($penambahan.' minutes'));
				$total[$count-1]['waktu_akhir'] = date_format($date, 'Y-m-d H:i:s');
				// $total[$count-1]['waktu_akhir'] = 
			}else {
				$cek = FALSE;
				
			}
		}

		print_r(json_encode($total));
	}
}
