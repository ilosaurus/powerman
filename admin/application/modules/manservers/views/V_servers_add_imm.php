<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Add Server With IBM imm</h2>
                    </div>
                    <div class="body">
                        <form action="<?php echo base_url() ?>manservers/addilo" id="form_imm" method="post">

                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Hostname</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="server_hostname" class="form-control" placeholder="Enter Server Hostname" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="serveraddress" type="text" name="server_address" class="form-control" placeholder="Enter Server Address -> ex : 10.0.0.1" pattern="^([0-9]{1,3}\.){3}[0-9]{1,3}$" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Mac Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="server_mac_address" name="server_mac_address" class="form-control" placeholder="Enter Server Mac Address" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="server_username" class="form-control" placeholder="Enter Server Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" name="server_password" class="form-control" placeholder="Enter Server Password" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server SSH Port</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_port" type="text" name="server_ssh_port" class="form-control" placeholder="Enter Server SSH Port" required>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            
                            <!-- ilo section -->
                            <div id="ilo_section"  class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >IBM IMM Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="imm_address" name="imm_address" class="form-control" placeholder="Enter IBM IMM Address" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >IBM IMM Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="imm_username" name="imm_username" class="form-control" placeholder="Enter IBM IMM Username" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >IBM IMM Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="imm_password" name="imm_password" class="form-control" placeholder="Enter IBM IMM Password" >
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <!-- ilo section -->                           
                            
                            <div class="row">
                                
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Server Role</label>
                                    <div class="form-group">
                                        <?php $data_server_role = $this->M_crud->get_data('server_category'); ?>
                                        <select id="select_server_role" class="form-control show-tick" name="server_role" required>
                                            <option value selected disabled >-- Please select server role --</option>
                                            <?php  foreach ($data_server_role as $key) { ?> 
                                            <option value="<?php echo $key->id ?>"><?php echo $key->category ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Auto Power On and Off</label>
                                    <div class="form-group">
                                        <select name="auto_power" class="form-control show-tick" required>
                                            <option value selected disabled >-- Auto Power On and Off --</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12"> 
                                    <label >Method Power Off</label>
                                    <select name="method_poweroff" class="form-control show-tick" required>
                                            <option value selected disabled >-- Method Power Off --</option>
                                            <option value="1">SSH Connection</option>
                                            <option value="4">IBM IMM Connection</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Method Power On</label>
                                    <select name="method_poweron" class="form-control show-tick" required>
                                            <option value selected disabled >-- Method Power On --</option>
                                            <option value="3">Wake On LAN</option>
                                            <option value="4">IBM IMM Connection</option>
                                    </select>
                                </div>
                            </div>
                


                            <div class="form-group">
                                <div class="form">
                                    <button type="submit" onClick="add_data()" class="form-control btn btn-primary m-t-15 waves-effect" >Save Server</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>


<script>
$('#serveraddress').mask('099.099.099.099');
$("#server_port").numeric({
    allowSpace: false, // Allow the space character
    allowUpper: false  // Allow Upper Case characters
});

function add_data() {
    jQuery.validator.messages.required = "";
    $("#form_imm").validate({
        submitHandler: function(form) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>manservers/addimm",
                data: $("#form_imm").serialize(),
                beforeSend : function () {
                    swal({
                        title: "Harap Tunggu",
                        type: "info",
                        showCancelButton: false,
                        closeOnConfirm: false,
                    });
                },
                success: function(response) {
                    // console.log(response);
                    data_array = response.split("|");
                    swal({
                        title: ""+data_array[1]+"",
                        type: ""+data_array[0]+""
                    },
                    function() {
                        location.reload();
                    }
                    );
                },
                error : function () {
                    swal("Cancelled", "Something Wrong", "error");
                }
            });
        }
    });

}

</script>


