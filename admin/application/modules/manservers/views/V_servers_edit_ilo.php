
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Edit Server (iLO)</h2>
                        
                    </div>
                    <div class="body">
                        <form id="form_general" action="#" method="post">                                            
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Hostname</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="server_hostname" name="server_hostname" class="form-control" placeholder="Enter Server Hostname" value="<?php echo $data_server[0]->server_hostname ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_address" type="text" name="server_address" class="form-control" placeholder="Enter Server Address -> ex : 10.0.0.1" pattern="^([0-9]{1,3}\.){3}[0-9]{1,3}$" value="<?php echo $data_server[0]->server_address ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Mac Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="server_mac_address" name="server_mac_address" class="form-control" placeholder="Enter Server Mac Address" value="<?php echo $data_server[0]->server_mac_address ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="server_username" name="server_username" class="form-control" placeholder="Enter Server Username" value="<?php echo $data_server[0]->server_user ?>" required>
                                            <input type="hidden"  name="id_server" class="form-control"  value="<?php echo $data_server[0]->id ?>" required>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_password" type="password" name="server_password" class="form-control" placeholder="Kosongkan jika tidak ingin di ganti" value="">
                                            <input type="hidden" name="server_password_lama" class="form-control"  value="<?php echo $data_server[0]->server_password ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server SSH Port</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_port" type="text" name="server_ssh_port" class="form-control" placeholder="Enter Server SSH Port" value="<?php echo $data_server[0]->server_ssh_port ?>" required>
                                            <input  type="hidden" name="urutan_lama" class="form-control"  value="<?php echo $data_server[0]->id_urutan_sop ?>" required>

                                        </div>
                                    </div>
                                </div>
                            </div>   

                                <!-- ilo section -->
                            <div id="ilo_section"  class="row">
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ilo_address" name="ilo_address"  value="<?php echo $data_ilo[0]->ilo_address ?>" class="form-control" placeholder="Enter HP iLO Address" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ilo_username" name="ilo_username" value="<?php echo $data_ilo[0]->ilo_username ?>" class="form-control" placeholder="Enter HP iLO Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="ilo_password" name="ilo_password"  class="form-control" placeholder="Kosongkan Jika Tidak Ingin Mengganti Password" >
                                            <input type="hidden" name="ilo_password_lama"  value="<?php echo $data_ilo[0]->ilo_password ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Version</label>
                                    <select name="ilo_version" class="form-control show-tick" required>
                                        <option value selected disabled >-- Select HP iLO Version --</option>
                                        <?php for ($i=1; $i <= 3; $i++) {  
                                            if ($i == $data_ilo[0]->ilo_version) {
                                                $sel = "selected";
                                            } else {
                                                $sel = "";
                                            }
                                            
                                        ?>
                                            <option <?php echo $sel; ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>    
                            <!-- ilo section -->


                            <div class="row">
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Method Power Off</label>
                                    <div  class="form-group">
                                        <select name="method_poweroff" required class="form-control show-tick"  >
                                            <option  value="2">iLO Connection</option>
                                            <option  value="1">SSH Connection</option>
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Method Power On</label>
                                    <div  class="form-group">
                                        <select name="method_poweron" required class="form-control show-tick"  >
                                            <option  value="2">iLO Connection</option>
                                            <option  value="3">Wake On LAN</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Server Role</label>
                                    <div class="form-group">
                                        <?php $data_server_role = $this->M_crud->get_data('server_category'); ?>
                                        <select id="select_server_role" class="form-control show-tick" name="server_role" required>
                                            <option value selected disabled >-- Please select server role --</option>
                                            <?php  foreach ($data_server_role as $key) { 
                                                if($key->id == $data_server[0]->id_server_category){
                                                    $sel = "selected";
                                                }else{
                                                    $sel = "";
                                                }
                                            ?> 
                                            <option <?php echo $sel ?> value="<?php echo $key->id ?>"><?php echo $key->category ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Auto Power On and Off</label>
                                    <div class="form-group">
                                        
                                        <select name="auto_power" class="form-control show-tick" required>
                                            <option value disabled >-- Auto Power On and Off --</option>
                                            <option <?php if($data_server[0]->id_urutan_sop != '0'){ echo "selected"; } ?> value="1">Yes</option>
                                            <option <?php if($data_server[0]->id_urutan_sop == '0'){ echo "selected"; } ?> value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>                         

                            <div class="form-group">
                                <div class="form">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-12 col xs 12">
                                            <button type="submit" onClick="add_data()" class=" form-control btn btn-primary m-t-15 waves-effect" >Save Server</button>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-12 col xs 12">
                                            <input type="button" onClick="redirect_url('<?php echo base_url() ?>servers/')" class=" form-control btn btn-danger m-t-15 waves-effect" value="Cancel">
                                        </div>
                                    </div>
                                </div>
                                <?php 
//  print_r($data_server);

 
?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>



<div style="display:none" class="error">
    <span></span>
</div>

<script>

function redirect_url(url) {
    location.href = ""+url+"";
}

function add_data() {
    jQuery.validator.messages.required = "";
    $("#form_general").validate({
        submitHandler: function(form) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>manservers/edit_server_ilo",
                data: $("#form_general").serialize(),
                beforeSend : function () {
                    swal({
                        title: "Harap Tunggu",
                        type: "info",
                        showCancelButton: false,
                        closeOnConfirm: false,
                    });
                },
                success: function(response) {
                    // console.log(response);
                    data_array = response.split("|");
                    swal({
                        title: ""+data_array[1]+"",
                        type: ""+data_array[0]+""
                    },
                    function() {
                        location.reload();
                    }
                    );
                },
                error : function () {
                    swal("Cancelled", "Something Wrong", "error");
                }
            });
        }
    });

}

$('#serveraddress').mask('099.099.099.099');
$("#server_port").numeric({
    allowSpace: false, // Allow the space character
    allowUpper: false  // Allow Upper Case characters
});


</script>


