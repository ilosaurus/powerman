<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Add Server</h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                    </div>
                    <div class="body">
                        <form action="<?php echo base_url() ?>servers/add" method="post">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Category</label>
                                    <div class="form-group">
                                        <select onchange="disable_proxmox_api_method(this.value)" id="select_server_category" class="form-control show-tick" name="server_category" required>
                                            <option value selected disabled >-- Please select server category --</option>
                                            <option value="0">Physical Server</option>
                                            <option value="1">Virtual Machine (Proxmox)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Role</label>
                                    <div class="form-group">
                                        <?php $data_server_role = $this->M_crud->get_data('server_category'); ?>
                                        <select id="select_server_role" class="form-control show-tick" name="server_role" required>
                                            <option value selected disabled >-- Please select server role --</option>
                                            <?php  foreach ($data_server_role as $key) { ?> 
                                            <option value="<?php echo $key->id ?>"><?php echo $key->category ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Auto Power On and Off</label>
                                    <div class="form-group">
                                        <select name="urutan" class="form-control show-tick" required>
                                            <option value selected disabled >-- Auto Power On and Off --</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <?php $method_power = $this->M_crud->get_data('method_poweron'); ?> 
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    <label >Method Power Off</label>
                                    <div id="default_power_off"  class="form-group">
                                        <select disabled required class="form-control show-tick"  >
                                            <option value selected disabled >-- Please select Server Category Before --</option>
                                        </select>
                                    </div>
                                    <div id="method_power_off_not_vm" style="display:none" class="form-group">
                                        <select onchange="get_method(this.value)" name="method_poweroff" id="select_server_method_poweroff_not_vm" class="form-control show-tick"  >
                                            <option value selected disabled >-- Please select Power Off Method --</option>
                                            <?php  foreach ($method_power as $key) { ?> 
                                            <?php if ($key->kode != 'proxmoxapi' && $key->kode != 'wol') { ?>
                                            <option value="<?php echo $key->id ?>"><?php echo $key->metode ?></option>
                                            <?php }  ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div id="method_power_off_vm" style="display:none" class="form-group">
                                        <select  id="select_server_method_poweroff_vm" class="form-control show-tick" name="method_poweroff" >
                                            <option value selected disabled >-- Please select Power Off Method --</option>
                                            <?php  foreach ($method_power as $key) { ?> 
                                            <?php if ($key->kode == 'proxmoxapi' || $key->kode == 'ssh') { ?>
                                            <option value="<?php echo $key->id ?>"><?php echo $key->metode ?></option> 
                                            <?php }} ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                    <label >Method Power On</label>
                                    <div id="default_power_on"  class="form-group">
                                        <select disabled required class="form-control show-tick"  >
                                            <option value selected disabled >-- Please select Server Category Before --</option>
                                        </select>
                                    </div>
                                    <div id="method_power_on_not_vm" style="display:none" class="form-group">
                                        <select onchange="get_method(this.value)" id="select_server_method_poweron_not_vm" class="form-control show-tick" name="method_poweron" >
                                            <option value selected disabled >-- Please select Power On Method --</option>
                                            <?php  foreach ($method_power as $key2) { ?> 
                                            <?php if ($key2->kode != 'ssh' && $key2->kode != 'proxmoxapi') { ?>
                                            <option value="<?php echo $key2->id ?>"><?php echo $key2->metode ?></option>
                                            <?php }  ?>
                                            <?php  } ?>
                                        </select>
                                    </div>
                                    <div id="method_power_on_vm" style="display:none" class="form-group">
                                        <select  id="select_server_method_poweron_vm" class="form-control show-tick" name="method_poweron" >
                                            <option value selected disabled >-- Please select Power On Method --</option>
                                            <?php  foreach ($method_power as $key2) { ?> 
                                            <?php if ($key2->kode == 'proxmoxapi') { ?>
                                            <option value="<?php echo $key2->id ?>"><?php echo $key2->metode ?></option> 
                                            <?php }} ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Hostname</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="server_hostname" class="form-control" placeholder="Enter Server Hostname" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="serveraddress" type="text" name="server_address" class="form-control" placeholder="Enter Server Address -> ex : 10.0.0.1" pattern="^([0-9]{1,3}\.){3}[0-9]{1,3}$" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Mac Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="server_mac_address" class="form-control" placeholder="Enter Server Mac Address" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="server_username" class="form-control" placeholder="Enter Server Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" name="server_password" class="form-control" placeholder="Enter Server Password" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server SSH Port</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_port" type="text" name="server_ssh_port" class="form-control" placeholder="Enter Server SSH Port" required>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            
                            <div id="vm_section" style="display : none" class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Virtual Machine ID</label>
                                    <div  class="form-group">
                                        <div class="form-line">
                                            <input  type="text" name="vm_id" class="form-control" placeholder="Enter VMID Proxmox">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label  >Server Parent</label>
                                    <div  class="form-group">
                                        <?php 
                                            $where_server2 = array(
                                                array("id_jenis_server", "0")
                                            );
                                            $data_server2 = $this->M_crud->get_data('server', $where_server2);
                                        ?>
                                        
                                        <select  name="vm_id_server_parent" class="form-control show-tick"  >
                                            <option value selected disabled >-- Please select Server Proxmox --</option>
                                            <?php foreach ($data_server2 as $data_server2) { ?>
                                            <option value="<?php echo $data_server2->id ?>"><?php echo $data_server2->server_hostname ?>/<?php echo $data_server2->server_address ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label  >VM Category</label>
                                    <div  class="form-group">
                                        <select  name="vm_id_category" class="form-control show-tick"  >
                                            <option value selected disabled >-- Please select Server Proxmox --</option>
                                            <option value="lxc" >Proxmox Container</option>
                                            <option value="qemu" >Proxmox Virtual Machine</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            <script>
                                function get_method(value) {
                                    

                                    if (value == '2') {
                                        $("#ilo_section").show();
                                        $("#imm_section").hide();
                                        $("#ucs_section").hide();
                                    } 

                                    if (value == '4') {
                                        $("#ilo_section").hide();
                                        $("#ucs_section").hide();
                                        $("#imm_section").show();
                                    }

                                    if (value == '5') {
                                        $("#imm_section").hide();
                                        $("#ilo_section").hide();
                                        $("#ucs_section").show();
                                    } 
                                    console.log(value);
                                }
                            </script>

                            <!-- IMM Section -->
                            <div id="imm_section" style="display: none" class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >IBM IMM Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="imm_address" name="imm_address" class="form-control" placeholder="Enter IBM IMM Address" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >IBM IMM Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="imm_username" name="imm_username" class="form-control" placeholder="Enter IBM IMM Username" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >IBM IMM Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="imm_password" name="imm_password" class="form-control" placeholder="Enter IBM IMM Password" >
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <!-- IMM Section -->

                            <!-- ilo section -->
                            <div id="ilo_section" style="display: none" class="row">
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ilo_address" name="ilo_address" class="form-control" placeholder="Enter HP iLO Address" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ilo_username" name="ilo_username" class="form-control" placeholder="Enter HP iLO Username" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="ilo_password" name="ilo_password" class="form-control" placeholder="Enter HP iLO Password" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Version</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ilo_version" name="ilo_version" class="form-control" placeholder="Enter HP iLO Version" >
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <!-- ilo section -->

                            <!-- ucs xml api section -->
                            <div id="ucs_section" style="display: none" class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >UCS Mgmt Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ucs_mgmt_address" name="ucs_mgmt_address" class="form-control" placeholder="Enter UCS Mgmt Address" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >UCS Mgmt Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ucs_mgmt_username" name="ucs_mgmt_username" class="form-control" placeholder="Enter UCS Mgmt Username" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >UCS Mgmt Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="ucs_mgmt_password" name="ucs_mgmt_password" class="form-control" placeholder="Enter UCS Mgmt Password" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ucs xml api section -->

                            <div class="form-group">
                                <div class="form">
                                    <input type="submit" name="submit" class="form-control btn btn-primary m-t-15 waves-effect" value="Save Server">
                                </div>
                            </div>


                            

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>


<script>
$('#serveraddress').mask('099.099.099.099');
$("#server_port").numeric({
    allowSpace: false, // Allow the space character
    allowUpper: false  // Allow Upper Case characters
});

function disable_proxmox_api_method(value) {
    if (value == '1') { // VM
        $("#method_power_off_vm, #method_power_on_vm").show();
        $("#method_power_off_not_vm, #method_power_on_not_vm, #default_power_off, #default_power_on").hide();
        $('#select_server_method_poweroff_vm').prop('disabled', false);
        $("#vm_section").show();

        $('#select_server_method_poweroff_vm').attr('name', 'method_poweroff');
        $('#select_server_method_poweron_vm').attr('name', 'method_poweron');
        $('#select_server_method_poweroff_not_vm').removeAttr('name');
        $('#select_server_method_poweron_not_vm').removeAttr('name');
    } else { // physical server
        $("#method_power_off_vm, #method_power_on_vm, #default_power_on, #default_power_off").hide();
        $("#method_power_off_not_vm, #method_power_on_not_vm").show();
        $('#select_server_method_poweroff_not_vm').prop('disabled', false);
        $("#vm_section").hide();
        
        $('#select_server_method_poweroff_not_vm').attr('name', 'method_poweroff');
        $('#select_server_method_poweron_not_vm').attr('name', 'method_poweron')
        $('#select_server_method_poweroff_vm').removeAttr('name');
        $('#select_server_method_poweron_vm').removeAttr('name');
    }
    // $('#proxmox_api_1', window.parent.document).css("display", "none");
}    
</script>


