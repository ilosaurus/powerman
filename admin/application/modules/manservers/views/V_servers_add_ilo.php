<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Add Server With iLO Connection</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown ">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                    <i class="material-icons">add</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="#" onclick="showmodal()" type="button" class="waves-effect waves-block">Import Data</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="<?php echo base_url() ?>manservers/addilo" id="form_ilo" method="post">

                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Hostname</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="server_hostname" class="form-control" placeholder="Enter Server Hostname" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="serveraddress" type="text" name="server_address" class="form-control" placeholder="Enter Server Address -> ex : 10.0.0.1" pattern="^([0-9]{1,3}\.){3}[0-9]{1,3}$" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Mac Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="server_mac_address" name="server_mac_address" class="form-control" placeholder="Enter Server Mac Address" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="server_username" class="form-control" placeholder="Enter Server Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" name="server_password" class="form-control" placeholder="Enter Server Password" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server SSH Port</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_port" type="text" name="server_ssh_port" class="form-control" placeholder="Enter Server SSH Port" required>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            
                            <div class="row">
                                
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Server Role</label>
                                    <div class="form-group">
                                        <?php $data_server_role = $this->M_crud->get_data('server_category'); ?>
                                        <select id="select_server_role" class="form-control show-tick" name="server_role" required>
                                            <option value selected disabled >-- Please select server role --</option>
                                            <?php  foreach ($data_server_role as $key) { ?> 
                                            <option value="<?php echo $key->id ?>"><?php echo $key->category ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Auto Power On and Off</label>
                                    <div class="form-group">
                                        <select name="auto_power" class="form-control show-tick" required>
                                            <option value selected disabled >-- Auto Power On and Off --</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12"> 
                                    <label >Method Power Off</label>
                                    <select name="method_poweroff" class="form-control show-tick" required>
                                            <option value selected disabled >-- Method Power Off --</option>
                                            <option value="1">SSH Connection</option>
                                            <option value="2">iLO Connection</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >Method Power On</label>
                                    <select name="method_poweron" class="form-control show-tick" required>
                                            <option value selected disabled >-- Method Power On --</option>
                                            <option value="3">Wake On LAN</option>
                                            <option value="2">iLO Connection</option>
                                    </select>
                                </div>
                            </div>
                

                            <!-- ilo section -->
                            <div id="ilo_section"  class="row">
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ilo_address" name="ilo_address" class="form-control" placeholder="Enter HP iLO Address" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ilo_username" name="ilo_username" class="form-control" placeholder="Enter HP iLO Username" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="ilo_password" name="ilo_password" class="form-control" placeholder="Enter HP iLO Password" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                                    <label >HP iLO Version</label>
                                    <select name="ilo_versione" class="form-control show-tick" required>
                                        <option value selected disabled >-- Select HP iLO Version --</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>    
                            <!-- ilo section -->

                            <div class="form-group">
                                <div class="form">
                                    <button type="submit" onClick="add_data()" class="form-control btn btn-primary m-t-15 waves-effect" >Save Server</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>



<!-- modal section -->
<div class="modal fade" id="modal_import" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">

    <form action="<?php echo base_url() ?>manservers/import_ilo" id="form_import" enctype="multipart/form-data" method="post" accept-charset="utf-8">
  
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title" id="defaultModalLabel">Import Data Server iLO</h3>
         </div>
        
         <div class="modal-body">
            
                <div class="form-group">
                    <div class="form-line">
                    <?= form_upload(array('id' => 'txtFileImport', 'name' => 'fileImport', 'class' => 'form-control', 'required' => ''))?>
                    </div>
                </div>
                
                <div>
                <p>
                    Anda dapat melakukan import data server dari file excel dengan mengikuti format template yang di sediakan.
                    <a href="<?php echo base_url() ?>assets/backend/template/templateimportgeneral.xls">Download Exceltemplate</a>
                </p>
                <p id="msg1" style="display:none">
                    File terlalu Besar
                </p>
                </div>
         </div>
         <div class="modal-footer">
         <!-- <button type="submit" onClick="import_data()" class="btn btn-link waves-effect" >IMPORT</button> -->

            <?= form_submit(array('name' => 'submit', 'value' => 'UPLOAD', 'class' => 'btn btn-link waves-effect', 'onclick' => 'import_data()'))?>
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
         
      </div>
      <?= form_close()?>
   </div>
</div>
<!-- modal section -->

<script>
$('#serveraddress').mask('099.099.099.099');
$("#server_port").numeric({
    allowSpace: false, // Allow the space character
    allowUpper: false  // Allow Upper Case characters
});


var attachement = document.getElementById('txtFileImport');
attachement.onchange = function() {
    var file = attachement.files[0];
    var extension = file.type;
    var nama_file = file.name;
    var ekstensi = nama_file.split('.')[1];
    if (ekstensi != "xlsx" && ekstensi != "xls" && ekstensi != "csv") {
        alert("Jenis file tidak valid");
        document.getElementById("txtFileImport").value = "";
    }else{
        if (file.size > 5500304) {
            alert("Ukuran file lebih 5 mb");
            document.getElementById("txtFileImport").value = "";
        }else{
            console.log("File sama dan ukuran file mantap");
        }
        
    }

    // if (file.size > 5000) {
    //    document.getElementById("txtFileImport").value = "";
    // }else if(ekstensi ){
    //    document.getElementById("txtFileImport").value = "";
    // }
};

function add_data() {
    jQuery.validator.messages.required = "";
    $("#form_ilo").validate({
        submitHandler: function(form) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>manservers/addilo",
                data: $("#form_ilo").serialize(),
                beforeSend : function () {
                    console.log("Lagi loading");
                    swal({
                        title: "Harap Tunggu",
                        type: "info",
                        showCancelButton: false,
                        closeOnConfirm: false,
                    });
                },
                success: function(response) {
                    // console.log(response);
                    
                    data_array = response.split("|");
                    swal({
                        title: ""+data_array[1]+"",
                        type: ""+data_array[0]+""
                    },
                    function() {
                        location.reload();
                    }
                    );
                },
                error : function () {
                    swal("Cancelled", "Something", "error");
                }
            });
        }
    });

}

function import_data() {
    // console.log($("#form_import").serialize());
    // var data = new FormData();
    // jQuery.each(jQuery('#txtFileImport')[0].files, function(i, file) {
    //     data.append('file-'+i, file);
    // });
   
    var form = new FormData($("#form_import")[0]);

    jQuery.validator.messages.required = "";
    // var file = $("txtFileImport").val();
    // var file = document.getElementById('txtFileImport').files[0];
    // var file_data = $('#txtFileImport').prop('files')[0];   
    // var form_data = new FormData();  
    // form_data.append('file', file_data);
    
    $("#form_import").validate();
    // alert("Import Data");
}

function showmodal() {
    // alert("ds");
    $('#modal_import').modal('show');
}
</script>


