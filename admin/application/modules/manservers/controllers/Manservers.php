<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('MENU_AKTIF', 'servers');
class Manservers extends MY_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->require_login(TRUE);
		$this->load->library('PHPExcel');
		$this->load->library('upload');
	}

	public function index()
	{
		redirect(base_url().'servers','refresh');
	}

	public function addgeneral()
	{
		$data2['active_nav'] =  MENU_AKTIF;
		if($this->input->post('server_hostname')){
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$auto_power = $this->input->post('auto_power');

			if ($auto_power == '0') {
				$urutan = '0';
			}else {
				$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
				if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
			}

			$data = array(
				'server_hostname' => $server_hostname,
				'server_address' => $server_address,
				'server_user' => $server_username,
				'server_password' => $server_password,
				'server_ssh_port' => $server_ssh_port,
				'server_mac_address' => $server_mac_address,
				'id_method_poweroff' => $method_poweroff,
				'id_method_poweron' => $method_poweron,
				'id_server_category' => $server_role,
				'id_jenis_server' => '0',
				'id_urutan_sop' => $urutan,
				'status' => $auto_power, // nda jelaspi ini
				'id_jenis_method' => '1', 
				'server_status' => '0'
			);
			$this->M_crud->insert_data('server', $data, $req = TRUE);
			echo "success|Insert Data Success";
		}else{
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_add_general');
			$this->load->view('footer');
		}
	}

	public function addilo()
	{
		$data2['active_nav'] =  MENU_AKTIF;
		if($this->input->post('server_hostname')){
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$auto_power = $this->input->post('auto_power');
			$ilo_address = $this->input->post('ilo_address');
			$ilo_username = $this->input->post('ilo_username');
			$ilo_password = $this->Crypt->en($this->input->post('ilo_password'));
			$ilo_version = $this->input->post('ilo_versione');

			if ($auto_power == '0') {
				$urutan = '0';
			}else {
				$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
				if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
			}


			$data = array(
				'server_hostname' => $server_hostname,
				'server_address' => $server_address,
				'server_user' => $server_username,
				'server_password' => $server_password,
				'server_ssh_port' => $server_ssh_port,
				'server_mac_address' => $server_mac_address,
				'id_method_poweroff' => $method_poweroff,
				'id_method_poweron' => $method_poweron,
				'id_server_category' => $server_role,
				'id_jenis_server' => '0',
				'id_urutan_sop' => $urutan,
				'status' => $auto_power, // nda jelaspi ini
				'id_jenis_method' => '2', 
				'server_status' => '0'
			);

			$insert_id_server = $this->M_crud->insert_data('server', $data, $req = TRUE);

			// echo $insert_id_server;
			$data_ilo = array(
				'ilo_address' => $ilo_address,
				'ilo_username' => $ilo_username,
				'ilo_password' => $ilo_password,
				'ilo_version' => $ilo_version,
				'ilo_status' => "0",
				'id_server' => $insert_id_server
			);

			$this->M_crud->insert_data('ilo', $data_ilo);

			echo "success|Insert Data Success";
			// print_r($data_ilo);
			// echo $server_hostname;
			
		}else{
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_add_ilo');
			$this->load->view('footer');
		}
	}

	public function addimm()
	{
		$data2['active_nav'] =  MENU_AKTIF;
		if($this->input->post('server_hostname')){
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$auto_power = $this->input->post('auto_power');
			$imm_address = $this->input->post('imm_address');
			$imm_username = $this->input->post('imm_username');
			$imm_password = $this->Crypt->en($this->input->post('imm_password'));

			if ($auto_power == '0') {
				$urutan = '0';
			}else {
				$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
				if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
			}

			$data = array(
				'server_hostname' => $server_hostname,
				'server_address' => $server_address,
				'server_user' => $server_username,
				'server_password' => $server_password,
				'server_ssh_port' => $server_ssh_port,
				'server_mac_address' => $server_mac_address,
				'id_method_poweroff' => $method_poweroff,
				'id_method_poweron' => $method_poweron,
				'id_server_category' => $server_role,
				'id_jenis_server' => '0',
				'id_urutan_sop' => $urutan,
				'status' => $auto_power, // nda jelaspi ini
				'id_jenis_method' => '3', 
				'server_status' => '0'
			);

			// print_r($data);

			$insert_id_server = $this->M_crud->insert_data('server', $data, $req = TRUE);

			// // echo $insert_id_server;
			$data_umm = array(
				'imm_address' => $imm_address,
				'imm_username' => $imm_username,
				'imm_password' => $imm_password,
				'id_server' => $insert_id_server
			);

			$this->M_crud->insert_data('imm', $data_umm);

			echo "success|Insert Data Success";
			// print_r($data_ilo);
			// echo $server_hostname;
			
		}else{
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_add_imm');
			$this->load->view('footer');
		}
	}

	public function adducs()
	{
		$data2['active_nav'] =  MENU_AKTIF;
		if($this->input->post('server_hostname')){
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$auto_power = $this->input->post('auto_power');
			$ucs_address = $this->input->post('ucs_address');
			$ucs_username = $this->input->post('ucs_username');
			$ucs_password = $this->Crypt->en($this->input->post('ucs_password'));

			if ($auto_power == '0') {
				$urutan = '0';
			}else {
				$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
				if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
			}

			$data = array(
				'server_hostname' => $server_hostname,
				'server_address' => $server_address,
				'server_user' => $server_username,
				'server_password' => $server_password,
				'server_ssh_port' => $server_ssh_port,
				'server_mac_address' => $server_mac_address,
				'id_method_poweroff' => $method_poweroff,
				'id_method_poweron' => $method_poweron,
				'id_server_category' => $server_role,
				'id_jenis_server' => '0',
				'id_urutan_sop' => $urutan,
				'status' => $auto_power, // nda jelaspi ini
				'id_jenis_method' => '4', 
				'server_status' => '0'
			);

			// print_r($data);

			$insert_id_server = $this->M_crud->insert_data('server', $data, $req = TRUE);

			// // echo $insert_id_server;
			$data_ucs = array(
				'ucs_address' => $ucs_address,
				'ucs_username' => $ucs_username,
				'ucs_password' => $ucs_password,
				'id_server' => $insert_id_server
			);
			$this->M_crud->insert_data('ucs', $data_ucs);
			echo "success|Insert Data Success";
			// print_r($data_ilo);
			// echo $server_hostname;
			
		}else{
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_add_ucs');
			$this->load->view('footer');
		}
	}

	public function add()
	{
		$data2['active_nav'] =  MENU_AKTIF;
		if($this->input->post('submit')){
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$server_category = $this->input->post('server_category');
			
			if ($this->input->post('urutan') == '0') {
				$urutan = '0';
			} else {
				$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
				if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
				// print_r($urutan[0]->id_urutan_sop);
				// echo $urutan;
			}
			
			
			$data = array(
				'server_hostname' => $server_hostname,
				'server_address' => $server_address,
				'server_user' => $server_username,
				'server_password' => $server_password,
				'server_ssh_port' => $server_ssh_port,
				'server_mac_address' => $server_mac_address,
				'id_method_poweroff' => $method_poweroff,
				'id_method_poweron' => $method_poweron,
				'id_server_category' => $server_role,
				'id_jenis_server' => $server_category,
				'id_urutan_sop' => $urutan,
				'status' => '0', // nda jelaspi ini
				'server_status' => '0'
			);

			$insert_id_server = $this->M_crud->insert_data('server', $data, $req = TRUE);
			// echo "<pre>";print_r($data);echo "</pre>";

			if ($method_poweroff == '2' || $method_poweron == '2') {
				$ilo_address = $this->input->post('ilo_address');
				$ilo_username = $this->input->post('ilo_username');
				$ilo_password = $this->Crypt->en($this->input->post('ilo_password'));
				$ilo_version = $this->input->post('ilo_version');
				$insert = $this->insert_ilo($insert_id_server, $ilo_address, $ilo_username, $ilo_password, $ilo_version );
				// echo "<pre>";print_r($insert);echo "</pre>";
			}

			if ($method_poweroff == '3' || $method_poweron == '3') {
				$this->insert_wol($insert_id_server, $server_mac_address);
			}

			if ($method_poweroff == '4' || $method_poweron == '4') {
				$imm_address = $this->input->post('imm_address');
				$imm_username = $this->input->post('imm_username');
				$imm_password = $this->Crypt->en($this->input->post('imm_password'));
				$this->insert_ibm($insert_id_server, $imm_address, $imm_username, $imm_password);
			}

			if ($method_poweroff == '5' || $method_poweron == '5') {
				$ucs_address = $this->input->post('ucs_mgmt_address');
				$ucs_username = $this->input->post('ucs_mgmt_username');
				$ucs_password = $this->Crypt->en($this->input->post('ucs_mgmt_password'));
				$this->insert_ucs($insert_id_server, $ucs_address, $ucs_username, $ucs_password);
			}

			if ($method_poweroff == '6' || $method_poweron == '6') {
				$id_server_parent = $this->input->post('vm_id_server_parent');
				$vmid = $this->input->post('vm_id');
				$jenis = $this->input->post('vm_id_category');
				$this->insert_proxmox($insert_id_server, $id_server_parent, $vmid, $jenis);
			}

			redirect(base_url()."servers", "refresh");

		}else {
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_add');
			$this->load->view('footer');
		}
	}

	public function edit_server($id = null)
	{
		$data2['active_nav'] =  MENU_AKTIF;
		$id = $this->Crypt->de($id);
		if ($this->input->post('server_hostname')) {
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$auto_power = $this->input->post('auto_power');

			
		}else {
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_edit');
			$this->load->view('footer');
		}
	}

	public function edit_server_general($id = null)
	{
		$data2['active_nav'] =  MENU_AKTIF;
		$id = $this->Crypt->de($id);
		if ($this->input->post('server_hostname')) {
			$id_server = $this->input->post('id_server');
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_password_lama = $this->input->post('server_password_lama');
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$server_category = $this->input->post('server_category');
			$auto_power = $this->input->post('auto_power');
			$urutan_lama = $this->input->post('urutan_lama');
			$status_urutan = TRUE;
			if ($auto_power == '0') {
				$urutan = '0';
				$status_urutan = TRUE;
			}else {
				if ($urutan_lama == '0') {
					$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
					if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
					$status_urutan = TRUE;
				} else {
					$urutan = $urutan_lama;
					$status_urutan = FALSE;
				}
			}

			if ($server_password == '') {
				$server_password_baru = $server_password_lama;
			}else {
				$server_password_baru = $server_password;
			}

			$data = array(
				'server_hostname' => $server_hostname,
				'server_address' => $server_address,
				'server_user' => $server_username,
				'server_password' => $server_password_baru,
				'server_ssh_port' => $server_ssh_port,
				'server_mac_address' => $server_mac_address,
				'id_method_poweroff' => $method_poweroff,
				'id_method_poweron' => $method_poweron,
				'id_server_category' => $server_role,
				'id_jenis_server' => '0',
				'id_urutan_sop' => $urutan,
				'status' => $auto_power, // nda jelaspi ini
				'server_status' => '0'
			);
			$where_server = array(array('id', $id_server));
			$this->M_crud->update_data('server', $data, $where_server);
			if ($status_urutan == TRUE) {
				$this->update_urutan();
			}
			
			echo "success|Update Data Success";
		}else {
			$where_id = array(array('id', $id));
			$data['data_server'] = $this->M_crud->get_data('server', $where_id);
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_edit_general', $data);
			$this->load->view('footer');
		}
	}

	public function edit_server_ilo($id = null)
	{
		$data2['active_nav'] =  MENU_AKTIF;
		$id = $this->Crypt->de($id);
		if ($this->input->post('server_hostname')) {
			$id_server = $this->input->post('id_server');
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_password_lama = $this->input->post('server_password_lama');
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$server_category = $this->input->post('server_category');
			$auto_power = $this->input->post('auto_power');
			$urutan_lama = $this->input->post('urutan_lama');

			$ilo_address = $this->input->post('ilo_address');
			$ilo_username = $this->input->post('ilo_username');
			$ilo_password = $this->input->post('ilo_password');
			$ilo_password_lama = $this->input->post('ilo_password_lama');
			$ilo_version = $this->input->post('ilo_version');
			$status_urutan = TRUE;

			if ($auto_power == '0') {
				$urutan = '0';
				$status_urutan = TRUE;
			}else {
				if ($urutan_lama == '0') {
					$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
					if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
					$status_urutan = TRUE;
				} else {
					$urutan = $urutan_lama;
					$status_urutan = FALSE;
				}
			}

			if ($server_password == '') {
				$server_password_baru = $server_password_lama;
			}else {
				$server_password_baru = $server_password;
			}

			if ($ilo_password == '') {
				$ilo_password_baru = $ilo_password_lama;
			}else {
				$ilo_password_baru = $ilo_password;
			}
			$data = array(
				'server_hostname' => $server_hostname,
				'server_address' => $server_address,
				'server_user' => $server_username,
				'server_password' => $server_password_baru,
				'server_ssh_port' => $server_ssh_port,
				'server_mac_address' => $server_mac_address,
				'id_method_poweroff' => $method_poweroff,
				'id_method_poweron' => $method_poweron,
				'id_server_category' => $server_role,
				'id_jenis_server' => '0',
				'id_urutan_sop' => $urutan,
				'status' => $auto_power, // nda jelaspi ini
				'server_status' => '0'
			);
			$data_ilo = array(
				'ilo_address' => $ilo_address,
				'ilo_username' => $ilo_username,
				'ilo_password' => $ilo_password_baru,
				'ilo_version' => $ilo_version
			);
			$where_server = array(array('id', $id_server));
			$where_ilo = array(array('id_server', $id));
			$this->M_crud->update_data('server', $data, $where_server);
			$this->M_crud->update_data('ilo', $data_ilo, $where_ilo);
			
			if ($status_urutan == TRUE) {
				$this->update_urutan();
			}
			
			
			echo "success|Update Data Success";
		}else {
			$where_id = array(array('id', $id));
			$where_ilo = array(array('id_server', $id));
			$data['data_server'] = $this->M_crud->get_data('server', $where_id);
			$data['data_ilo'] = $this->M_crud->get_data('ilo', $where_ilo);
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_edit_ilo', $data);
			$this->load->view('footer');
		}
	}


	public function insert_ilo($id_server, $ilo_address, $ilo_username, $ilo_password, $ilo_version){
		$data = array(
			'ilo_address' => $ilo_address,
			'ilo_username' => $ilo_username,
			'ilo_password' => $ilo_password,
			'ilo_version' => $ilo_version,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('ilo', $data);
	}

	public function insert_ibm($id_server, $imm_address, $imm_username, $imm_password)
	{
		$data = array(
			'imm_address' => $imm_address,
			'imm_username' => $imm_username,
			'imm_password' => $imm_password,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('imm', $data);
	}

	public function insert_ucs($id_server, $ucs_address, $ucs_username, $ucs_password)
	{
		$data = array(
			'ucs_address' => $ucs_address,
			'ucs_username' => $ucs_username,
			'ucs_password' => $ucs_password,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('ucs', $data);
	}

	public function insert_wol($id_server, $mac_address)
	{
		
		$data = array(
			'mac_address' => $mac_address,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('wol', $data);
	}

	public function insert_proxmox($id_server, $id_server_parent, $vmid, $jenis)
	{
		$data = array(
			'jenis' => $jenis,
			'id_vm' => $vmid,
			'id_server_parent' => $id_server_parent,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('proxmox', $data);
	}

	public function update_urutan()
	{
		$order_by = array('id_urutan_sop', 'asc');
		$where_ = array(array('id_urutan_sop !=','0'));
		$data_server = $this->M_crud->get_data('server', $where_, $order_by );
		$count = 1;
		// echo "<pre>";
		// print_r($data_server);
		// echo "</pre>";
		foreach ($data_server as $data_server) {
			$id_server = $data_server->id;
			$where = array(array('id', $id_server));
			$data = array('id_urutan_sop' => $count);
			$this->M_crud->update_data('server', $data, $where);
			$count++;
		}
	}


	public function import_general()
	{
		if($this->input->post('submit')){
			$fileName = time() . $_FILES['fileImport']['name'];    
			$config['upload_path'] = './fileExcel/';                                // Buat folder dengan nama "fileExcel" di root folder
			$config['file_name'] = $fileName;
			$config['allowed_types'] = 'xls|xlsx|csv';
			$config['max_size'] = 10000;
			$this->upload->initialize($config);
			if( ! $this->upload->do_upload('fileImport')) {
				$data = array('error' => $this->upload->display_errors()); 
			}else { 
				$data = array('upload_data' => $this->upload->data()); 
			} 
			$inputFileName = './fileExcel/'.$data['upload_data']['file_name'];
			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array                 
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
				
				if ($rowData[0][7] == 0) {
						$urutan = '0';
				}else {
					$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
					if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
				}
				

				$data2 = array(                                                      // Sesuaikan sama nama kolom tabel di database
					"server_hostname" => $rowData[0][0],
					"server_address" => $rowData[0][1],
					"server_mac_address" => $rowData[0][2],
					"server_ssh_port" => $rowData[0][3],
					"server_user" => $rowData[0][4],
					"server_password" => $this->Crypt->en($rowData[0][5]),
					"id_server_category" => $rowData[0][6],
					"id_urutan_sop" => $urutan,
					'id_method_poweroff' =>$rowData[0][8],
					'id_method_poweron' => $rowData[0][9],
					'id_jenis_server' => '0',
					'id_urutan_sop' => $urutan,
					'status' => $rowData[0][7], // nda jelaspi ini
					'id_jenis_method' => '1', 
					'server_status' => '0'
				);

				$this->M_crud->insert_data('server', $data2, $req = TRUE);
				
				
				// print_r($data);
				// $insert = $this->db->insert("data_orang", $data);                   // Sesuaikan nama dengan nama tabel untuk melakukan insert data
				// delete_files($media['file_path']);                                  // menghapus semua file .xls yang diupload
			}
			delete_files($data['upload_data']['file_path']);  
			$this->session->set_flashdata('success','success|Insert Data Success');
			redirect(base_url()."manservers/addgeneral",'refresh');
			
		}else {
			echo "not post";
		}

		// if ($this->input->post('tes')) {
		// 	echo "post ada";
		// } else {
		// 	echo "post tidak ada";
		// }
		
		
	}

	public function import_ilo()
	{
		if($this->input->post('submit')){
			$fileName = time() . $_FILES['fileImport']['name'];    
			$config['upload_path'] = './fileExcel/';                                // Buat folder dengan nama "fileExcel" di root folder
			$config['file_name'] = $fileName;
			$config['allowed_types'] = 'xls|xlsx|csv';
			$config['max_size'] = 10000;
			$this->upload->initialize($config);
			if( ! $this->upload->do_upload('fileImport')) {
				$data = array('error' => $this->upload->display_errors()); 
			}else { 
				$data = array('upload_data' => $this->upload->data()); 
			} 
			$inputFileName = './fileExcel/'.$data['upload_data']['file_name'];
			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array                 
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
				
				if ($rowData[0][7] == 0) {
						$urutan = '0';
				}else {
					$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
					if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
				}
				

				$data2 = array(                                                      // Sesuaikan sama nama kolom tabel di database
					"server_hostname" => $rowData[0][0],
					"server_address" => $rowData[0][1],
					"server_mac_address" => $rowData[0][2],
					"server_ssh_port" => $rowData[0][3],
					"server_user" => $rowData[0][4],
					"server_password" => $this->Crypt->en($rowData[0][5]),
					"id_server_category" => $rowData[0][6],
					"id_urutan_sop" => $urutan,
					'id_method_poweroff' =>$rowData[0][8],
					'id_method_poweron' => $rowData[0][9],
					'id_jenis_server' => '0',
					'id_urutan_sop' => $urutan,
					'status' => $rowData[0][7], // nda jelaspi ini
					'id_jenis_method' => '1', 
					'server_status' => '0'
				);

				$insert_id_server = $this->M_crud->insert_data('server', $data2, $req = TRUE);
				// echo $insert_id_server;

				$data_ilo = array(
					'ilo_address' =>  $rowData[0][10],
					'ilo_username' =>  $rowData[0][11],
					'ilo_password' => $rowData[0][12],
					'ilo_version' =>  $rowData[0][13],
					'ilo_status' => "0",
					'id_server' => $insert_id_server
				);

				$this->M_crud->insert_data('ilo', $data_ilo);
				
				
				// print_r($data);
				// $insert = $this->db->insert("data_orang", $data);                   // Sesuaikan nama dengan nama tabel untuk melakukan insert data
				// delete_files($media['file_path']);                                  // menghapus semua file .xls yang diupload
			}
			delete_files($data['upload_data']['file_path']); 
			$this->session->set_flashdata('success');
			redirect(base_url()."manservers/addilo",'refresh');
			
		}else {
			echo "not post";
		}

		// if ($this->input->post('tes')) {
		// 	echo "post ada";
		// } else {
		// 	echo "post tidak ada";
		// }
		
		
	}
	

}
