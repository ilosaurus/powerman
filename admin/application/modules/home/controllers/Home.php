<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('MENU_AKTIF', 'home');

class Home extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->require_login(TRUE);
	}

	public function index()
	{
		$data['active_nav'] =  MENU_AKTIF;
		$this->load->view('header');
		$this->load->view('sidebar', $data);
		$this->load->view('V_home');
		$this->load->view('footer');
	}

	
}
