<script src="<?php echo base_url() ?>assets/js/jput-2.js"></script>
<style>
#g1, #g2, #g3 {
        height:240px;
        display: inline-block;
        margin-top: -70px;
}

#g4, #g5 {
        height:180px;
        display: inline-block;
        margin-top: -55px;
       
}
thead, th, tbody {text-align: center;}
</style>
    
<section class="content">
    <div class="row m-b-0 m-t-0">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="bg-red header">
                    <h2>UPS Information</h2>
                </div>
                <div class="body m-b-0 m-t-0 p-t-0 p-b-0">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <h4 class="font-bold">APC MGE Galaxy 3500</h4>
                            <h4>UPS Status <span id="status_label" class="label label-success">Online</span></h4>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <center>
                                <h4 class="m-t-0 bold">Input Voltage (PLN)</h4>
                                <div  class="m-b-0" id="g4"></div>
                            </center>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                           <center>
                                <h4 class="m-t-0 bold">Output Voltage</h4>
                                <div class="m-b-0" id="g5"></div>
                           </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row m-b-0 m-t-0">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="card ">
                <div class="bg-red header">
                    <h2>UPS Load</h2>
                </div>
                <div class="body m-b-0 m-t-0 p-t-0 p-b-0">
                    <center>
                        <div id="g1"></div>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-blue-grey">
                    <h2>Battery Charge</h2>
                </div>
                <div class="body">
                    <center>
                        <div id="g2"></div>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-light-green">
                    <h2>UPS Runtime</h2>
                </div>
                <div class="body">
                    <center>
                        <div id="g3"></div>
                    </center>
                </div>
            </div>
        </div>
    </div>

    <div class="block-header">
        <h2>DATA ANALYTICS</h2>
    </div>
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-light-green">
                <div class="m-b--35 font-bold">LATEST POWER ONLINE</div>
                    <ul class="dashboard-stat-list">
                        <li>
                            TODAY
                            <span id="pw_on_today" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST WEEK
                            <span id="pw_on_lw" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            THIS MONTH
                            <span id="pw_on_cm" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST MONTH
                            <span id="pw_on_lm" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-red">
                <div class="m-b--35 font-bold">LATEST POWER ONBATTERY</div>
                    <ul class="dashboard-stat-list">
                        <li>
                            TODAY
                            <span id="pw_of_today" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST WEEK
                            <span id="pw_of_lw" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            THIS MONTH
                            <span id="pw_of_cm" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST MONTH
                            <span id="pw_of_lm" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>   
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-brown">
                <div class="m-b--45 m-t--5 font-bold">
                <select id="spec_month" onchange="get_spec_month(this.value)" class="form-control "></select>
                </div>
                    <ul class="dashboard-stat-list">
                        <li>
                            ONLINE/ONBATT
                            <span id="spec_month_on" class="pull-right"><b>--/--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            POWER DOWN (<a data-toggle="modal" data-target="#modal_detail_power" style="color:white">detail</a>)
                            <span id="power_down" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            AVG UPS LOAD
                            <span id="avg_load_capacity" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            AVG UPS RUNTIME
                            <span id="avg_battery_runtime" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-grey">
                <div class="m-b--35 font-bold">INPUT VOLTAGE AVERAGE (Volt) </div>
                    <ul class="dashboard-stat-list">
                        <li>
                            TODAY
                            <span id="avg_input_cd" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST WEEK
                            <span id="avg_input_lw" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            THIS MONTH
                            <span id="avg_input_cm" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST MONTH
                            <span  id="avg_input_lm"  class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-blue-grey">
                <div class="m-b--35 font-bold">RUNTIME AVERAGE (Minutes)</div>
                    <ul class="dashboard-stat-list">
                        <li>
                            TODAY
                            <span id="avg_runtime_cd" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST WEEK
                            <span id="avg_runtime_lw" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            THIS MONTH
                            <span id="avg_runtime_cm" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST MONTH
                            <span id="avg_runtime_lm" class="pull-right"><b>--</b> <small>Loading</small></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-black">
                <div class="m-b--35 font-bold">LOAD AVERAGE (%)</div>
                    <ul class="dashboard-stat-list">
                        <li>
                            TODAY
                            <span id="avg_cd" class="pull-right"><b>-</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST WEEK
                            <span id="avg_lw" class="pull-right"><b>-</b> <small>Loading</small></span>
                        </li>
                        <li>
                            THIS MONTH
                            <span id="avg_cm"  class="pull-right"><b>-</b> <small>Loading</small></span>
                        </li>
                        <li>
                            LAST MONTH
                            <span  id="avg_lm" class="pull-right"><b>-</b> <small>Loading</small></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <br>
</section>

<!-- Large Size -->
<div class="modal fade" id="modal_detail_power" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Power Down Detail</h4>
            </div>
            <div class="modal-body">
               <table id="table_detail_power" class="table table-responsive table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Durasi </th>
                            <th>Dari </th>
                            <th>Sampai </th>
                        </tr>
                    </thead>
                    <tbody id="tbody_modal"></tbody>
               </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<script>
   
    var g1, g2, g3, g4, datanya;
    $(document).ready(function(){
        // var jPut.detail.data = [{"waktu":"2018-03-24 15:25:02","jumlah_row":41,"menit":205,"waktu_akhir":"2018-03-24 18:50:02"},{"waktu":"2018-03-31 03:10:01","jumlah_row":34,"menit":170,"waktu_akhir":"2018-03-31 06:00:01"}];
 
        var g1 = new JustGage({
            id: "g1",
            value: 0,
            decimals : 1,
            min: 0,
            max: 100,
            label: "%",
            relativeGaugeSize: true
        });


        var g4 = new JustGage({
            id: "g4",
            value: 0,
            decimals : 1,
            min: 0,
            max: 240,
            label: "Volts",
            relativeGaugeSize: true,
            customSectors: [
                { color : "red", lo : 0, hi : 50 },
                { color : "yellow", lo : 50, hi : 150 },
                { color: "#b2d509", lo : 150, hi: 240 }]
        });

        var g5 = new JustGage({
            id: "g5",
            value: 0,
            decimals : 1,
            min: 0,
            max: 240,
            label: "Volts",
            relativeGaugeSize: true,
            customSectors: [
                { color : "red", lo : 0, hi : 50 },
                { color : "yellow", lo : 50, hi : 150 },
                { color: "#b2d509", lo : 150, hi: 240 }]
        });

        var g2 = new JustGage({
            id: "g2",
            value: 0,
            decimals : 1,
            min: 0,
            max: 100,
            label: "%",
            relativeGaugeSize: true,
            customSectors: [
                { color : "red", lo : 0, hi : 50 },
                { color : "yellow", lo : 51, hi : 70 },
                { color: "#b2d509", lo : 70, hi: 100 }]
        });

        var g3 = new JustGage({
            id: "g3",
            value: 0,
            decimals : 1,
            min: 0,
            max: 200,
            label: "Minutes",
            relativeGaugeSize: true,
            customSectors: [
                { color : "red", lo : 0, hi : 70 },
                { color : "yellow", lo : 70, hi : 150 },
                { color: "#b2d509", lo : 150, hi: 200 }]
        });
        
        setInterval(function(){
            $.get("<?php echo base_url() ?>data_ups/load/", function(data, status){
                datanya = data;
                var data_array = datanya.split("|");
                var TIMELEFT = data_array[0];
                var BCHARGE = data_array[1];
                var LOADPCT = data_array[2];
                var LINEV = data_array[3];
                var OUTPUTV = data_array[4];
                var STATUS = data_array[5];
                g1.refresh(LOADPCT);
                g2.refresh(BCHARGE);
                g3.refresh(TIMELEFT);
                g4.refresh(LINEV);
                g5.refresh(OUTPUTV);
                $('#status_label').html(STATUS);
            });
        }, 1800);

        setTimeout(function () { 
        location.reload();
        }, 122500);

        loop_select()
        get_average();
        set_power_today(); 
        get_last_week();
        get_this_month();
        get_last_month();
        
        // get_last_week();
        

    });

    function showmodaldetail() {
        // $.get("<?php echo base_url() ?>data_ups/poweronbatt/"+bulan+"/"+tahun, function(data_poweronbatt, status){ 
        //     var pw_onbatt = JSON.parse(data_poweronbatt);
        //     var count_pw_onbatt = pw_onbatt.length; 
        //     console.log();
            
        //     // $( "#power_down" ).html("<b>"+count_pw_onbatt.toFixed(0)+"</b> Times");

        // });
    }

    function loop_select() {
        var d = new Date();
        var bulan_skrg = d.getMonth();
        var tahun = d.getFullYear();
        var Month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        for (i = 0; i < Month.length; i++) {
            var opt = document.createElement("option");
            if (bulan_skrg == i ) {
                document.getElementById("spec_month").innerHTML += '<option selected value="' + i + '">' + Month[i] + '</option>';
            } else {
                document.getElementById("spec_month").innerHTML += '<option value="' + i + '">' + Month[i] + '</option>';
            }

        }
    }

    function get_spec_month(masukan_bulan) {
        var d = new Date();
        var bulan_skrg = d.getMonth();
        var tahun = d.getFullYear();
        var total_onbatt = 0;
        var total_online = 0;
        var total_hour_online = 0;
        var total_hour_onbatt = 0;
        var month = parseInt(masukan_bulan);
        var bulan = month + 1;
        var result_1;
        $("#spec_month_on" ).empty();
        $("#spec_month_on" ).html("<b>--</b> Loading");
        if (bulan_skrg == masukan_bulan) {
            $.get("<?php echo base_url() ?>data_ups/currmonth/", function(data, status){
                var total_currmonth_online = 0; 
                var total_currmonth_onbatt = 0;
                var resultcurrmonth = JSON.parse(data);
                var i = 0;
                for (i; i < resultcurrmonth.length; i++) {
                    total_currmonth_online = total_currmonth_online + resultcurrmonth[i].ONLINE;
                    total_currmonth_onbatt = total_currmonth_onbatt + resultcurrmonth[i].ONBATT;
                }
                var online_currmont = (total_currmonth_online*5)/60;
                var onbatt_currmont = (total_currmonth_onbatt*5)/60;
                $( "#spec_month_on" ).html("<b>"+online_currmont.toFixed(1)+" / "+onbatt_currmont.toFixed(1)+"</b> Hours");
                
                i = 0;
                total_currmonth_online = 0; 
                total_currmonth_onbatt = 0;
                online_currmont = 0;
                onbatt_currmont = 0
            });            
        } else {
            $.get("<?php echo base_url() ?>data_ups/timemonth/"+bulan+"/"+tahun, function(data, status){
                var total_specmonth_online = 0; 
                var total_specmonth_onbatt = 0;
                var resultspecmonth = JSON.parse(data);
                var i = 0;
                for (i; i < resultspecmonth.length; i++) {
                    total_specmonth_online = total_specmonth_online + resultspecmonth[i].ONLINE;
                    total_specmonth_onbatt = total_specmonth_onbatt + resultspecmonth[i].ONBATT;
                }    
                var online_specmonth = (total_specmonth_online*5)/60;
                var onbatt_specmonth = (total_specmonth_onbatt*5)/60;
                $( "#spec_month_on" ).html("<b>"+online_specmonth.toFixed(1)+" / "+onbatt_specmonth.toFixed(1)+"</b> Hours");
                total_specmonth_online = 0; 
                total_specmonth_onbatt = 0;
                online_specmonth = 0;
                onbatt_specmonth = 0;
                i = 0;
            });
        }
        $.get("<?php echo base_url() ?>data_ups/get_average_spec/"+bulan+"/"+tahun, function(data_average, status){ 
            var data_avg = JSON.parse(data_average);
            var avg_battery_capacity = data_avg.battery_capacity;
            var avg_battery_runtime = data_avg.battery_runtime;
            var power_down = data_avg.input_voltage;
            var avg_load_capacity = data_avg.load_capacity;
            
            $( "#avg_load_capacity" ).html("<b>"+avg_load_capacity.toFixed(1)+"</b> %");
            $( "#avg_battery_runtime" ).html("<b>"+avg_battery_runtime.toFixed(1)+"</b> Min");
            $( "#avg_battery_capacity" ).html("<b>"+avg_battery_capacity.toFixed(1)+"</b> %");
        });

        $.get("<?php echo base_url() ?>data_ups/poweronbatt/"+bulan+"/"+tahun, function(data_poweronbatt, status){ 
            $( "#power_down" ).html("<b>--</b> Times");
            var pw_onbatt = JSON.parse(data_poweronbatt);
            var count_pw_onbatt = pw_onbatt.length; 
            $( "#power_down" ).html("<b>"+count_pw_onbatt.toFixed(0)+"</b> Times");
            $("#tbody_modal").empty();
            var table = $("#table_detail_power tbody");
            var cnt = 1;
            $.each(pw_onbatt, function(idx, elem){
                cnt = cnt + idx;
                table.append("<tr><td>"+cnt+"</td><td>"+elem.menit+" Menit</td>  <td>"+elem.waktu+"</td> <td>"+elem.waktu_akhir+"</td></tr>");
                
            });
        });
    }

    function get_average() {
        var url_load_capacity = "<?php echo base_url() ?>data_ups/get_average/load_capacity";
        var url_runtime = "<?php echo base_url() ?>data_ups/get_average/battery_runtime";
        var url_input_voltage = "<?php echo base_url() ?>data_ups/get_average/input_voltage";
        $( "#avg_cd, #avg_lw, #avg_cm, #avg_lm, #avg_input_cd ,#avg_input_lw ,#avg_input_cm, #avg_input_lm, #avg_runtime_cd, #avg_runtime_lw, #avg_runtime_cm, #avg_runtime_lm " ).empty();
        $.get(url_load_capacity, function(data, status){
            var datas = JSON.parse(data);
            var avg_today = datas.avg_cd;
            var avg_lw = datas.avg_lw;
            var avg_cm = datas.avg_cm;
            var avg_lm = datas.avg_lm;
            // #avg_input_cd ,#avg_input_lw ,#avg_input_cm, #avg_input_lm
            $( "#avg_cd" ).html("<b>"+avg_today.toFixed(1)+"</b> %");
            $( "#avg_lw" ).html("<b>"+avg_lw.toFixed(1)+"</b> %");
            $( "#avg_cm" ).html("<b>"+avg_cm.toFixed(1)+"</b> %");
            $( "#avg_lm" ).html("<b>"+avg_lm.toFixed(1)+"</b> %");
        });

        $.get(url_runtime, function(data, status){
            var data_runtime = JSON.parse(data);
            var avg_runtime_cd = data_runtime.avg_cd;
            var avg_runtime_lw = data_runtime.avg_lw;
            var avg_runtime_cm = data_runtime.avg_cm;
            var avg_runtime_lm = data_runtime.avg_lm;
            // #avg_input_cd ,#avg_input_lw ,#avg_input_cm, #avg_input_lm
            $( "#avg_runtime_cd" ).html("<b>"+avg_runtime_cd.toFixed(1)+"</b> Minutes");
            $( "#avg_runtime_lw" ).html("<b>"+avg_runtime_lw.toFixed(1)+"</b> Minutes");
            $( "#avg_runtime_cm" ).html("<b>"+avg_runtime_cm.toFixed(1)+"</b> Minutes");
            $( "#avg_runtime_lm" ).html("<b>"+avg_runtime_lm.toFixed(1)+"</b> Minutes");
        });

        $.get(url_input_voltage, function(data, status){
            var data_input = JSON.parse(data);
            var avg_input_cd = data_input.avg_cd;
            var avg_input_lw = data_input.avg_lw;
            var avg_input_cm = data_input.avg_cm;
            var avg_input_lm = data_input.avg_lm;
            // #avg_input_cd ,#avg_input_lw ,#avg_input_cm, #avg_input_lm
            $( "#avg_input_cd" ).html("<b>"+avg_input_cd.toFixed(1)+"</b> Volt");
            $( "#avg_input_lw" ).html("<b>"+avg_input_lw.toFixed(1)+"</b> Volt");
            $( "#avg_input_cm" ).html("<b>"+avg_input_cm.toFixed(1)+"</b> Volt");
            $( "#avg_input_lm" ).html("<b>"+avg_input_lm.toFixed(1)+"</b> Volt");
        });

        
    }

    function get_last_month() {
        var d = new Date();
        var bulan_skrg = d.getMonth()+1;
        var bulan_lalu = d.getMonth();
        var tahun = d.getFullYear();
        var total_online = 0;
        var total_onbatt = 0;
        var i = 0;
        var total_hour_online, total_hour_onbatt;        
        $.get("<?php echo base_url() ?>data_ups/timemonth/"+bulan_lalu+"/"+tahun, function(data, status){
            $("#pw_of_lm, #pw_on_lm" ).empty();
           var result_power = JSON.parse(data); 
           for (i; i < result_power.length; i++) {
                total_online = total_online + result_power[i].ONLINE;
                total_onbatt = total_onbatt + result_power[i].ONBATT;
           }     
           total_hour_online = (total_online*5)/60;
           total_hour_onbatt = (total_onbatt*5)/60;
           $( "#pw_of_lm" ).html("<b>"+total_hour_onbatt.toFixed(1)+"</b> Hours");
           $( "#pw_on_lm" ).html("<b>"+total_hour_online.toFixed(1)+"</b> Hours"); 
        });

        $.get("<?php echo base_url() ?>data_ups/get_average_spec/"+bulan_skrg+"/"+tahun, function(data_average, status){ 
            var data_avg = JSON.parse(data_average);
            var avg_battery_capacity = data_avg.battery_capacity;
            var avg_battery_runtime = data_avg.battery_runtime;
            var avg_load_capacity = data_avg.load_capacity;
            $( "#avg_load_capacity" ).html("<b>"+avg_load_capacity.toFixed(1)+"</b> %");
            $( "#avg_battery_runtime" ).html("<b>"+avg_battery_runtime.toFixed(1)+"</b> Min");
            $( "#avg_battery_capacity" ).html("<b>"+avg_battery_capacity.toFixed(1)+"</b> %");
        });

        $.get("<?php echo base_url() ?>data_ups/poweronbatt/"+bulan_skrg+"/"+tahun, function(data_poweronbatt, status){ 
            var pw_onbatt = JSON.parse(data_poweronbatt);
            var count_pw_onbatt = pw_onbatt.length; 
            $( "#power_down" ).html("<b>"+count_pw_onbatt.toFixed(0)+"</b> Times");
            $("#tbody_modal").empty();
            var table = $("#table_detail_power tbody");
            var cnt = 1;
            $.each(pw_onbatt, function(idx, elem){
                cnt = cnt + idx;
                table.append("<tr><td>"+cnt+"</td><td>"+elem.menit+" Menit</td>  <td>"+elem.waktu+"</td> <td>"+elem.waktu_akhir+"</td></tr>");
                
            });
        });
    }

    

    function get_this_month() {
        $("#pw_of_cm, #pw_on_cm" ).empty();
        $.get("<?php echo base_url() ?>data_ups/currmonth/", function(data, status){
           var result_power = JSON.parse(data);
           var total_onbatt = 0;
           var total_online = 0;
           var i = 0;
           var total_hour_online, total_hour_onbatt;
           for (i; i < result_power.length; i++) {
                total_online = total_online + result_power[i].ONLINE;
                total_onbatt = total_onbatt + result_power[i].ONBATT;
           }
           total_hour_online = (total_online*5)/60;
           total_hour_onbatt = (total_onbatt*5)/60;
           $( "#pw_on_cm" ).html("<b>"+total_hour_online.toFixed(1)+"</b> Hours");
           $( "#pw_of_cm" ).html("<b>"+total_hour_onbatt.toFixed(1)+"</b> Hours");
           $( "#spec_month_on" ).html("<b>"+total_hour_online.toFixed(1)+"/"+total_hour_onbatt.toFixed(1)+"</b> Hours");
        });

        
    }

    function get_last_week() {
        $.get("<?php echo base_url() ?>data_ups/get_last_week/", function(data, status){
            $("#pw_of_lw, #pw_on_lw" ).empty();
           var result_power = JSON.parse(data);
           var lw_online = result_power.ONLINE/60;
           var lw_onbatt = result_power.ONBATT/60;
           $( "#pw_of_lw" ).html("<b>"+lw_onbatt.toFixed(1)+"</b> Hours");
           $( "#pw_on_lw" ).html("<b>"+lw_online.toFixed(1)+"</b> Hours");
        });
    }

    function set_power_today() {
        $( "#pw_of_today, #pw_on_today" ).empty();
        $.get("<?php echo base_url() ?>data_ups/currday/", function(data, status){
           var result_power = JSON.parse(data);
           var today_online = result_power.ONLINE/60;
           var today_onbatt = result_power.ONBATT/60;
           $( "#pw_of_today" ).html("<b>"+today_onbatt.toFixed(1)+"</b> Hours");
           $( "#pw_on_today" ).html("<b>"+today_online.toFixed(1)+"</b> Hours");
        });
    }

</script>