<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->load->library('pagination');
	}

	public function index()
	{
		redirect(base_url().'error','refresh');
	}

	public function p($url = null)
	{
		if ($url != null) {
			$where = array(array('url',$url));
			$cek_url = $this->M_crud->cek_data('man_layanan', $where);
			if ($cek_url) {
				$data['modul'] = 'Layanan';
				$data['title'] = $this->M_crud->get_row('man_layanan','judul', $where);
				$data['waktu'] = $this->M_crud->get_row('man_layanan','upload_date', $where);
				$data['data_layanan'] = $this->M_crud->get_data('man_layanan', $where);
				$this->load->view('header');
				$this->load->view('V_layanan_read', $data);
				// $this->load->view('recentnews');
				$this->load->view('footer');
			}else {
				redirect(base_url().'error','refresh');
			}
		}else {
			redirect(base_url().'error','refresh');
		}
	}






}
