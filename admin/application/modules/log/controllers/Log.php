<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('MENU_AKTIF', 'log');

class Log extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->load->library('datatables');
		$this->load->model('M_log','system_log');
		// $this->require_login(TRUE);
	}

	public function index(){
		redirect(base_url().'log/autoscript','refresh');
	}

	public function autoscript()
	{
		$data2['active_nav'] =  "log_autoscript";
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_log_autoscript');
		$this->load->view('footer');
	}

	public function scriptstatus()
	{
		$data2['active_nav'] =  "log_autoscript";
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_log_scriptstatus');
		$this->load->view('footer');
	}

	public function upsstatus()
	{
		$data2['active_nav'] =  "log_autoscript";
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_log_upsstatus');
		$this->load->view('footer');
	}

	public function notif()
	{
		$data2['active_nav'] =  "log_notif";
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_log_notif');
		$this->load->view('footer');
	}

	public function get_data_all(){

	  $draw = $_REQUEST['draw'];
	  $length = $_REQUEST['length'];
	  $start = $_REQUEST['start'];
	  $search = $_REQUEST['search']["value"];
	  $total = $this->db->count_all_results("system_log");
	  $output = array();
	  $output['draw'] =  $draw;
	  // $output['recordsTotal'] adalah total data sebelum difilter, $output['recordsFiltered'] adalah total data ketika difilter, Biasanya kedua duanya bernilai sama, maka kita assignment , keduaduanya dengan nilai dari $total
	  $output['recordsTotal']=$output['recordsFiltered']=$total;
	  /*disini nantinya akan memuat data yang akan kita tampilkan 
	  pada table client*/
	  $output['data']=array();
	  /*Jika $search mengandung nilai, berarti user sedang telah 
	  memasukan keyword didalam filed pencarian*/
	  if($search!=""){
		$this->db->like("log",$search);
	  }
  
	  /*Lanjutkan pencarian ke database*/
	  $this->db->limit($length,$start);
	  /*Urutkan dari alphabet paling terkahir*/
	  $this->db->where('log_type','AUTOSCRIPT');
	  $this->db->order_by('id','ASC');
	  $query=$this->db->get('system_log');
  
	  /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
	  dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
	  yang mengandung keyword tertentu
	  */
	  if($search!=""){
		$this->db->like("log",$search);
		$jum=$this->db->get('system_log');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
	  }
  
	  $nomor_urut=$start+1;
	  foreach ($query->result_array() as $key => $value) {
		$output['data'][$key]=$value;
		$nomor_urut++;
	  }
	  echo json_encode($output);
	}

	public function get_data_detail(){
		if ($this->input->post("id")) {
			$id = $this->input->post("id");
			$where = array(
				array("id", "$id")
			);
			$get_log = $this->M_crud->get_row('system_log','log', $where);
			$datetime = $this->M_crud->get_row('system_log','datetime', $where);
			$log_type = $this->M_crud->get_row('system_log','log_type', $where);
			$data_log_array = explode("|", $get_log);
			if ($data_log_array[0] == "POWERON") {
					$string1 = "Powering On";
			}else if($data_log_array[0] == "POWEROFF") {
					$string1 = "Powering Off";
			}
			$message = 	$datetime." - ".$string1." ".$data_log_array[1]." With ".strtoupper($data_log_array[2]);
			$message2 = "LOG TYPE - ".$log_type;
			echo "success|".$message."|".$message2;
		} else {
			echo "error|SomethingError";
		}
		
	}

	public function get_data_detail_general(){
		if ($this->input->post("id")) {
			$id = $this->input->post("id");
			$where = array(
				array("id", "$id")
			);
			$get_log = $this->M_crud->get_row('system_log','log', $where);
			$datetime = $this->M_crud->get_row('system_log','datetime', $where);
			$log_type = $this->M_crud->get_row('system_log','log_type', $where);
			$message = 	$datetime." - ".$get_log;
			$message2 = "LOG TYPE - ".$log_type;
			echo "success|".$message."|".$message2;
		} else {
			echo "error|SomethingError";
		}
		
	}

	public function get_data_all2(){
		$draw = $_REQUEST['draw'];
		$length = $_REQUEST['length'];
		$start = $_REQUEST['start'];
		$search = $_REQUEST['search']["value"];
		$this->db->from('system_log');
		$this->db->where("log_type","AUTOSCRIPT");
		$total = $this->db->count_all_results();
		$output = array();
		$output['draw'] =  $draw;
		// $output['recordsTotal'] adalah total data sebelum difilter, $output['recordsFiltered'] adalah total data ketika difilter, Biasanya kedua duanya bernilai sama, maka kita assignment , keduaduanya dengan nilai dari $total
		$output['recordsTotal']=$output['recordsFiltered']=$total;
		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();
		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		  $this->db->like("log",$search);
		}
	
		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->where('log_type','AUTOSCRIPT');
		$this->db->order_by('datetime','desc');
		$query=$this->db->get('system_log');
	
		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		  $this->db->like("log",$search);
		  $jum=$this->db->get('system_log');
		  $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		$output_db = $query->result_array();
		// echo "<pre>";
		// print_r($output_db);
		// echo "</pre>";
	
		$nomor_urut=$start+1;
		foreach ($output_db as $key => $value) {
		  $output['data'][$key]=$value;
		//   echo "<pre>";
		//   print_r($output['data'][$key];
		//   echo "</pre>";
			// echo $key;
			$data_log = $value['log'];
			$data_log_array = explode("|", $data_log);
			// echo $data_log_array[0];
			// echo $data_log_array[1];
			// echo $data_log_array[2];

			if ($data_log_array[0] == "POWERON") {
					$string1 = "Powering On";
			}else if($data_log_array[0] == "POWEROFF") {
					$string1 = "Powering Off";
			}

			$value['log'] = $string1." ".$data_log_array[1]." With ".strtoupper($data_log_array[2]);
			// echo $value['log'];
			$output['data'][$key]=$value;

			// print_r($value['log'];
		  // echo "<br>";
		  $nomor_urut++;
		}
		echo json_encode($output);
	}

	public function get_data_scriptstatus(){
		$draw = $_REQUEST['draw'];
		$length = $_REQUEST['length'];
		$start = $_REQUEST['start'];
		$search = $_REQUEST['search']["value"];
		$this->db->from('system_log');
		$this->db->where("log_type","SCRIPT_STATUS");
		$total = $this->db->count_all_results();
		$output = array();
		$output['draw'] =  $draw;
		$output['recordsTotal']=$output['recordsFiltered']=$total;
		$output['data']=array();
		if($search!=""){
		  $this->db->like("log",$search);
		}
		$this->db->limit($length,$start);
		$this->db->where('log_type','SCRIPT_STATUS');
		$this->db->order_by('datetime','desc');
		$query=$this->db->get('system_log');
		if($search!=""){
		  $this->db->like("log",$search);
		  $jum=$this->db->get('system_log');
		  $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}
		$output_db = $query->result_array();
	
		$nomor_urut=$start+1;
		foreach ($output_db as $key => $value) {
			$output['data'][$key]=$value;
			$nomor_urut++;
		  
		}
		echo json_encode($output);
	}

	public function get_data_upsstatus(){
		$draw = $_REQUEST['draw'];
		$length = $_REQUEST['length'];
		$start = $_REQUEST['start'];
		$search = $_REQUEST['search']["value"];
		$this->db->from('system_log');
		$this->db->where("log_type","UPS_STATUS");
		$total = $this->db->count_all_results();
		$output = array();
		$output['draw'] =  $draw;
		$output['recordsTotal']=$output['recordsFiltered']=$total;
		$output['data']=array();
		if($search!=""){
		  $this->db->like("log",$search);
		}
		$this->db->limit($length,$start);
		$this->db->where('log_type','UPS_STATUS');
		$this->db->order_by('datetime','desc');
		$query=$this->db->get('system_log');
		if($search!=""){
		  $this->db->like("log",$search);
		  $jum=$this->db->get('system_log');
		  $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}
		$output_db = $query->result_array();
	
		$nomor_urut=$start+1;
		foreach ($output_db as $key => $value) {
			$output['data'][$key]=$value;
			$nomor_urut++;
		  
		}
		echo json_encode($output);
	}

	public function get_data_notif(){
		$draw = $_REQUEST['draw'];
		$length = $_REQUEST['length'];
		$start = $_REQUEST['start'];
		$search = $_REQUEST['search']["value"];
		$total = $this->db->count_all_results("notif");
		$output = array();
		$output['draw'] =  $draw;
		$output['recordsTotal']=$output['recordsFiltered']=$total;
		$output['data']=array();
		if($search!=""){
		  $this->db->like("message",$search);
		}
		$this->db->limit($length,$start);
		$this->db->order_by('datetime','desc');
		$query=$this->db->get('notif');
		if($search!=""){
		  $this->db->like("message",$search);
		  $jum=$this->db->get('notif');
		  $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}
		$output_db = $query->result_array();
	
		$nomor_urut=$start+1;

		foreach ($output_db as $key => $value) {
			$id_admin = $value['id_admin'];
			$where = array(array("id", "$id_admin"));
			$nama_admin = $this->M_crud->get_row('admin','name', $where);
			$nomor_admin = $this->M_crud->get_row('admin','phone', $where);
			$value['admin'] = $nama_admin." (".$nomor_admin.")";

			$output['data'][$key]=$value;
			$nomor_urut++;
		  
		}
		echo json_encode($output);
	}

}
