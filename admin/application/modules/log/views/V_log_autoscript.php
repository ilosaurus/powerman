<!-- JQuery DataTable Css -->
<link href="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/jquery.dataTables.js"></script>
<!-- <script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/jquery.dataTables.min.js"></script> -->
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<!-- <link href="<?php echo base_url() ?>assets/backend/plugins/jquery-datatable/skin/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet"> -->

<style>
th.dt-center, td.dt-center { text-align: center; }
</style>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0 m-r-0 m-l-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="bg-red header">
                        <h2>Log - AutoScript</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table id="table" class="table dt-center table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Log</th>
                                        <th>Log Type</th>
                                        <th>Time</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>

<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title" id="defaultModalLabel">Log AutoScript Detail</h3>
         </div>
         
         <div class="modal-body">
            <form action="#" method="post">
                <div class="form-group">
                    <div class="form-line">
                        <textarea id="textarea1" rows="2" disabled class="form-control no-resize" class="form-control"></textarea>
                    </div>
                </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   

    $(document).ready(function() {
		var table = $('#table').DataTable({  
           "processing":true,  
           "StateSave":true, 
           "StateDuration":true, 
           "serverSide":true, 
           "searching" : {"regex":true} ,
           "ajax": "<?php echo base_url() ?>log/get_data_all2", 
           "success": function (response) {
               console.log(response);
           },
           "order":false,
           "bDestroy":true,
           "columns" : [
               {
                   "data":"",
                   render : function (data, type, row, meta) {
                       return meta.row + meta.settings._iDisplayStart + 1;
                   }
               },
               { "data":"log" },
               { "data":"log_type" },
               { "data": "datetime"},
            //    {
            //         "data": "id",
            //         "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
            //             $(nTd).html('<button type="button" onClick="get_data_detail(\''+oData.id+'\')" class="btn btn-block btn-sm btn-default waves-effect">Detail</button>');
            //         }
            //     }
           ],
           "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],
        });  
	});


    function get_data_detail(id) {
        // console.log(id);
        
        var id = id;
        $.ajax({
            url : "<?php echo base_url() ?>log/get_data_detail/",
            type : "POST",
            data : {
                id : id
            },
            beforeSend : function () {
                console.log("Lagi loading");
            },
            success : function(data){
                data_array = data.split("|");
                if (data_array[0] = "success") {
                    $('#modal_detail').modal('show');
                    $('#textarea1').html(""+data_array[1]+"\n"+""+data_array[2]);
                }else{
                    $('#modal_detail').modal('show');
                    $('#textarea1').html("Something Error");
                }
            },
            error : function () {
                $('#modal_detail').modal('show');
                $('#textarea1').html("Something Error");
            }
        });
    }
 
</script>