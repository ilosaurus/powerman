<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corner extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->load->library('pagination');
	}

	public function index()
	{
		//echo $url;
		redirect(base_url().'error','refresh');
	}

	public function p($url = null)
	{
		if ($url != null) {
			$where = array(array('url',$url));
			$cek_url = $this->M_crud->cek_data('man_corner', $where);
			if ($cek_url) {
				$data['modul'] = 'corner';
				$data['title'] = $this->M_crud->get_row('man_corner','judul', $where);
				$data['waktu'] = $this->M_crud->get_row('man_corner','upload_date', $where);
				$data['data_corner'] = $this->M_crud->get_data('man_corner', $where);
				$this->load->view('header');
				$this->load->view('V_corner_read', $data);
				$this->load->view('footer');
			}else {
				redirect(base_url().'error','refresh');
			}
		}else {
			redirect(base_url().'error','refresh');
		}
	}






}
