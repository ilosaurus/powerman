<style>
thead, th, tr, tbody {text-align: center;}
</style>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Power Control Method</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover ">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Method</th>
                                        <th>Method Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $count = 1;
                                        foreach ($data_method as $data) {
                                    ?>
                                    <tr>
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $data->metode ?></td>
                                        <td><?php echo $data->kode ?></td>
                                    </tr>
                                    <?php
                                       $count++;  }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>


