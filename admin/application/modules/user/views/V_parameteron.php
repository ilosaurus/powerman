<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Power Off Parameter</h2>
                    </div>
                    <style>
                        thead, th, tr, tbody {text-align: center;}
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <center><th class="center">No</th></center>
                                        <center><th>Parameter</th></center>
                                        <center><th>Nilai Parameter</th></center>
                                        <center><th>Status</th></center>
                                        <center><th>Aksi</th></center>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $count = 1;
                                        foreach ($data_parameter as $data ) {
                                    ?>
                                    <tr class="center">
                                        <center><td><?php echo $count ?></td></center>
                                        <center><td id="par_<?php  echo $count ?>" ><?php echo $data->parameter ?> </td></center>
                                        <center><td  ><?php echo $data->value_on ?> <?php echo $data->satuan ?></td></center>
                                        <?php
                                            if ($data->status_on == '1') {
                                                $label = "<span class='label label-success'>Enable</span>";
                                            } else {
                                                $label = "<span class='label label-warning'>Disable</span>";
                                            }
                                            
                                        ?>
                                        <center><td ><?php echo $label ?></td></center>
                                        <center>
                                            <td>
                                                <button type="button" onClick="modal_detail(<?php echo $count ?>)" class="btn bg-cyan btn-block btn-xs waves-effect">Edit</button>
                                            </td>
                                        </center>
                                    </tr>
                                    <input type="hidden" id="id_<?php  echo $count ?>" value="<?php  echo $data->id ?>">
                                    <input type="hidden" id="val_<?php  echo $count ?>" value="<?php  echo $data->value_on ?>">
                                    <input type="hidden" id="satuan_<?php  echo $count ?>" value="<?php  echo $data->satuan ?>">
                                    <input type="hidden" id="status_<?php  echo $count ?>" value="<?php  echo $data->status_on ?>">
                                    <input type="hidden" id="desc_<?php  echo $count ?>" value="<?php  echo $data->desc ?>">
                                    <?php
                                        $count++; }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>


<!-- Modal Section  -->
<!-- Default Size -->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="defaultModalLabel">Parameter Poweroff Detail</h4>
         </div>
         <hr>
         <div class="modal-body">
            <form action="<?php echo base_url() ?>admin/powercontrol/edit_parameter" method="post">
                <label >Parameter</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_parameter" class="form-control" disabled readonly value="">
                        <input type="hidden" id="modal_id_parameter" name="id_parameter" class="form-control" value="">
                    </div>
                </div>
                <label >Deskripsi Parameter</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_desc" class="form-control" disabled readonly value="">
                    </div>
                </div>
                <label >Nilai Parameter  </label> <span id="modal_satuan" ></span>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_nilai_par" class="form-control" value="">
                    </div>
                </div>
                <label >Status Parameter </label> 
                <div class="form-group">
                    <select id="modal_status" name="status" class="form-control show-tick">
                        <option selected value="1">Enable</option>
                    </select>
                </div>
            </form>
         </div>
         <div class="modal-footer">
            <a id="modal_button_edit" onClick="send_data()"  type="button" class="btn btn-link waves-effect" href="#">UPDATE</a>
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal Section -->

<script type="text/javascript">
function modal_detail(count) {
    var parameter = $("#par_"+count).html();
    var id = $("#id_"+count).val();
    var deskripsi = $("#desc_"+count).val();
    var value = $("#val_"+count).val();
    var satuan = $("#satuan_"+count).val();
    var status;

    if ($("#status_"+count).val() == 0) {
        status = 1; 
    } else {
        status = 1; 
    }
    
    $("#par_"+count).html();
    $("#modal_id_parameter").val(id);
    $("#modal_parameter").val(parameter);
    $("#modal_nilai_par").val(value);
    $("#modal_satuan").html("("+satuan+")");
    $("#modal_desc").val(deskripsi);
    $("#modal_status").val(status).trigger("change");
    $('#modal_detail').modal('show');
}

function send_data() {
    var id_parameter =  $("#modal_id_parameter").val();
    var value_parameter = $("#modal_nilai_par").val();
    var status = $("#modal_status").val();
    $('#modal_detail').modal('hide');
    $.ajax({
        url : "<?php echo base_url() ?>powercontrol/update_data_par_poweron/",
        type : "POST",
        data : {
            id_parameter : id_parameter,
            value_parameter : value_parameter,
            status_parameter : status
        },
        beforeSend : function () {
            console.log("Lagi loading");
            swal({
                title: "Harap Tunggu",
                type: "info",
                showCancelButton: false,
                closeOnConfirm: false,
            });
        },
        success : function(data){
            console.log("Success : "+data);
            data_array = data.split("|");
            swal({
                title: "Update Data Success",
                type: ""+data_array[0]+""
            },
            function() {
                location.reload();
            }
            );
        },
        error : function () {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
    });    
}
</script>


