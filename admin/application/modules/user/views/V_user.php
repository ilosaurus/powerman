<style>
    thead, th, tr, tbody {text-align: center;}
</style>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Administrator List </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown ">
                                <a href="javascript:void(0);" onClick="modal_add()" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                    <i class="material-icons">add</i>
                                </a>
                            </li>
                        </ul>

                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover ">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $count = 1;
                                        foreach ($data_user as $data_user) {
                                    ?>
                                    <tr>
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $data_user->name ?></td>
                                        <td><?php echo $data_user->username ?></td>
                                        <td><?php echo $data_user->email ?></td>
                                        <td><?php echo $data_user->phone ?></td>
                                        <td>
                                            <?php if($data_user->status == '0'){ ?>
                                            <div class="btn-group " disabled role="group" aria-label="Justified button group">
                                                <a href="javascript:void(0);" disabled class="btn btn-default btn-xs waves-effect" role="button">Edit</a>
                                                <a href="javascript:void(0);" disabled  class="btn btn-danger btn-xs waves-effect" role="button">Delete</a>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="btn-group " role="group" aria-label="Justified button group">
                                                <a href="javascript:void(0);" onClick="modal_edit('<?php echo $this->Crypt->en($data_user->id) ?>')" class="btn btn-default btn-xs waves-effect" role="button">Edit</a>
                                                <a href="javascript:void(0);" onClick="delete_user('<?php echo $this->Crypt->en($data_user->id) ?>')" class="btn btn-danger btn-xs waves-effect" role="button">Delete</a>
                                            </div>
                                            <?php } ?>
                                            
                                         </td>
                                    </tr>
                                    <?php
                                       $count++; }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>

<div class="modal fade" id="modal_add" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title" id="defaultModalLabel">Add User Administrator</h3>
         </div>
         
         <div class="modal-body">
            <form  id="form_add" method="post">
                <label >Username</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="username" placeholder="Enter your username" class="form-control" value="" required>
                    </div>
                </div>
                <label >Password</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="password" name="password"  placeholder="Enter your password" class="form-control" value="" required>
                    </div>
                </div>
                <label >Name</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="name" placeholder="Enter your name" class="form-control"value=""required>
                    </div>
                </div>
                <label >Email</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="email" placeholder="Enter your email" class="form-control"value=""required>
                    </div>
                </div>
                <label >Phone</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="form_add_phone" name="phone" placeholder="Enter your phone" class="form-control"value=""required>
                    </div>
                </div>
            
         </div>
         <div class="modal-footer">
            <button type="submit" role="submit" class="btn btn-link waves-effect" >SUBMIT</button>                      
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title" id="defaultModalLabel">Add User Administrator</h3>
         </div>
         
         <div class="modal-body">
            <form action="#" id="form_edit" method="post">
                <label >Username</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="form_edit_username" name="username" placeholder="Enter your username" class="form-control" value="" required>
                        <input type="hidden" id="form_edit_id" name="id"  class="form-control" value="" required>

                    </div>
                </div>
                <label >Password</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="password" id="form_edit_password" name="password" placeholder="Kosongkan Jika Tidak Ingin Mengubah Password" class="form-control"value="">
                    </div>
                </div>
                <label >Name</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="form_edit_name" name="name" placeholder="Enter your name" class="form-control"value=""required>
                    </div>
                </div>
                <label >Email</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="form_edit_email" name="email" placeholder="Enter your email" class="form-control"value=""required>
                    </div>
                </div>
                <label >Phone</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="form_edit_phone" name="phone" placeholder="Enter your phone" class="form-control"value=""required>
                    </div>
                </div>
            
         </div>
         <div class="modal-footer">
            <button type="submit" onclick="update_data()" role="submit" class="btn btn-link waves-effect" >SUBMIT</button>                      
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
         </form>
      </div>
   </div>
</div>


<script type="text/javascript">
    function modal_add() {
        $('#modal_add').modal('show');
        jQuery.validator.messages.required = "";
        $("#form_add").validate({
            submitHandler: function(form) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>user/add_data",
                    data: $("#form_add").serialize(),
                    beforeSend : function () {
                        $('#modal_add').modal('hide');
                        // console.log("Lagi loading");
                        swal({
                            title: "Harap Tunggu",
                            type: "info",
                            showCancelButton: false,
                            closeOnConfirm: false,
                        });
                    },
                    success: function(response) {
                        // console.log(response);
                        
                        data_array = response.split("|");
                        if (data_array[0] == 'error') {
                            swal("Cancelled", data_array[1], "error");
                            // $('#modal_add').modal('show');
                        } else {
                            swal({
                                title: ""+data_array[1]+"",
                                type: ""+data_array[0]+""
                            },
                                function() {
                                    location.reload();
                                }
                            );
                        }
                    },
                    error : function () {
                        swal("Cancelled", "Something", "error");
                    }
                });
            }
        });
    }

    function modal_edit(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>user/get_data_user",
            data: {
                id : id,
            },
            beforeSend : function () {
                console.log("Lagi loading");
            },
            success: function(response) {
                // console.log(response.name);
                var data_user = JSON.parse(response);
                $("#form_edit_id").val(data_user[0]['id']);
                $("#form_edit_username").val(data_user[0]['username']);
                $("#form_edit_name").val(data_user[0]['name']);
                
               
                $("#form_edit_email").val(data_user[0]['email']);
                $("#form_edit_phone").val(data_user[0]['phone']);
                $('#modal_edit').modal('show');
                console.log(data_user);
                console.log(data_user[0]['name']);
                
            },
            error : function () {
                console.log("Error gan");
            }
        });
        // $('#modal_edit').modal('show');
    }

    function update_data() {
        var id = $("#form_edit_id").val();
        jQuery.validator.messages.required = "";
        $("#form_edit").validate({
            submitHandler: function(form) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>user/update_data_user",
                    data: $("#form_edit").serialize(),
                    beforeSend : function () {
                        console.log("Lagi loading");
                        // swal({
                        //     title: "Harap Tunggu",
                        //     type: "info",
                        //     showCancelButton: false,
                        //     closeOnConfirm: false,
                        // });
                    },
                    success: function(response) {
                        // console.log(response);
                        
                        data_array_user_update = response.split("|");
                        console.log(data_array_user_update);
                        
                        if (data_array_user_update[0] == 'error') {
                            swal("Cancelled", data_array_user_update[1], "error");
                        } else {
                            swal({
                                title: ""+data_array_user_update[1]+"",
                                type: ""+data_array_user_update[0]+""
                            },
                                function() {
                                    location.reload();
                                }
                            );
                        }
                    },
                    error : function () {
                        swal("Cancelled", "Something", "error");
                    }
                });
            }
        });
    }

    function delete_user(id) {
        var id = id;
        swal({
            title: "Are you sure?",
            text: "Delete This User ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            $.ajax({
                url : "<?php echo base_url() ?>user/delete_data_user",
                type : "POST",
                data : {
                    id : id
                },
                beforeSend : function () {
                    swal({
                        title: "Harap Tunggu",
                        type: "info",
                        showCancelButton: false,
                        closeOnConfirm: false,
                    });
                },
                success : function(data){
                    data_array_delete = data.split("|");
                    console.log(data_array_delete);
                    swal({
                            title: "delete server",
                            text: data_array_delete[1],
                            type: ""+data_array_delete[0]+""
                        },
                        function() {
                            location.reload();
                        }
                    );
                },
                error : function () {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        });
    }

    $("#form_edit_phone, #form_add_phone").numeric({
        allowSpace: false, // Allow the space character
        allowUpper: false  // Allow Upper Case characters
    });
    
</script>


