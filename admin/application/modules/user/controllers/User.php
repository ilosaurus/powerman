<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('MENU_AKTIF', 'server');

class User extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->require_login(TRUE);
		$this->get_session();
	}

	public function index()
	{
		if ($_SESSION['admin_powerman']['status'] == 0) {
			$data2['active_nav'] =  "user";
			$data['data_user'] = $this->M_crud->get_data('admin');
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_user', $data);
			$this->load->view('footer');
		} else {
			redirect(base_url().'home','refresh');
		}
	}

	public function add_data()
	{
		if ($this->input->post("username")) {
			$username = $this->input->post("username");
			$where_username = array(array('username', $username));
			$cek_username = $this->M_crud->cek_data('admin', $where_username);
			if ($cek_username) {
				echo "error|Username Already Exist";
			} else {
				$data = array(
					'username' => $username,
					'password' => md5($this->input->post("password")),
					'name' => $this->input->post("name"),
					'email' => $this->input->post("email"),
					'phone' => $this->input->post("phone"),
					'status' => "1"
				);
				$this->M_crud->insert_data('admin', $data);
				echo "success|Insert Data Success";
			}
		}else {
			echo "error|Nothing Data POST";
		}
	}

	public function get_data_user()
	{
		if ($this->input->post("id")) {
			$id =  $this->Crypt->de($this->input->post("id"));
			$where = array(array('id', $id));
			$data = $this->M_crud->get_data('admin', $where);
			echo json_encode($data);
		}else {
			echo "error";
		}
	}


	public function update_data_user()
	{
		if ($this->input->post("id")) {
			$id = $this->input->post("id");
			$username = $this->input->post("username");
			$password = $this->input->post("password");
			$name = $this->input->post("name");
			$email = $this->input->post("email");
			$phone = $this->input->post("phone");

			$where = array(array('id', $id));
			$where_username = array(
				array('id !=', $id),
				array('and', 'username like', $username)
			);
			$password_lama = $this->M_crud->get_row('admin','password', $where);
			$cek_data_username = $this->M_crud->cek_data('admin', $where_username);

			if ($cek_data_username) {
				$status_username = "ada";

				echo "error|Username Used By Other User";
			} else {
				$status_username = "tidak ada";
				if ($password == '' || $password == NULL) {
					$newpass = $password_lama;
				}else {
					$newpass = md5($password);
				}

				$data = array(
					'username' => $username,
					'password' => $this->input->post("password"),
					'name' => $this->input->post("name"),
					'email' => $this->input->post("email"),
					'password' =>$newpass,
					'phone' => $this->input->post("phone"),
					'status' => "1"
				);

				$this->M_crud->update_data('admin', $data, $where);
				echo "success|Update Data Success";
			}
			
		}else {
			echo "error|Something Error";
		}
	}

	public function delete_data_user()
	{
		if ($this->input->post("id")) {
			$id = $this->Crypt->de($this->input->post("id"));
			$where = array(array('id', $id));
			$cek_data = $this->M_crud->cek_data('admin', $where);
			if ($cek_data) {
				$this->M_crud->delete_data('admin', $where);
				echo "success|Delete Data Success";
			}else {
				echo "error|Data Not Found";
			}

		} else {
			echo "error|Something Error";
		}
		
		
			
	}



}
