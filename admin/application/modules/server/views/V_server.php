<style>
    thead, th, tbody {text-align: center;}
</style>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Server List</h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Hostname</th>
                                        <th>Address</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $count = 1;
                                        $data_ip = '';
                                        
                                        foreach ($data_server as $data_server) {
                                    ?>
                                    <tr>
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $data_server->server_hostname ?></td>
                                        <td><?php echo $data_server->server_address  ?></td>
                                        <?php
                                            if ($count == 1) {
                                                $data_ip .= $data_server->server_address;
                                            } else {
                                                $data_ip .= '|'.$data_server->server_address;
                                            }
                                            
                                            $where_poweron = array(
                                                array("id", $data_server->id_method_poweron)
                                            );
                                            $where_poweroff = array(
                                                array("id", $data_server->id_method_poweroff)
                                            );
                                            $where_server_category = array(
                                                array("id", $data_server->id_server_category)
                                            );

                                            $method_poweron = $this->M_crud->get_row('method_poweron', 'metode', $where_poweron);
                                            $method_poweroff = $this->M_crud->get_row('method_poweron', 'metode', $where_poweroff);
                                            $server_category = $this->M_crud->get_row('server_category', 'category', $where_server_category);
                                            if ($data_server->server_status == '1') {
                                                $server_status = '<span class="label label-success">Online</span>';
                                            } else {
                                                $server_status = '<span class="label label-danger">Offline</span>';
                                            }
                                            
                                        ?>
                                        <td><?php echo $server_category  ?></td>
                                        <td id="<?php echo "status_".$count; ?>"><?php echo $server_status ?></td>
                                        <td>$320,800</td>
                                    </tr>
                                    <?php
                                        $count++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                                        
                            <input id="data_ip" type="hidden" value="<?php echo $data_ip ?>">
                            <!-- <div>
                                        <button onClick="get_data_ping()" class="btn btn-default" >Tes</button>
                            </div> -->
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>

<!-- <button type="button" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">MODAL - LARGE SIZE</button> -->
<!-- Modal Section  -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="largeModalLabel">Modal title</h4>
         </div>
         <div class="modal-body">
            tes ini modal
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button>
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal Section -->

<script type="text/javascript">

    function get_data_ping() {
        var data_ip = $("#data_ip").val();
        $.ajax({
            url : "http://10.0.12.244/monitoring/ping.php",
            type : "post",
            data : {
				data_ip : data_ip
			},
            success : function(response){
                var datanya= response;
                data_array = datanya.split("|");
                for (let index = 0; index < data_array.length; index++) {
                    var count = index + 1;
                    if (data_array[index] == '1') {
                        $("#status_"+count).html("<span class='label label-success'>Online</span>");
                    } else {
                        $("#status_"+count).html("<span class='label label-danger'>Offline</span>");
                    } 
                }
            }
        });
    }

    setInterval(function(){
        get_data_ping();
    }, 2200);


    setTimeout(function () { 
      location.reload();
    }, 70500);


    $(function () {
        $('.js-basic-example').DataTable({
            responsive : true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": true,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ "_all" ] }
            ],
            "iDisplayLength": 50
        });
    });
</script>


