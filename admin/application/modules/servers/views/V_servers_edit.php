
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Edit Server</h2>
                        
                    </div>
                    <div class="body">
                        <form id="myform" action="#" method="post">                                            
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Hostname</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="server_hostname" name="server_hostname" class="form-control" placeholder="Enter Server Hostname" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_address" type="text" name="server_address" class="form-control" placeholder="Enter Server Address -> ex : 10.0.0.1" pattern="^([0-9]{1,3}\.){3}[0-9]{1,3}$" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Mac Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="server_mac_address" name="server_mac_address" class="form-control" placeholder="Enter Server Mac Address" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Username</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="server_username" name="server_username" class="form-control" placeholder="Enter Server Username" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_password" type="password" name="server_password" class="form-control" placeholder="Enter Server Password" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server SSH Port</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="server_port" type="text" name="server_ssh_port" class="form-control" placeholder="Enter Server SSH Port" required>
                                        </div>
                                    </div>
                                </div>
                            </div>   

                            <div class="row">
                                <?php $method_power = $this->M_crud->get_data('method_poweron'); ?> 
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Method Power Off</label>
                                    <div class="form-group">
                                        <select disabled required class="form-control show-tick"  >
                                            <option value selected disabled >-- Please select Server Category Before --</option>
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Method Power On</label>
                                    <div class="form-group">
                                        <select disabled required class="form-control show-tick"  >
                                            <option value selected disabled >-- Please select Server Category Before --</option>
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                    <label >Server Role</label>
                                    <div class="form-group">
                                        <?php $data_server_role = $this->M_crud->get_data('server_category'); ?>
                                        <select id="select_server_role" class="form-control show-tick" name="server_role" required>
                                            <option value selected disabled >-- Please select server role --</option>
                                            <?php  foreach ($data_server_role as $key) { ?> 
                                            <option value="<?php echo $key->id ?>"><?php echo $key->category ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                         

                            <div class="form-group">
                                <div class="form">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-12 col xs 12">
                                            <input type="submit" class=" form-control btn btn-primary m-t-15 waves-effect" value="Save Server">
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-12 col xs 12">
                                            <input type="button" onClick="redirect_url('<?php echo base_url() ?>servers/')" class=" form-control btn btn-danger m-t-15 waves-effect" value="Cancel">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>
<div style="display:none" class="error">
    <span></span>
</div>

<script>

function redirect_url(url) {
    location.href = ""+url+"";
}


jQuery.validator.messages.required = "";
$("#myform").validate({
  invalidHandler: function(event, validator) {
    // 'this' refers to the form
    var errors = validator.numberOfInvalids();
    if (errors) {
    //   var message = errors == 1
    //     ? 'You missed 1 field. It has been highlighted'
    //     : 'You missed ' + errors + ' fields. They have been highlighted';
    //   $("div.error span").html(message);
    //   $("div.error").show();
    console.log("error");
    
    } else {
      $("div.error").hide();
    }
  }
});


function send_data() {
    
    // var isValid = $(server_password).parents('form').isValid();
    // if(!isValid) {
    //   e.preventDefault(); //prevent the default action
    // }
    // var pass = $('#server_password').val();
    // var pass_hash = window.btoa(pass);
    
    // $.ajax({
    //     type: "post",
    //     url: "<?php echo base_url() ?>servers/edit_server/",
    //     data: {
    //         pass_hash: pass_hash
    //     },
    //     success: function(response) {
    //         console.log(response);
    //     },
    //     error: function(response) {
    //         console.log(response);
    //     }
    // });
    
    // console.log(pass);
    // console.log(pass_hash);
}

$('#serveraddress').mask('099.099.099.099');
$("#server_port").numeric({
    allowSpace: false, // Allow the space character
    allowUpper: false  // Allow Upper Case characters
});


</script>


