<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<section class="content">
   <div class="container-fluid">
      <div class="row clearfix m-b-0 m-t-0">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card ">
               <div class="header">
                  <h2>Urutan Prosedur Power Off & Power On</h2>
               </div>
               <div class="body">
                    <div class="row">
                        <div class="col-md-8 col-ls-8 col-sm-12 col-xs-12">
                            <div class="dd nestable-with-handle">
                                <ol class="dd-list">
                                    <?php foreach ($data_urutan as $data_urutan) { 
                                        $where_category = array(
                                            array("id", $data_urutan->id_server_category)
                                        );
                                        $kategori = $this->M_crud->get_row('server_category', 'category', $where_category);
                                    ?>
                                    <li class="dd-item dd3-item" data-id="<?php echo $data_urutan->id ?>">
                                        <div class="dd-handle dd3-handle"></div>
                                        <div class="dd3-content"><?php echo $data_urutan->server_hostname ?> / <?php echo $data_urutan->server_address ?> / <?php echo $kategori ?></div>
                                    </li>
                                    <?php } ?>
                                </ol>
                            </div>
                        </div>
                        <div class="col-md-4 col-ls-4 col-sm-12 col-xs-12">
                            <h5>Hint : </h5>
                            <p>Prosedur Power Off dari Urutan atas ke bawah</p>
                            <p>Prosedur Power On dari Urutan bawah ke atas</p>
                        </div>
                    </div>
                    <!-- <b>Output JSON</b>
                    <textarea cols="30" rows="3" class="form-control no-resize" readonly></textarea> -->
                </div>
            </div>
         </div>
      </div>
      <!-- #END# Widgets -->
   </div>
</section>
<!-- <script src="<?php echo base_url() ?>../assets/backend/js/pages/ui/sortable-nestable.js"></script> -->
<script type="text/javascript">

   $(function () {
        $('.dd').nestable({
            maxDepth: 1,
            group: $(this).prop('id')
        });
        $('.dd').on('change', function () {
            var $this = $(this);
            var serializedData = window.JSON.stringify($($this).nestable('serialize'));

            // var serializedData = window.JSON.stringify($($this).nestable('serialize'));
            // $this.parents('div.body').find('textarea').val(serializedData);
            // alert(serializedData);
            // console.log(serializedData);
            $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>servers/ajax_urutan/",
                    data : {
                        data_server : serializedData
                    },
                    success: function(response) {
                        console.log(response);
                    },
                    error: function(response) {
                        console.log(response);
                    }
            });
        });
   });
</script>