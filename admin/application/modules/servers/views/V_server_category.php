
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Server List</h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover ">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Hostname</th>
                                        <th>Status</th>
                                        <th>Power Control</th>
                                        <th>Control Method</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $count = 1;
                                        for ($i=0; $i < 100; $i++) { 
                                    ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td>System Architect</td>
                                        <td>Edinburgh</td>
                                        <td>61</td>
                                        <td>2011/04/25</td>
                                        <td>$320,800</td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>


<!-- Modal Section  -->
<!-- Default Size -->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="defaultModalLabel">Parameter Poweroff Detail</h4>
         </div>
         <hr>
         <div class="modal-body">
            <form action="<?php echo base_url() ?>admin/powercontrol/edit_parameter" method="post">
                <label >Parameter</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_parameter" class="form-control" disabled readonly value="">
                        <input type="hidden" id="modal_id_parameter" name="id_parameter" class="form-control" value="">
                    </div>
                </div>
                <label >Deskripsi Parameter</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_desc" class="form-control" disabled readonly value="">
                    </div>
                </div>
                <label >Nilai Parameter  </label> <span id="modal_satuan" ></span>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_nilai_par" class="form-control" value="">
                    </div>
                </div>
                <label >Status Parameter </label> 
                <div class="form-group">
                    <select id="modal_status" name="status" class="form-control show-tick">
                        <option selected value="1">Enable</option>
                    </select>
                </div>
            </form>
         </div>
         <div class="modal-footer">
            <a id="modal_button_edit" onClick="send_data()"  type="button" class="btn btn-link waves-effect" href="#">UPDATE</a>
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal Section -->




