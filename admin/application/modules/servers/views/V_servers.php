<style>
    thead, th, tbody {text-align: center;}
</style>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2 >Server List</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown ">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                    <i class="material-icons">add</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo base_url() ?>manservers/addgeneral" class=" waves-effect waves-block">Add Server (General)</a></li>
                                    <li><a href="<?php echo base_url() ?>manservers/addilo" class=" waves-effect waves-block">Add Server (HP iLO)</a></li>
                                    <!-- <li><a href="<?php echo base_url() ?>manservers/addimm" class=" waves-effect waves-block">Add Server (IBM IMM)</a></li>
                                    <li><a href="<?php echo base_url() ?>manservers/adducs" class=" waves-effect waves-block">Add Server (UCS CIMC)</a></li> -->

                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Hostname</th>
                                        <th>Address</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $count = 1;
                                        $data_ip = '';
                                        $data_id = '';
                                        
                                        foreach ($data_server as $data_server) {
                                    ?>
                                    <tr>
                                        <input style="display:none" id="en_<?php echo $count ?>" value="<?php echo $this->Crypt->en($data_server->id) ?>" />
                                        <input style="display:none" id="method_<?php echo $count ?>" value="<?php echo $data_server->id_jenis_method ?>" />
                                        <td><?php echo $count ?></td>
                                        <td id="server_hostname_<?php echo $count ?>"><?php echo $data_server->server_hostname ?></td>
                                        <td id="server_address_<?php echo $count ?>"><?php echo $data_server->server_address  ?></td>
                                        <?php
                                            if ($count == 1) {
                                                $data_id .= $data_server->id;
                                                $data_ip .= $data_server->server_address;
                                            } else {
                                                $data_id .= '|'.$data_server->id;
                                                $data_ip .= '|'.$data_server->server_address;
                                            }
                                            
                                            $where_poweron = array(
                                                array("id", $data_server->id_method_poweron)
                                            );
                                            $where_poweroff = array(
                                                array("id", $data_server->id_method_poweroff)
                                            );
                                            $where_server_category = array(
                                                array("id", $data_server->id_server_category)
                                            );

                                            $method_poweron = $this->M_crud->get_row('method_poweron', 'kode', $where_poweron);
                                            $method_poweroff = $this->M_crud->get_row('method_poweron', 'kode', $where_poweroff);

                                            $nama_method_poweron = $this->M_crud->get_row('method_poweron', 'metode', $where_poweron);
                                            $nama_method_poweroff = $this->M_crud->get_row('method_poweron', 'metode', $where_poweroff);

                                            $server_category = $this->M_crud->get_row('server_category', 'category', $where_server_category);


                                            if ($data_server->server_status == '0') {
                                                $server_status = '<span class="label label-danger">Offline</span>';
                                                $server_action = '<button onClick="power('.$count.', 1)" class="btn btn-success btn-circle waves-effect waves-circle waves-float"><i class="material-icons" >power_settings_new</i></button>';
                                            }

                                            if ($data_server->server_status == '1') {
                                                $server_status = '<span class="label label-success">Online</span>';
                                                $server_action = '<button onClick="power('.$count.', 0)" class="btn btn-danger btn-circle waves-effect waves-circle waves-float"><i class="material-icons" >power_settings_new</i></button>';

                                            } 

                                            if ($data_server->server_status == '2') {
                                                $server_status = '<span class="label label-info">Booting</span>';
                                                $server_action = '<button disabled class="btn btn-default btn-circle waves-effect waves-circle waves-float"><i class="material-icons" >power_settings_new</i></button>';

                                            }

                                            if ($data_server->server_status == '3') {
                                                $server_status = '<span class="label label-warning">Powering Off</span>';
                                                $server_action = '<button disabled class="btn btn-default btn-circle waves-effect waves-circle waves-float"><i class="material-icons" >power_settings_new</i></button>';

                                            }
                                            
                                        ?>
                                        <td><?php echo $server_category  ?></td>
                                        <td id="<?php echo "status_".$count; ?>"><?php echo $server_status ?></td>
                                        <td id="<?php echo "action_".$count; ?>">
                                        <?php echo $server_action ?>
                                        <button id="button_modal" onClick="modal_detail(<?php echo $count ?>)" class="btn btn-default btn-circle waves-effect waves-circle waves-float"><i class="material-icons" >zoom_in</i></button>
                                        </td>
                                       
                                       <?php 
                                            $where_ilo = array(
                                                array("id_server", $data_server->id)
                                            );

                                            $where_wol = array(
                                                array("id_server", $data_server->id)
                                            );

                                            $get_data_ilo = $this->M_crud->get_row('ilo', 'id', $where_ilo);
                                            if ($get_data_ilo) {
                                                $id_ilo = $get_data_ilo;
                                            } else {
                                                $id_ilo = '0';
                                            }

                                            $get_data_wol = $this->M_crud->get_row('wol', 'id', $where_wol);
                                            if ($get_data_wol) {
                                                $id_wol = $get_data_wol;
                                            } else {
                                                $id_wol = '0';
                                            }
                                       ?>

                                    </tr>
                                        <p id="data_server_<?php echo $count ?>" style="display:none" ><?php echo  $data_server->id."|".$id_ilo."|".$id_wol."|".$server_category."|".$data_server->server_user."|".$data_server->server_status."|".$nama_method_poweron."|".$nama_method_poweroff."|".$data_server->id_method_poweroff."|".$data_server->id_method_poweron."|".$method_poweroff."|".$method_poweron ?></p>
            
                                    
                                        
                                    <?php 
                                        $count++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                                        
                            <input id="data_ip" type="hidden" value="<?php echo $data_ip ?>">
                            <input id="data_id" type="hidden" value="<?php echo $data_id ?>">
                            <!-- <div>
                                    <button onClick="showConfirmMessage()" class="btn btn-default" >Tes</button>
                            </div> -->

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>

<!-- <button type="button" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">MODAL - LARGE SIZE</button> -->
<!-- Modal Section  -->
<!-- Default Size -->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title" id="defaultModalLabel">Server Detail</h3>
         </div>
         
         <div class="modal-body">
            <form action="#" method="post">
                <label >Server Hostname</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_server_hostname" class="form-control" disabled readonly value="">
                        <input type="hidden" id="modal_server_address_en"  value="">
                        <input type="hidden" id="modal_server_method"  value="">
                    </div>
                </div>
                <!-- <label >Server Address</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_server_address" class="form-control" disabled readonly value="">
                    </div>
                </div> -->
                <label >Server Category</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_server_category" class="form-control" disabled readonly value="">
                    </div>
                </div>

                <label >Poweroff Method</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_poweroff_method" class="form-control" disabled readonly value="">
                    </div>
                </div>

                <label  >Poweron Method</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_poweron_method" class="form-control" disabled readonly value="">
                    </div>
                </div>

                
                <label >Server Status</label><div id="modal_server_status" class=""></div>
                
            </form>
         </div>
         <div class="modal-footer">
            <input type="hidden" id="param_add">
            <button type="button" id="modal_button_edit" onClick="modal_edit()" class="btn btn-link waves-effect" >UPDATE </button>
            <button type="button" id="modal_button_delete" onClick="delete_server()" class="btn btn-link waves-effect" >DELETE </button>
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
      </div>
   </div>
</div>




<!-- Modal Section -->

<script type="text/javascript">


    function get_data_ping() {
        var data_ip = $("#data_ip").val();
        var data_id = $("#data_id").val();
        $.ajax({
            url : "http://118.98.121.237:8989/monitoring/ping.php",
            type : "post",
            data : {
             data_id : data_id,
				data_ip : data_ip
			},
            success : function(response){
                // console.log("Request Success : "+response+"\n\n");
                var text;
                var datanya= response;
                // var datanya = "1|2|1|1|3";
                data_array = datanya.split("|");
                for (let index = 0; index < data_array.length; index++) {
                    var count = index + 1;
                    var button_detail = "<button id='button_modal' onClick='modal_detail("+count+")' class='btn btn-default btn-circle waves-effect waves-circle waves-float'><i class='material-icons' >zoom_in</i></button>";

                    if (data_array[index] == "0") {
                        $("#status_"+count).html("<span class='label label-danger'>Offline</span>");
                        $("#action_"+count).html("<button onClick='power("+count+", 1)' class='btn btn-success btn-circle waves-effect waves-circle waves-float'><i class='material-icons' >power_settings_new</i></button>"+button_detail);
                        
                    }

                    if (data_array[index] == "1") {
                        $("#status_"+count).html("<span class='label label-success'>Online</span>");
                        $("#action_"+count).html("<button onClick='power("+count+", 0)' class='btn btn-danger btn-circle waves-effect waves-circle waves-float'><i class='material-icons' >power_settings_new</i></button>  "+button_detail);
                    }

                    if (data_array[index] == "2") {
                        $("#status_"+count).html("<span class='label label-info'>Booting</span>");
                        $("#action_"+count).html("<button disabled class='btn btn-default btn-circle waves-effect waves-circle waves-float'><i class='material-icons' >power_settings_new</i></button>"+button_detail);

                    }

                    if (data_array[index] == "3") {
                        $("#status_"+count).html("<span class='label label-warning'>Powering Off</span>");
                        $("#action_"+count).html("<button disabled class='btn btn-default btn-circle waves-effect waves-circle waves-float'><i class='material-icons' >power_settings_new</i></button>"+button_detail);

                    }
                }
            }
        });
    }


    function modal_edit() {

        $('#modal_detail').modal('hide');
        var en_id = $('#modal_server_address_en').val();
        var id_jenis_method = $('#modal_server_method').val();
        if(id_jenis_method == 1){
            window.location = "<?php echo base_url() ?>manservers/edit_server_general/"+en_id;
        }

        if(id_jenis_method == 2){
            window.location = "<?php echo base_url() ?>manservers/edit_server_ilo/"+en_id;
        }

        if(id_jenis_method == 3){
            window.location = "<?php echo base_url() ?>manservers/edit_server_imm/"+en_id;
        }

        if(id_jenis_method == 4){
            window.location = "<?php echo base_url() ?>manservers/edit_server_ucs/"+en_id;
        }

       
    }


    function delete_server() {
        var id_server = $("#param_add").val();
        var server_hostname = $("#modal_server_hostname").val();
        // $('#modal_detail').modal('hide');
        swal({
            title: "Are you sure?",
            text: "Delete Server "+server_hostname,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            $.ajax({
                url : "<?php echo base_url() ?>servers/delete",
                type : "POST",
                data : {
                    id_server : id_server
                },
                beforeSend : function () {
                    $('#modal_detail').modal('hide');
                    swal({
                        title: "Harap Tunggu",
                        type: "info",
                        showCancelButton: false,
                        closeOnConfirm: false,
                    });
                },
                success : function(data){
                    data_array = data.split("|");
                    swal({
                            title: "delete server",
                            text: data_array[1],
                            type: ""+data_array[0]+""
                        },
                        function() {
                            location.reload();
                        }
                    );
                },
                error : function () {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        });
    }

    function modal_detail(count) {
        var data_server =  $("#data_server_"+count).text();
        server_array = data_server.split("|");

        var server_hostname = $("#server_hostname_"+count).html();
        var server_address = $("#server_address_"+count).html();
        var id_server = server_array[0];
        var id_ilo = server_array[1];
        var id_wol = server_array[2];
        var server_category = server_array[3];
        var server_user = server_array[4];
        var server_status = server_array[5];
        var method_poweron = server_array[6];
        var method_poweroff = server_array[7];
        var status_dengan_label = $("#status_"+count).html();
        var en_id = $("#en_"+count).val();
        var id_jenis_method = $("#method_"+count).val();
        

        $("#modal_button_edit").attr("href", "<?php echo base_url() ?>servers/"+id_server);
        $("#modal_server_hostname").val(server_hostname+" ("+server_address+") ");
        $("#modal_server_address").val(server_address);
        $("#modal_server_category").val(server_category);
        $("#modal_server_status").html(status_dengan_label);
        $("#param_add").val(id_server);
        $("#modal_poweroff_method").val(method_poweroff);
        $("#modal_poweron_method").val(method_poweron);
        $("#modal_server_address_en").val(en_id);
        $("#modal_server_method").val(id_jenis_method);
        // $server_category."|".$data_server->server_user."|".$data_server->server_status."|".$data_server->id_method_poweron."|".$data_server->id_method_poweroff
        
        // console.log(server_hostname+" _ "+server_address+" _ "+server_category);
        // console.log(server_array);
        // console.log(status_dengan_label);
        $('#modal_detail').modal('show');
    }

    setInterval(function(){
        get_data_ping();
    }, 1200);


    setTimeout(function () { 
      location.reload();
    }, 82500);



    function power(count, action) { // action 1 = power on || action 0 = power off
        var data_server =  $("#data_server_"+count).text();
        server_poweroff = data_server.split("|");
       
        var id_server = server_poweroff[0];
        var id_ilo = server_poweroff[1];
        var id_wol = server_poweroff[2];
        var server_category = server_poweroff[3];
        var server_user = server_poweroff[4];
        var server_status = server_poweroff[5];
        var method_poweroff = server_poweroff[7];
        var id_method_poweroff = server_poweroff[8];
        var id_method_poweron = server_poweroff[9];
        var kode_method_poweroff = server_poweroff[10];
        var kode_method_poweron = server_poweroff[11];

        if (action == '1') {
            var url_post = "<?php echo base_url() ?>servers/poweron";
            var aksi = "Power On";
            var method_power = id_method_poweron;
            var kode_method_power = kode_method_poweron;
            var titlenya = "Powering On";
            var textnya = "Powering On this Server!";
        } else {
            var url_post = "<?php echo base_url() ?>servers/poweroff";
            var aksi = "Power Off";
            var method_power = id_method_poweroff;
            var kode_method_power = kode_method_poweroff;
            var titlenya = "Powering Off";
            var textnya = "Powering Off this Server!";
        }    


        swal({
            title: "Are you sure?",
            text: textnya,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            $.ajax({
                url : url_post,
                type : "POST",
                data : {
                    action : aksi,
                    id_server : id_server,
                    id_ilo : id_ilo,
                    id_wol : id_wol,
                    method : method_power,
                    kode : kode_method_power 
                },
                beforeSend : function () {
                    console.log("Lagi loading");
                    swal({
                        title: "Harap Tunggu",
                        type: "info",
                        showCancelButton: false,
                        closeOnConfirm: false,
                    });
                },
                success : function(data){
                    data_array = data.split("|");
                    swal({
                            title: titlenya,
                            text: data_array[1],
                            type: ""+data_array[0]+""
                        },
                        function() {
                            location.reload();
                        }
                    );
                },
                error : function () {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });

           
            // $.post(url_post, {
            //     action : aksi,
            //     id_server : id_server,
            //     id_ilo : id_ilo,
            //     id_wol : id_wol,
            //     method : method_power,
            //     kode : kode_method_power 
            // },
            //     function(data, status) {
            //         data_array = data.split("|");
            //         swal({
            //                 title: titlenya,
            //                 text: data_array[1],
            //                 type: ""+data_array[0]+""
            //             },
            //             function() {
            //                 location.reload();
            //             }
            //         );
            //     }
            // );

        });
    }



</script>

                    <!-- <?php if ($this->session->flashdata('msg')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('msg'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?> -->


