<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('MENU_AKTIF', 'servers');

class Servers extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->require_login(TRUE);
	}

	public function index()
	{
		$data2['active_nav'] =  MENU_AKTIF;
		$order_by = array('id_urutan_sop', 'asc');
		$data['data_server'] = $this->M_crud->get_data('server', null, $order_by);	
		exec("sudo php /var/www/monitoring/ping_crontab_v2.php");
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_servers', $data);
		$this->load->view('footer');
	}

	public function urutan()
	{
		$data2['active_nav'] =  "urutan";
		$where_server = array(
			array("id_urutan_sop != ", "0")
		);
		$order_by = array('id_urutan_sop', 'asc');
		$data['data_urutan'] = $this->M_crud->get_data('server', $where_server, $order_by);
		// echo "<pre>";print_r($data['data_urutan'] );echo "</pre>";
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_urutan', $data);
		$this->load->view('footer');
	}

	public function ajax_urutan()
	{

		if ($this->input->post('data_server')) {
			$data_server = json_decode($this->input->post('data_server'), true);
			// echo count($data_server);
			for ($i=0; $i < count($data_server); $i++) { 
				$count = $i + 1;
				$where_server = array( array("id", $data_server[$i]['id']) );
				$data = array( 'id_urutan_sop' => $count );
				$this->M_crud->update_data('server', $data, $where_server);
				echo "Update id_data_server = ".$data_server[$i]['id']." di urutan -> ".$count." Success \n";
			}
			// echo $data_server[0]['id'];
			// print_r($data_server);
		} else {
			echo "noo data";
		}
		
		
	}

	public function category()
	{
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_server_category');
		$this->load->view('footer');
	}

	public function delete()
	{
		if($this->input->post('id_server')){
			$id_server = $this->input->post('id_server');
			$where_id =  array( array("id", $id_server));
			$where_id_server =  array( array("id_server", $id_server));

			
			
			///////// cek id server di ilo /////////////////
			$cek_data_ilo = $this->M_crud->cek_data('ilo', $where_id_server);
			if ($cek_data_ilo) {
				$this->M_crud->delete_data('ilo', $where_id_server);
			}
			////////////////////////////////////////////////

			///////// cek id server di imm /////////////////
			$cek_data_imm = $this->M_crud->cek_data('imm', $where_id_server);
			if ($cek_data_imm) {
				$this->M_crud->delete_data('imm', $where_id_server);
			}
			////////////////////////////////////////////////

			///////// cek id server di ucs /////////////////
			$cek_data_ucs = $this->M_crud->cek_data('ucs', $where_id_server);
			if ($cek_data_ucs) {
				$this->M_crud->delete_data('ucs', $where_id_server);
			}
			////////////////////////////////////////////////

			///////// cek id server di wol /////////////////
			$cek_data_wol = $this->M_crud->cek_data('wol', $where_id_server);
			if ($cek_data_wol) {
				$this->M_crud->delete_data('wol', $where_id_server);
			}
			////////////////////////////////////////////////

			///////// cek id server di proxmox api /////////////////
			$where_id_server_parent =  array( array("id_server_parent", $id_server));
			$cek_data_proxmox = $this->M_crud->cek_data('proxmox', $where_id_server_parent);
			if ($cek_data_proxmox) {
				// $this->M_crud->delete_data('proxmox', $where_id_server_parent);
				$data_proxmox_vm = $this->M_crud->get_data('proxmox', $where_id_server_parent);
				foreach ($data_proxmox_vm as $data_proxmox_vm) {
					$id_server_vm = $data_proxmox_vm->id_server;
					$where_id_server_vm = array( array("id", $id_server_vm));
					$where_id_server_vm_id_table_proxmox  = array( array("id_server", $id_server_vm));
					$this->M_crud->delete_data('server', $where_id_server_vm);
					$this->M_crud->delete_data('proxmox', $where_id_server_vm_id_table_proxmox);
				}
				// $this->M_crud->delete_data('server', $where_id_server_vm);
			}
			////////////////////////////////////////////////////////


			///////// cek id server di table server /////////
			$cek_data_server = $this->M_crud->cek_data('server', $where_id);
			if ($cek_data_server) {
				$this->M_crud->delete_data('server', $where_id);
			}
			////////////////////////////////////////////////

			echo "success|Success Deleted Server";
		}else {
			echo "error|posterror";
		}
	}

	public function add()
	{
		$data2['active_nav'] =  MENU_AKTIF;
		if($this->input->post('submit')){
			$server_hostname = $this->input->post('server_hostname');
			$server_address = $this->input->post('server_address');
			$server_username = $this->input->post('server_username');
			$server_password =  $this->Crypt->en($this->input->post('server_password'));
			$server_ssh_port = $this->input->post('server_ssh_port');
			$server_mac_address = $this->input->post('server_mac_address');
			$method_poweroff = $this->input->post('method_poweroff');
			$method_poweron = $this->input->post('method_poweron');
			$server_role = $this->input->post('server_role');
			$server_category = $this->input->post('server_category');
			
			if ($this->input->post('urutan') == '0') {
				$urutan = '0';
			} else {
				$urutan_tertinggi = $this->M_crud->get_row_order('server','id_urutan_sop','desc','1');
				if ($urutan_tertinggi) { $urutan = $urutan_tertinggi[0]->id_urutan_sop + 1; } else { $urutan = '0';}
				// print_r($urutan[0]->id_urutan_sop);
				// echo $urutan;
			}
			
			
			$data = array(
				'server_hostname' => $server_hostname,
				'server_address' => $server_address,
				'server_user' => $server_username,
				'server_password' => $server_password,
				'server_ssh_port' => $server_ssh_port,
				'server_mac_address' => $server_mac_address,
				'id_method_poweroff' => $method_poweroff,
				'id_method_poweron' => $method_poweron,
				'id_server_category' => $server_role,
				'id_jenis_server' => $server_category,
				'id_urutan_sop' => $urutan,
				'status' => '0', // nda jelaspi ini
				'server_status' => '0'
			);

			$insert_id_server = $this->M_crud->insert_data('server', $data, $req = TRUE);
			// echo "<pre>";print_r($data);echo "</pre>";

			if ($method_poweroff == '2' || $method_poweron == '2') {
				$ilo_address = $this->input->post('ilo_address');
				$ilo_username = $this->input->post('ilo_username');
				$ilo_password = $this->Crypt->en($this->input->post('ilo_password'));
				$ilo_version = $this->input->post('ilo_version');
				$insert = $this->insert_ilo($insert_id_server, $ilo_address, $ilo_username, $ilo_password, $ilo_version );
				// echo "<pre>";print_r($insert);echo "</pre>";
			}

			if ($method_poweroff == '3' || $method_poweron == '3') {
				$this->insert_wol($insert_id_server, $server_mac_address);
			}

			if ($method_poweroff == '4' || $method_poweron == '4') {
				$imm_address = $this->input->post('imm_address');
				$imm_username = $this->input->post('imm_username');
				$imm_password = $this->Crypt->en($this->input->post('imm_password'));
				$this->insert_ibm($insert_id_server, $imm_address, $imm_username, $imm_password);
			}

			if ($method_poweroff == '5' || $method_poweron == '5') {
				$ucs_address = $this->input->post('ucs_mgmt_address');
				$ucs_username = $this->input->post('ucs_mgmt_username');
				$ucs_password = $this->Crypt->en($this->input->post('ucs_mgmt_password'));
				$this->insert_ucs($insert_id_server, $ucs_address, $ucs_username, $ucs_password);
			}

			if ($method_poweroff == '6' || $method_poweron == '6') {
				$id_server_parent = $this->input->post('vm_id_server_parent');
				$vmid = $this->input->post('vm_id');
				$jenis = $this->input->post('vm_id_category');
				$this->insert_proxmox($insert_id_server, $id_server_parent, $vmid, $jenis);
			}

			redirect(base_url()."servers", "refresh");

		}else {
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_add');
			$this->load->view('footer');
		}
	}

	public function edit_server($id = null)
	{
		$data2['active_nav'] =  MENU_AKTIF;
		$id = $this->Crypt->de($id);
		if ($this->input->post('pass_hash')) {
			// $server_password =  $this->input->post('server_password');
			$pass_hass = $this->input->post('pass_hash');
			// echo $pass_hass;
			$input = base64_decode($pass_hass);//$BB6B>8l8@B,;n(B - testing
			print_r($input);
			// $input_encoding = 'iso-2022-jp';
			// echo iconv($input_encoding, 'UTF-8', $input);
			// echo "success|Success Update Data Server";
		}else {
			$this->load->view('header');
			$this->load->view('sidebar', $data2);
			$this->load->view('V_servers_edit');
			$this->load->view('footer');
		}
	}

	public function insert_ilo($id_server, $ilo_address, $ilo_username, $ilo_password, $ilo_version){
		$data = array(
			'ilo_address' => $ilo_address,
			'ilo_username' => $ilo_username,
			'ilo_password' => $ilo_password,
			'ilo_version' => $ilo_version,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('ilo', $data);
	}

	public function insert_ibm($id_server, $imm_address, $imm_username, $imm_password)
	{
		$data = array(
			'imm_address' => $imm_address,
			'imm_username' => $imm_username,
			'imm_password' => $imm_password,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('imm', $data);
	}

	public function insert_ucs($id_server, $ucs_address, $ucs_username, $ucs_password)
	{
		$data = array(
			'ucs_address' => $ucs_address,
			'ucs_username' => $ucs_username,
			'ucs_password' => $ucs_password,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('ucs', $data);
	}

	public function insert_wol($id_server, $mac_address)
	{
		
		$data = array(
			'mac_address' => $mac_address,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('wol', $data);
	}

	public function insert_proxmox($id_server, $id_server_parent, $vmid, $jenis)
	{
		$data = array(
			'jenis' => $jenis,
			'id_vm' => $vmid,
			'id_server_parent' => $id_server_parent,
			'id_server' => $id_server
		);
		$this->M_crud->insert_data('proxmox', $data);
	}




	public function poweroff()
	{	
		if ($this->input->post("action")) {
			$id_server = $this->input->post("id_server");
			$method = $this->input->post("method");
			$id_ilo = $this->input->post("id_ilo");
			$id_wol = $this->input->post("id_wol");
			$kode = $this->input->post("kode");
			$status = '3';
			$where_server = array(array("id", $id_server));
			$data_server = $this->M_crud->get_data('server', $where_server);
			$server_username = $data_server[0]->server_user;
			$server_password = $this->Crypt->de($data_server[0]->server_password);
			$server_ssh_port = $data_server[0]->server_ssh_port;
			$server_address = $data_server[0]->server_address;
			// /////////////////////////// iLO Section //////////////////////////////////////////////////////////
			if ($kode == 'ilo') {
				$where_data_ilo = array(
					array("id_server", $id_server)
				);
				$data_ilo = $this->M_crud->get_data('ilo', $where_data_ilo);
				$ilo_address = $data_ilo[0]->ilo_address;
				$ilo_version = $data_ilo[0]->ilo_version;
				$ilo_status = $data_ilo[0]->ilo_status;
				$ilo_username = $data_ilo[0]->ilo_username;
				$ilo_password = $this->Crypt->de($data_ilo[0]->ilo_password);
				$action = "power off";
				exec("sudo fping -t 10 $ilo_address 2>&1", $output_poweroff_ilo, $return_poweroff_ilo);
				if ($return_poweroff_ilo == 0) {
					$this->method_ssh_ilo($ilo_address, $ilo_username, $ilo_password,  $ilo_version, $action);
					echo "success|Powering Off Server With iLO Connection";
				} else {
					echo "error|iLO Address Unreachable";
				}
			}
			// /////////////////////////// iLO Section //////////////////////////////////////////////////////////

			// /////////////////////////// ssh Coon Section //////////////////////////////////////////////////////////
			if ($kode == "ssh") {
				$action = "poweroff";
				exec("sudo fping -t 10 $server_address 2>&1", $cek_status_server_ssh, $return_status_server_ssh);
				if ($return_status_server_ssh == 0) {
					$this->method_ssh_conn($server_address, $server_username, $server_password, $server_ssh_port, $action);
					echo "success|Powering Off Server With SSH Connection";
				} else {
					$status = '0';
					echo "error|Server Unreachable or Already Off";
				}
			}
			// /////////////////////////// ssh Section //////////////////////////////////////////////////////////

			///////////////////////////// IBM IMM Section //////////////////////////////////////////////////////////
			if ($kode == 'imm') {
				$action_imm = "off";
				$where_data_imm = array(
					array("id_server", $id_server)
				);
				$data_imm = $this->M_crud->get_data('imm', $where_data_imm);
				$imm_address = $data_imm[0]->imm_address;
				$imm_username = $data_imm[0]->imm_username;
				$imm_password = $this->Crypt->de($data_imm[0]->imm_password);
				exec("sudo fping -t 10 $server_address 2>&1", $cek_status_server_imm, $return_status_server_imm);
				if ($return_status_server_imm == 0) {
					exec("sudo fping -t 10 $imm_address 2>&1", $output_imm, $return_var_imm);
					if ($return_var_imm == 0) {
						$this->method_ssh_imm($imm_address, $imm_username, $imm_password,  $action_imm);
						echo "success|Powering Off Server With IBM IMM";
					} else {
						echo "error|IBM IMM Address Unreachble";
					}
				}else {
					echo "error|Server Unreachable or Already Off";
				}
			}
			// /////////////////////////// IBM IMM Section //////////////////////////////////////////////////////////

			///////////////////////////// UCS XML APISection //////////////////////////////////////////////////////////
			if ($kode == 'ucs') {
				$action_ucs = "down";
				$where_data_ucs = array(
					array("id_server", $id_server)
				);
				$data_ucs = $this->M_crud->get_data('imm', $where_data_ucs);
				$ucs_address = $data_ucs[0]->ucs_address;
				$ucs_username = $data_ucs[0]->ucs_username;
				$ucs_password = $this->Crypt->de($data_ucs[0]->ucs_password);
				exec("sudo fping -t 10 $server_address 2>&1", $output_cek, $return_cek);
				if ($return_cek == 0) {
					exec("sudo fping -t 10 $ucs_address 2>&1", $output_cek_ucs, $return_cek_ucs);
					if ($return_cek_ucs == 0) {
						$this->method_api_ucs($ucs_address, $action_ucs);
						echo "success|Powering Off Server With UCS XML API";
					} else {
						
						echo "error|UCS Address Unreachable";
					}
				}else {
					echo "success|Server is already down";
				}
			}
			///////////////////////////// UCS XML APISection //////////////////////////////////////////////////////////
			if ($kode == 'proxmoxapi') {
				require(getcwd()."/pve2_api.class.php"); 
				$where_data_server = array(
					array("id_server", $id_server)
				);
				error_reporting(0); 
				$data_vm = $this->M_crud->get_data('proxmox', $where_data_server);
				$vmid = $data_vm[0]->id_vm;
				$jenis_vm = $data_vm[0]->jenis;
				$id_server_parent = $data_vm[0]->id_server_parent;
				$where_data_server_parent = array(
					array("id", $id_server_parent)
				);
				$data_server_parent = $this->M_crud->get_data('server', $where_data_server_parent);
				$address = $data_server_parent[0]->server_address;
				$username = $data_server_parent[0]->server_user;
				$password = $this->Crypt->de($data_server_parent[0]->server_password);
				exec("sudo fping -t 10 $address 2>&1", $output1, $return_var1);
				if ($return_var11 == 0) {
					$pve2 = new PVE2_API($address, $username, "pam", $password);
					if ($pve2->login()) {
						$nodes = $pve2->get_node_list();
						$first_node = $nodes[0];
						$status_vm = 'stop';
						error_reporting(0); 
						$pve2->post("/nodes/".$first_node."/".$jenis_vm."/".$vmid."/status/".$status_vm);
						echo "success|Send API Power Off VM Proxmox";
					} else {
						$status = '1';
						echo "error|Error Login API Proxmox";
					}
				} else {
					$status = '1';
					echo "error|Server  Parent Unreachble";
				}
			}

			$where_server = array(
				array('id',$id_server)
			);
			
			$data = array(
				'server_status' => $status
			);
			$this->M_crud->update_data('server', $data, $where_server);
		} else {
			echo "error|error";
		}
		
	}

	public function poweron(){	
		if ($this->input->post("action")) {
			$id_server = $this->input->post("id_server");
			$method = $this->input->post("method");
			$id_ilo = $this->input->post("id_ilo");
			$id_wol = $this->input->post("id_wol");
			$kode = $this->input->post("kode");
			$status = '2';

			$where_server = array(
				array("id", $id_server)
			);
			$data_server = $this->M_crud->get_data('server', $where_server);
			$server_username = $data_server[0]->server_user;
			$server_password = $this->Crypt->de($data_server[0]->server_password);
			$server_ssh_port = $data_server[0]->server_ssh_port;
			$server_address = $data_server[0]->server_address;
			$server_mac_address = $data_server[0]->server_mac_address;
			
			if($kode == 'ilo'){
				$where_data_ilo = array(
					array("id_server", $id_server)
				);
				$data_ilo = $this->M_crud->get_data('ilo', $where_data_ilo);
				$ilo_address = $data_ilo[0]->ilo_address;
				$ilo_version = $data_ilo[0]->ilo_version;
				$ilo_status = $data_ilo[0]->ilo_status;
				$ilo_username = $data_ilo[0]->ilo_username;
				$ilo_password = $this->Crypt->de($data_ilo[0]->ilo_password);
				$action = "power on";
				exec("sudo fping -t 10 $server_address 2>&1", $output_cek, $return_cek);
				if ($return_cek != 0) {
					exec("sudo fping -t 10 $ilo_address 2>&1", $output_poweroff_ilo, $return_poweroff_ilo);
					if ($return_poweroff_ilo == 0) {
						$this->method_ssh_ilo($ilo_address, $ilo_username, $ilo_password,  $ilo_version, $action);
						echo "success|Powering On Server With iLO";
					} else {
						echo "error|iLO Address Unreachable";
					}
				} else {
					echo "success|Server is UP";
				}
			}

			if ($kode == 'ssh') {
				$status = '0';
				echo "error|no ssh for power on server";
			}

			if ($kode == 'wol') {
				exec("sudo fping -t 10 $server_address 2>&1", $output_cek_wol, $return_cek_wol);
				if ($return_cek_wol != 0) {
					$this->method_wol($server_mac_address);
					echo "success|Sending WOL Magic Packet";
				}else {
					echo "error|Server Already Up";
				}
			}

			if ($kode == 'imm') {
				$action_imm = "on";
				$where_data_imm = array(
					array("id_server", $id_server)
				);
				$data_imm = $this->M_crud->get_data('imm', $where_data_imm);
				$imm_address = $data_imm[0]->imm_address;
				$imm_username = $data_imm[0]->imm_username;
				$imm_password = $this->Crypt->de($data_imm[0]->imm_password);
				// echo "success|$imm_address";
				exec("sudo fping -t 10 $server_address 2>&1", $output_cek_server_imm, $return_cek_server_imm);
				if ($return_cek_server_imm != 0) {
					exec("sudo fping -t 10 $imm_address 2>&1", $output_imm, $return_var_imm);
					if ($return_var_imm == 0) {
						$this->method_ssh_imm($imm_address, $imm_username, $imm_password,  $action_imm);
						echo "success|Powering On Server With IBM IMM";
					} else {
						echo "error|IBM IMM Address Unreachble";
					}
				} else {
					echo "error|Server Already Up";
				}
			}

			if ($kode == 'ucs') {
				$action_ucs = "up";
				$where_data_ucs = array(
					array("id_server", $id_server)
				);
				$data_ucs = $this->M_crud->get_data('imm', $where_data_ucs);
				$ucs_address = $data_ucs[0]->ucs_address;
				$ucs_username = $data_ucs[0]->ucs_username;
				$ucs_password = $this->Crypt->de($data_ucs[0]->ucs_password);
				exec("sudo fping -t 10 $server_address 2>&1", $output_cek, $return_cek);
				if ($return_cek != 0) {
					exec("sudo fping -t 10 $ucs_address 2>&1", $output_cek_ucs, $return_cek_ucs);
					if ($return_cek_ucs == 0) {
						$this->method_api_ucs($ucs_address, $action_ucs);
						echo "success|Powering On Server With UCS XML API";
					} else {
						echo "error|UCS Address Unreachable";
					}
				}else {
					echo "success|Server is UP";
				}
				
			}

			if ($kode == 'proxmoxapi') {
				require(getcwd()."/pve2_api.class.php"); //include API Proxmox
				$where_data_server = array(
					array("id_server", $id_server)
				);
				error_reporting(0);
				$data_vm = $this->M_crud->get_data('proxmox', $where_data_server);
				$vmid = $data_vm[0]->id_vm;
				$jenis_vm = $data_vm[0]->jenis;
				$id_server_parent = $data_vm[0]->id_server_parent;
				$where_data_server_parent = array(
					array("id", $id_server_parent)
				);
				$data_server_parent = $this->M_crud->get_data('server', $where_data_server_parent);
				$address = $data_server_parent[0]->server_address;
				$username = $data_server_parent[0]->server_user;
				$password = $this->Crypt->de($data_server_parent[0]->server_password);

				exec("sudo fping -t 10 $address 2>&1", $output1, $return_var1);

				if ($return_var1 == 0) {
					$pve2 = new PVE2_API($address, $username, "pam", $password);
					if ($pve2->login()) {
						//Get first node name.
						$nodes = $pve2->get_node_list();
						$first_node = $nodes[0];
						$status_vm = 'start';
						error_reporting(0); 
						$pve2->post("/nodes/".$first_node."/".$jenis_vm."/".$vmid."/status/".$status_vm);
						echo "success|Send API Power On VM Proxmox";
						// die(json_encode(array('success' => 1)));	
					} else {
						$status = '0';
						echo "error|Error Login API Proxmox";
						// die(json_encode(array('success' => 0, 'message' => 'Login Proxmox Gagal')));
					}
				} else {
					$status = '0';
					echo "error|Servers Parent Unreachble";
				}
			}

			$where_server = array(
				array('id',$id_server)
			);
			$data = array(
				'server_status' => $status
			);
			$this->M_crud->update_data('server', $data, $where_server);
		} else {
			echo "error|error data";
		}
	}


	public function method_api_ucs($server_address, $action)
	{
		if ($action) {
			// action -> up || down || cycling //
			$ip_address = $server_address; // server address cimc 
			$get_cookie = $this->get_cookie($ip_address, "admin", "password");
			$url = "https://".$ip_address."/nuova";
			$xml = '
				<configConfMo cookie="'.$get_cookie.'" inHierarchical="false" dn="sys/rack-unit-1" > 
					<inConfig>
						<computeRackUnit adminPower="'.$action.'" dn="sys/rack-unit-1">
						</computeRackUnit>
					</inConfig>
				</configConfMo>
			';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_URL, $url );
			curl_setopt($ch, CURLOPT_POST, TRUE );
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE );
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml );
			curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE );
			// curl_setopt($ch, CURLOPT_VERBOSE, TRUE); // DEBUG
			$output = curl_exec($ch);
			curl_close($ch);
			$xml_parse = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA); 
			$array = json_decode(json_encode((array)$xml_parse), TRUE);
			// echo '<pre>';print_r($array);echo '</pre>';
			echo "success";
		} else {
			echo "error";
		}
		
		
	}

	public function get_cookie($ip_address, $username, $password)
	{
		$url = "https://".$ip_address."/nuova";
		$xml = "<aaaLogin inName='".$username."' inPassword='".$password."' />";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_POST, TRUE );
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml );
		curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE );
	  	$output = curl_exec($ch);
		curl_close($ch);
		$xml_parse = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA); 
		$array = json_decode(json_encode((array)$xml_parse), TRUE);
		return $array['@attributes']['outCookie'];
	}


	public function method_ssh_conn($server_address, $server_username, $server_password, $server_ssh_port, $action)
	{
		$script_ssh = 'sudo sshpass -p "'.$server_password.'" ssh '.$server_username.'@'.$server_address.' -p    '.$server_ssh_port.'  "'.$action.'" ' ;
		exec($script_ssh, $output,  $return_var);
	}

	public function method_ssh_ilo($ilo_address, $ilo_username, $ilo_password,$ilo_version, $action)
	{
		if ($ilo_version == '2') { /// ini script untuk ilo 2
			$script = 'sudo sshpass -p "'.$ilo_password.'" ssh -o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha1 '.$ilo_username.'@'.$ilo_address.' '.$action.' ';
			exec($script, $output,  $return_var);
		}else{ // ini script untuk ilo 3 dan 4
			$script = 'sudo sshpass -p "'.$ilo_password.'" ssh '.$ilo_username.'@'.$ilo_address.' '.$action.'';
			exec($script, $output,  $return_var);
		}
	}

	public function method_wol($mac_address_server)
	{
		$script = 'sudo wakeonlan '.$mac_address_server.'';
		exec($script, $output,  $return_var);
	}

	public function method_ssh_imm($server_address, $server_username, $server_password,  $action)
	{
		if ($action == 'off') {
			$script = "sudo sshpass -p '".$server_password."' ssh -tt ".$server_username."@".$server_address." < /var/www/monitoring/command_off";
		} else {
			$script = 'sudo sshpass -p "'.$server_password.'" ssh -tt '.$server_username.'@'.$server_address.' < /var/www/monitoring/command_on';
		}
		// echo $script;
		exec($script, $output,  $return_var);
	}
}
