<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('MENU_AKTIF', 'home');

class Servers extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->require_login(TRUE);
	}

	public function index()
	{
		$data['data_server'] = $this->M_crud->get_data('server');
		
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_servers', $data);
		$this->load->view('footer');
		
	}

	public function method()
	{
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_method');
		$this->load->view('footer');
	}

	public function category()
	{
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_server_category');
		$this->load->view('footer');
	}



	public function poweroff()
	{	

		// $method = $this->input->post("method");
		// $id_ilo = $this->input->post("id_ilo");
		// $id_wol = $this->input->post("id_wol");
		// $jenis_ilo = $this->input->post("jenis_ilo");
		// $id_server = $this->input->post("id_server");
		

		$method = "ilo";
		$jenis_ilo = "2";
		$status = '3';
		$id = "1";
		// $method = $this->input->post("method");
		if ($method == "ilo") {
			
			$ip = "10.0.0.10";
			if ($jenis_ilo == "1") {
				exec("sudo sshpass -p 'admin_150' ssh -o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha1 admin_150@10.0.3.150 power off", $output,  $return_var);
			}else{
				exec("sudo sshpass -p 'admin_39' ssh admin_39@10.0.3.39 power off", $output,  $return_var);
			}
			// exec("sudo sshpass -p 'admin_39' ssh admin_39@10.0.3.39 power on", $output, $return_var);
			// exec("sudo who", $output, $return_var);
			echo "<pre>";print_r($output);echo "</pre>";
			$where = array(
				array('id',$id)
			);
			$data = array(
				'server_status' => $status
			);

			$this->M_crud->update_data('server', $data, $where);
			// echo $status;
			$this->session->set_flashdata('msg', 'Shutdown On Process');
			redirect(base_url()."servers/", 'refresh');

		}

		if ($method == "wol") {
			echo "exec using wol";
			$this->session->set_flashdata('msg', 'Shutdown On Process');
		}

		if ($method == "ssh") {
			echo "exec using SSH Connection";
			$this->session->set_flashdata('msg', 'Shutdown On Process');
		}
	}

	public function poweron()
	{	$method = "ilo";
		$jenis_ilo = "2";
		$status = '2';
		$id = "1";
		if ($method == "ilo") {
			if ($jenis_ilo == "1") {
				exec("sudo sshpass -p 'admin_150' ssh -o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha1 admin_150@10.0.3.150 power on", $output,  $return_var);
			}else{
				exec("sudo sshpass -p 'admin_39' ssh admin_39@10.0.3.39 power on", $output,  $return_var);
			}

			
			$where = array(
				array('id',$id)
			);
			$data = array(
				'server_status' => $status
			);

			$this->M_crud->update_data('server', $data, $where);
			echo $status;
		}

		if ($method == "wol") {
			echo "exec using wol";
		}

		if ($method == "ssh") {
			echo "exec using SSH Connection";
		}

		echo "<pre>";print_r($output);echo "</pre>";
		
	}

	public function testpost()
	{
		if ($this->input->post("action")) {
			// redirect(base_url()."home");
			$id_server = $this->input->post("id_server");
			$method = $this->input->post("method");
			$id_ilo = $this->input->post("id_ilo");
			$id_wol = $this->input->post("id_wol");

			$where_server = array(
				array("id", $id_server)
			);
			$data_server = $this->M_crud->get_data('server', $where_server);
			$server_username = $data_server[0]->server_user;
			$server_password = $data_server[0]->server_password;
			$server_ssh_port = $data_server[0]->server_ssh_port;
			$server_address = $data_server[0]->server_address;

			if ($id_ilo != '0') {
				//// get data ilo server //////////
				$where_data_ilo = array(
					array("id", $id_ilo)
				);

				$data_ilo = $this->M_crud->get_data('ilo', $where_data_ilo);
				$ilo_address = $data_ilo[0]->ilo_address;
				$ilo_version = $data_ilo[0]->ilo_version;
				$ilo_status = $data_ilo[0]->ilo_status;
				$ilo_username = $data_ilo[0]->ilo_username;
				$ilo_password = $data_ilo[0]->ilo_password;

				if ($ilo_version != '1') { /// ini script untuk ilo 2
					$script = 'sudo sshpass -p "'.$ilo_username.'" ssh '.$ilo_username.'@'.$ilo_address.'  power off';
					echo $script."\n";
					// exec($script, $output,  $return_var);
				}else{ // ini script untuk ilo 3 dan 4
					$script = 'sudo sshpass -p "'.$ilo_username.'" ssh -o HostKeyAlgorithms=ssh-rsa,ssh-dss -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes128-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha1 '.$ilo_username.'@'.$ilo_address.' power off';
					echo $script."\n";
				}
			}

			if ($id_wol != '0') { //power off using ssh connection
				//// get data wol server //////////
				$script_ssh = 'sudo sshpass -p "'.$server_password.'" ssh '.$server_username.'@'.$server_address.' -p    '.$server_ssh_port.'  "poweroff" ' ;
				exec($script_ssh, $output,  $return_var);
				// echo $script_ssh;
			}


			echo "\nmethod ->".$this->input->post("method");
			echo "\nid_ilo ->".$this->input->post("id_ilo");
			echo "\nid_wol ->".$this->input->post("id_wol");
			echo "\nid_server ->".$this->input->post("id_server");

		} else {
			echo "error";
		}
		
	}


	

}
