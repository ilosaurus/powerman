<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kontak extends MY_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('V_kontak');
		$this->load->view('footer');
	}

}
