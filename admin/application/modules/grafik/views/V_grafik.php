<section class="content">
    <div class="container-fluid">

        <div class="row clearfix m-b-0 m-t-0 m-r-0 m-l-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="bg-red header">
                        <h2>UPS Grafik</h2>
                    </div>
                    <div class="body">
                        <div id="div_chart" style="position: relative;">
                            <center><canvas id="mycanvas"></canvas></center>
                        </div>
                        <hr>
                        <div >
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                <select onchange="updatechart()" id="selectinterval" class="form-control pull-right">
                                    <option value="30">Last 30 Minutes</option>
                                    <option selected value="60">Last 60 Minutes</option>
                                    <option value="120">Last 120 Minutes</option>
                                    <option value="180">Last 180 Minutes</option>
                                </select>
                            </div>
                            
                            <div  class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                <h5 id="input_avg"></h5>
                            </div>
                            <div  class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                                <h5 id="output_avg"></h5>
                            </div>
                            <div  class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                                <h5 id="label_input_output" >Avg/Max/Min</h5>
                            </div>
                        </div>

                        <hr/>
                        <hr>
                        <br>

                        <div id="div_chart_load" style="position: relative;">
                            <center><canvas id="canvasload"></canvas></center>
                        </div>
                        <hr>
                        <div >
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                <select onchange="updatechartload()" id="selectintervalload" class="form-control pull-right">
                                    <option value="30">Last 30 Minutes</option>
                                    <option selected value="60">Last 60 Minutes</option>
                                    <option value="120">Last 120 Minutes</option>
                                    <option value="180">Last 180 Minutes</option>
                                </select>
                            </div>
                            
                            <div  class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                <h5 id="load_avg"></h5>
                            </div>
                            <div  class="col-md-3 col-lg-3 col-sm-3 col-xs-3"></div>
                        </div>
                        
                        <hr>
                        <hr>
                        <br>

                        <div id="div_chart_runtime" style="position: relative;">
                            <center><canvas id="canvasruntime"></canvas></center>
                        </div>
                        <hr>
                        <div >
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                <select onchange="updatechartruntime()" id="selectintervalruntime" class="form-control pull-right">
                                    <option value="30">Last 30 Minutes</option>
                                    <option selected value="60">Last 60 Minutes</option>
                                    <option value="120">Last 120 Minutes</option>
                                    <option value="180">Last 180 Minutes</option>
                                </select>
                            </div>
                            
                            <div  class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                <h5 id="runtime_avg"></h5>
                            </div>
                            <div  class="col-md-3 col-lg-3 col-sm-3 col-xs-3"></div>
                        </div>

                        <hr>
                        <hr>
                        <br>

                        <div id="div_chart_batterycapacity" style="position: relative;">
                            <center><canvas id="canvasbatterycapacity"></canvas></center>
                        </div>
                        <hr>
                        <div >
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                <select onchange="updatechartbatterycapacity()" id="selectintervalbatterycapacity" class="form-control pull-right">
                                    <option value="30">Last 30 Minutes</option>
                                    <option selected value="60">Last 60 Minutes</option>
                                    <option value="120">Last 120 Minutes</option>
                                    <option value="180">Last 180 Minutes</option>
                                </select>
                            </div>
                            
                            <div  class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                <h5 id="batterycapacity_avg"></h5>
                            </div>
                            <div  class="col-md-3 col-lg-3 col-sm-3 col-xs-3"></div>
                        </div>
                        <hr>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            // url : "<?php echo base_url() ?>data_ups/load_data_grafik",
            url : "<?php echo base_url() ?>data_ups/datagrafik/60",
		    type : "GET",
            success : function(data){
                data = JSON.parse(data)
                var id = [];
                var output_voltage = [];
                var input_voltage = [];
            
                for(var i in data) {
                    output_voltage.push(data[i].output_voltage);
                    input_voltage.push(data[i].input_voltage);
                    id.push(data[i].id);
                }
                
                var total_input = 0;
                var total_output = 0;
                for(var i = 0; i < input_voltage.length; i++) {
                    total_input += input_voltage[i];
                    total_output += output_voltage[i];
                }
                var input_avg = Math.ceil(total_input / input_voltage.length);
                var max_input = Math.max.apply(null, input_voltage);
                var min_input = Math.min.apply(null, input_voltage);


                var output_avg = Math.ceil(total_output / output_voltage.length);
                var max_output = Math.max.apply(null, output_voltage);
                var min_output = Math.min.apply(null, output_voltage);
                
                $('#input_avg').text('Input : '+input_avg+'/'+max_input+'/'+min_input+' Volt');
                $('#output_avg').text('Output : '+output_avg+'/'+max_output+'/'+min_output+' Volt');
                // console.log(max_input);

                var chartdata = {
				    labels: id,
				    datasets: [
                        {
                            label: "Input",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.75)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            data: input_voltage,
                            
                        },
                        {
                            label: "Output",
                            fill: true,
                            lineTension: 0.1,
                            backgroundColor: "rgba(29, 202, 255, 0.75)",
                            borderColor: "rgba(29, 202, 255, 1)",
                            pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
                            pointHoverBorderColor: "rgba(29, 202, 255, 1)",
                            data: output_voltage
                        }
                    ]
                };
                var ctx = $("#mycanvas");
                var LineGraph = new Chart(ctx, {
                    type: 'line',
                    data: chartdata,
                    options: {
                        responsive: true,
                        title: {
                        display: true,
                        text: 'Input & Output Voltage UPS'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    suggestedMin: 0,
                                    suggestedMax: 240,
                                    stepSize: 20
                                }
                            }]
                        }
                    }
                });


            }
        });

        $.ajax({
            // url : "<?php echo base_url() ?>data_ups/load_data_grafik",
            url : "<?php echo base_url() ?>data_ups/datagrafikload/60",
		    type : "GET",
            success : function(data){
                data = JSON.parse(data)
                var id = [];
                var load_capacity = [];
                var battery_runtime = [];
                var battery_capacity = [];
                var battery_voltage = [];
            
                for(var i in data) {
                    load_capacity.push(data[i].load_capacity);
                    battery_runtime.push(data[i].battery_runtime);
                    battery_capacity.push(data[i].battery_capacity);
                    battery_voltage.push(data[i].battery_voltage);
                    id.push(data[i].id);
                }
                
                console.log(battery_voltage);
                
                var total_load_capacity = 0;
                var total_battery_runtime = 0;
                var total_battery_capacity = 0;
                var total_battery_voltage = 0;


                for(var i = 0; i < load_capacity.length; i++) {
                    total_load_capacity += load_capacity[i];
                    total_battery_runtime += battery_runtime[i];
                    total_battery_capacity += battery_capacity[i];
                    total_battery_voltage += battery_voltage[i];
                }


                var avg_load = Math.ceil(total_load_capacity / load_capacity.length);
                var max_load = Math.max.apply(null, load_capacity);
                var min_load = Math.min.apply(null, load_capacity);


                var avg_runtime = Math.ceil(total_battery_runtime / battery_runtime.length);
                var max_runtime = Math.max.apply(null, battery_runtime);
                var min_runtime = Math.min.apply(null, battery_runtime);

                var avg_battery_capacity = Math.ceil(total_battery_capacity / battery_capacity.length);
                var max_battery_capacity = Math.max.apply(null, battery_capacity);
                var min_battery_capacity = Math.min.apply(null, battery_capacity);

                var avg_battery_voltage = Math.ceil(total_battery_voltage / battery_voltage.length);
                var max_battery_voltage = Math.max.apply(null, battery_voltage);
                var min_battery_voltage = Math.min.apply(null, battery_voltage);
                
                $('#load_avg').text('Load : '+avg_load+'/'+max_load+'/'+min_load+' % Avg/Max/Min');
                $('#runtime_avg').text('Runtime : '+avg_runtime+'/'+max_runtime+'/'+min_runtime+'  Minutes  Avg/Max/Min');
                $('#batterycapacity_avg').text('Battery Capacity : '+avg_battery_capacity+'/'+max_battery_capacity+'/'+min_battery_capacity+'  %  Avg/Max/Min');
                var chartdata_load = {
				    labels: id,
				    datasets: [
                        {
                            label: "Load Capacity",
                            fill: true,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.75)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            data: load_capacity,
                            
                        }
                    ]
                };

                var chartdata_runtime = {
				    labels: id,
				    datasets: [
                        {
                            label: "Load Capacity",
                            fill: true,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.75)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            data: battery_runtime,
                            
                        }
                    ]
                };

                var chartdata_batterycapacity = {
				    labels: id,
				    datasets: [
                        {
                            label: "Battery Capacity",
                            fill: true,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.75)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            data: battery_capacity,
                            
                        }
                    ]
                };


                var ctx = $("#canvasload");
                var ctx_runtime = $("#canvasruntime");
                var ctx_batterycapacity = $("#canvasbatterycapacity");
                var LineGraph_load = new Chart(ctx, {
                    type: 'line',
                    data: chartdata_load,
                    options: {
                        responsive: true,
                        title: {
                        display: true,
                        text: 'UPS Load (%)'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    suggestedMin: 0,
                                    suggestedMax: 30,
                                    stepSize: 2
                                }
                            }]
                        }
                    }
                });
                var LineGraph_runtime = new Chart(ctx_runtime, {
                    type: 'line',
                    data: chartdata_runtime,
                    options: {
                        responsive: true,
                        title: {
                        display: true,
                        text: 'UPS Runtime (Minutes)'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    suggestedMin: 0,
                                    suggestedMax: 160,
                                    stepSize: 10
                                }
                            }]
                        }
                    }
                });
                var LineGraph_batterycapacity = new Chart(ctx_batterycapacity, {
                    type: 'line',
                    data: chartdata_batterycapacity,
                    options: {
                        responsive: true,
                        title: {
                        display: true,
                        text: 'UPS Battery Capacity (%)'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    suggestedMin: 0,
                                    suggestedMax: 100,
                                    stepSize: 10
                                }
                            }]
                        }
                    }
                });


            }
        });
    });
    
    function updatechart() {
        var interval = $('#selectinterval option:selected').val();
        console.log("interval grafik input & output = "+interval);     
        $.ajax({
            // url : "<?php echo base_url() ?>data_ups/load_data_grafik",
            url : "<?php echo base_url() ?>data_ups/datagrafik/"+interval,
		    type : "GET",
            success : function(data){
                $("#div_chart").html("");
                $("#div_chart").html("<center><canvas id='mycanvas'> </canvas></center>");
                
                data = JSON.parse(data)
                var id = [];
                var output_voltage = [];
                var input_voltage = [];
                
                // console.log(data);


                for(var i in data) {
                    output_voltage.push(data[i].output_voltage);
                    input_voltage.push(data[i].input_voltage);
                    id.push(data[i].id);
                }

                var total_input = 0;
                var total_output = 0;
                for(var i = 0; i < input_voltage.length; i++) {
                    total_input += input_voltage[i];
                    total_output += output_voltage[i];
                }
                var input_avg = Math.ceil(total_input / input_voltage.length);
                var max_input = Math.max.apply(null, input_voltage);
                var min_input = Math.min.apply(null, input_voltage);

                var output_avg = Math.ceil(total_output / output_voltage.length);
                var max_output = Math.max.apply(null, output_voltage);
                var min_output = Math.min.apply(null, output_voltage);
                $('#input_avg').text('Input : '+input_avg+'/'+max_input+'/'+min_input+' Volt');
                $('#output_avg').text('Output : '+output_avg+'/'+max_output+'/'+min_output+' Volt');
               
                var chartdata = {
				    labels: id,
				    datasets: [
                        {
                            label: "Input",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.75)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            data: input_voltage,
                            
                        },
                        {
                            label: "Output",
                            fill: true,
                            lineTension: 0.1,
                            backgroundColor: "rgba(29, 202, 255, 0.75)",
                            borderColor: "rgba(29, 202, 255, 1)",
                            pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
                            pointHoverBorderColor: "rgba(29, 202, 255, 1)",
                            data: output_voltage
                        }
                    ]
                };
                var ctx = $("#mycanvas");
                var LineGraph = new Chart(ctx, {
                    type: 'line',
                    data: chartdata,
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Input & Output Voltage UPS'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    suggestedMin: 0,
                                    suggestedMax: 240,
                                    stepSize: 20
                                }
                            }]
                        }
                    }
                });
                

            }
        });
    }
    
    function updatechartload() {
        var interval = $('#selectintervalload option:selected').val();
        console.log("interval grafik load = "+interval);     
        $.ajax({
            url : "<?php echo base_url() ?>data_ups/datagrafikload/"+interval,
		    type : "GET",
            success : function(data){
                $("#div_chart_load").html("");
                $("#div_chart_load").html("<center><canvas id='canvasload'> </canvas></center>");
                
                data = JSON.parse(data)
                var id = [];
                var load_capacity = [];
            
                for(var i in data) {
                    load_capacity.push(data[i].load_capacity);
                    id.push(data[i].id);
                }
                var total_load_capacity = 0;
                for(var i = 0; i < load_capacity.length; i++) {
                    total_load_capacity += load_capacity[i];
                }
                var avg_load = Math.ceil(total_load_capacity / load_capacity.length);
                var max_load = Math.max.apply(null, load_capacity);
                var min_load = Math.min.apply(null, load_capacity);
                $('#load_avg').text('Load : '+avg_load+'/'+max_load+'/'+min_load+' % Avg/Max/Min');
            
                var chartdata_load = {
				    labels: id,
				    datasets: [
                        {
                            label: "Load Capacity",
                            fill: true,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.75)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            data: load_capacity,
                            
                        }
                    ]
                };
                var ctx = $("#canvasload");
                var LineGraph_load = new Chart(ctx, {
                    type: 'line',
                    data: chartdata_load,
                    options: {
                        responsive: true,
                        title: {
                        display: true,
                        text: 'UPS Load (%)'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    suggestedMin: 0,
                                    suggestedMax: 30,
                                    stepSize: 2
                                }
                            }]
                        }
                    }
                });

            }
        });
    }

    function updatechartruntime() {
        var interval = $('#selectintervalruntime option:selected').val();
        console.log("interval grafik load = "+interval);     
        $.ajax({
            url : "<?php echo base_url() ?>data_ups/datagrafikload/"+interval,
		    type : "GET",
            success : function(data){
                $("#div_chart_runtime").html("");
                $("#div_chart_runtime").html("<center><canvas id='canvasruntime'> </canvas></center>");
                
                data = JSON.parse(data)
                var id = [];
                var battery_runtime = [];
            
                for(var i in data) {
                    battery_runtime.push(data[i].battery_runtime);
                    id.push(data[i].id);
                }
                var total_runtime = 0;
                for(var i = 0; i < battery_runtime.length; i++) {
                    total_runtime += battery_runtime[i];
                }
                var avg_runtime = Math.ceil(total_runtime / battery_runtime.length);
                var max_runtime = Math.max.apply(null, battery_runtime);
                var min_runtime = Math.min.apply(null, battery_runtime);
                $('#runtime_avg').text('Runtime : '+avg_runtime+'/'+max_runtime+'/'+min_runtime+'  Minutes  Avg/Max/Min');
            
                var chartdata_runtime = {
				    labels: id,
				    datasets: [
                        {
                            label: "Runtime UPS (Minutes)",
                            fill: true,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.75)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            data: battery_runtime,
                            
                        }
                    ]
                };
                var ctx = $("#canvasruntime");
                var LineGraph_runtime = new Chart(ctx, {
                    type: 'line',
                    data: chartdata_runtime,
                    options: {
                        responsive: true,
                        title: {
                        display: true,
                        text: 'UPS Runtime (Minutes)'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    suggestedMin: 0,
                                    suggestedMax: 160,
                                    stepSize: 10
                                }
                            }]
                        }
                    }
                });

            }
        });
    }

    function updatechartbatterycapacity() {
        var interval = $('#selectintervalbatterycapacity option:selected').val();
        console.log("interval grafik load = "+interval);     
        $.ajax({
            url : "<?php echo base_url() ?>data_ups/datagrafikload/"+interval,
		    type : "GET",
            success : function(data){
                $("#div_chart_batterycapacity").html("");
                $("#div_chart_batterycapacity").html("<center><canvas id='canvasbatterycapacity'> </canvas></center>");
                
                data = JSON.parse(data)
                var id = [];
                var battery_capacity = [];
            
                for(var i in data) {
                    battery_capacity.push(data[i].battery_capacity);
                    id.push(data[i].id);
                }
                var total_batterycapacity = 0;
                for(var i = 0; i < battery_capacity.length; i++) {
                    total_batterycapacity += battery_capacity[i];
                }
                var avg_batterycapacity = Math.ceil(total_batterycapacity / battery_capacity.length);
                var max_batterycapacity = Math.max.apply(null, battery_capacity);
                var min_batterycapacity = Math.min.apply(null, battery_capacity);
                $('#batterycapacity_avg').text('Battery Capacity : '+avg_batterycapacity+'/'+max_batterycapacity+'/'+min_batterycapacity+'  %  Avg/Max/Min');
            
                var chartdata_batterycapacity = {
				    labels: id,
				    datasets: [
                        {
                            label: "Battery Capacity",
                            fill: true,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.75)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            data: battery_capacity,
                            
                        }
                    ]
                };
                var ctx_batterycapacity = $("#canvasbatterycapacity");
                var LineGraph_batterycapacity = new Chart(ctx_batterycapacity, {
                    type: 'line',
                    data: chartdata_batterycapacity,
                    options: {
                        responsive: true,
                        title: {
                        display: true,
                        text: 'UPS Battery Capacity (%)'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    suggestedMin: 0,
                                    suggestedMax: 100,
                                    stepSize: 10
                                }
                            }]
                        }
                    }
                });

            }
        });
    }
</script>