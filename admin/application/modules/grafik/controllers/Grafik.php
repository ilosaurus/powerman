<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('MENU_AKTIF', 'server');

class Grafik extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		// $this->require_login(TRUE);
	}

	public function index()
	{
		$data2['active_nav'] =  "grafik";
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_grafik');
		$this->load->view('footer');
	}


}
