
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Server Category</h2>
                    </div>
                    <style>
                        thead, th, tr, tbody {text-align: center;}
                    </style>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <center><th class="center">No</th></center>
                                        <center><th>Server Category</th></center>
                                        <center><th>Server Category Delay</th></center>
                                        <center><th>Aksi</th></center>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $count = 1;
                                        foreach ($server_category as $data ) {
                                    ?>
                                    <tr class="center">
                                        <center><td><?php echo $count ?></td></center>
                                        <center><td ><?php echo $data->category ?> </td></center>
                                        <center><td ><?php echo $data->delay ?> Second</td></center>
                                        <center>
                                            <td>
                                                <button type="button" onClick="modal_detail(<?php echo $count ?>)" class="btn bg-cyan btn-block btn-xs waves-effect">Edit</button>
                                            </td>
                                        </center>
                                    </tr>
                                    <input type="hidden" id="id_<?php  echo $count ?>" value="<?php  echo $data->id ?>">
                                    <input type="hidden" id="cat_<?php  echo $count ?>" value="<?php  echo $data->category ?>">
                                    <input type="hidden" id="delay_<?php  echo $count ?>" value="<?php  echo $data->delay ?>">
                                    <?php
                                        $count++; }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>


<!-- Modal Section  -->
<!-- Default Size -->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="defaultModalLabel">Parameter Poweroff Detail</h4>
         </div>
         <hr>
         <div class="modal-body">
            <form action="<?php echo base_url() ?>admin/powercontrol/edit_parameter" method="post">
                <label >Server Category</label>
                <div class="form-group">
                    <div class="form-line">
                        <input style="background-color:#fcfcfc" type="text" id="modal_category" class="form-control" disabled readonly value="">
                        <input type="hidden" id="modal_id_category" name="id_category" class="form-control" value="">
                    </div>
                </div>
                <label >Server Category Delay  (Second) </label> 
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="modal_delay_category" autofocus name="delay_category" class="form-control" value="">
                    </div>
                </div>
            </form>
         </div>
         <div class="modal-footer">
            <a id="modal_button_edit" onClick="send_data()"  type="button" class="btn btn-link waves-effect" href="#">UPDATE</a>
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal Section -->

<script type="text/javascript">
function modal_detail(count) {
    var id = $("#id_"+count).val();
    var cat = $("#cat_"+count).val();
    var delay = $("#delay_"+count).val();

    $("#modal_id_category").val(id);
    $("#modal_category").val(cat);
    $("#modal_delay_category").val(delay);

    $('#modal_detail').modal('show');
}

function send_data() {
    $('#modal_detail').modal('hide');
    var id_category =  $("#modal_id_category").val();
    var delay_category = $("#modal_delay_category").val();
    $.ajax({
        url : "<?php echo base_url() ?>powercontrol/categoryupdate/",
        type : "POST",
        data : {
            id_category : id_category,
            delay_category : delay_category
        },
        beforeSend : function () {
            
            swal({
                title: "Harap Tunggu",
                type: "info",
                showCancelButton: false,
                closeOnConfirm: false,
            });
        },
        success : function(data){
            console.log("Success : "+data);
            data_array = data.split("|");
            swal({
                title: "Update Data Success",
                type: ""+data_array[0]+""
            },
            function() {
                location.reload();
            }
            );
        },
        error : function () {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
    });    
}
</script>


