
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Setting Parameter Power Control</h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                                <form method="post" action="<?php echo base_url() ?>powercontrol/settingparameter">
                                <label >Parameter</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select onchange="get_data_parameter(this.value)" name="parameter" class="form-control show-tick">
                                                <?php foreach ($data_parameter as $data_parameter) { 
                                                    if ($data_parameter->id == $data_parameter_aktif[0]->id) {
                                                       $aktif = "selected";
                                                    } else {
                                                        $aktif = "";
                                                    }
                                                    
                                                ?> 
                                                    <option  <?php echo $aktif ?> value="<?php echo $data_parameter->id ?>"><?php echo $data_parameter->parameter ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <input id="id" name="id" type="hidden" value="<?php echo $data_parameter_aktif[0]->id ?>">
                                    
                                    <label>Kode Parameter</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                           <input id="kode" type="text" readonly disabled name="kode" value="<?php echo $data_parameter_aktif[0]->kode ?>" class="form-control" placeholder="Enter your email address">
                                        </div>
                                    </div>

                                    <label>Deskripsi Parameter</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea id="desc"  name="desc" readonly disabled class="form-control" ><?php  echo $data_parameter_aktif[0]->desc ?></textarea>
                                        </div>
                                    </div>

                                    <label>Value Parameter</label>                          
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input  id="value" type="text" name="value" value="<?php echo $data_parameter_aktif[0]->value ?>" class="form-control" placeholder="Enter your email address">
                                        </div>
                                    </div>            
                                    <br>
                                    <input  type="submit" name="submit" value="Submit" class="btn btn-primary m-t-15 waves-effect form-control">
                                    <!-- <button type="button" class="btn btn-primary m-t-15 waves-effect">LOGIN</button> -->
                                    
                                </form>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                                <p>Catatan</p>
                                <hr>
                            </div>
                        </div>
                            
                    </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>
<script type="text/javascript">
    $("#value").numeric();
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": true,
            "iDisplayLength": 50
        });

        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });

    function get_data_parameter(id) {
        // alert(id);
        $.ajax({
            url : "<?php echo base_url() ?>powercontrol/get_data_parameter",
            type : "post",
            data : {
				id : id
			},
            success : function(response){
                if (response != null) {
                    var data_array = response.split("|");
                    var id_parameter = data_array[0];
                    var nama_parameter = data_array[1];
                    var kode_parameter = data_array[2];
                    var value_parameter = data_array[3];
                    var desc_parameter = data_array[4];
                    var status_parameter = data_array[5];
                    $("#value").val(value_parameter);
                    $("#kode").val(kode_parameter);
                    $("#desc").val(desc_parameter);
                    $("#id").val(id_parameter);
                    // console.info(value_parameter);
                } else {
                    console.log("ndada data");
                }
                
            }
        });
    }

</script>


