<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('MENU_AKTIF', 'server');

class Powercontrol extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->require_login(TRUE);
	}

	public function index()
	{
		$data['data_server'] = $this->M_crud->get_data('server');
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_index', $data);
		$this->load->view('footer');
	}

	public function method()
	{
		$data2['active_nav'] =  "method";
		$data['data_method'] = $this->M_crud->get_data('method_poweron');
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_method', $data);
		$this->load->view('footer');
	}

	public function category()
	{
		$data2['active_nav'] =  "cat";
		$data['server_category'] = $this->M_crud->get_data('server_category');
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_server_category', $data);
		$this->load->view('footer');
	}

	public function categoryupdate()
	{
		if ($this->input->post('id_category')) {
			$id_update = $this->input->post('id_category');
			$delay = $this->input->post('delay_category');
			// $data_post = array('value' => $value,'id' => $id_update);
			$where_id = array(array('id', $id_update));
			$data = array( 'delay' => $delay );
			$this->M_crud->update_data('server_category', $data, $where_id);
			echo "success|Update Data Success";
		}else{
			echo "error|Update Data Error";
		}
	}

	public function parameteroff()
	{
		$data2['active_nav'] =  "par_off";
		$data['data_parameter'] = $this->M_crud->get_data('parameter_poweron');
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_parameteroff', $data);
		$this->load->view('footer');
	}

	public function parameteron()
	{
		$data2['active_nav'] =  "par_on";
		$data['data_parameter'] = $this->M_crud->get_data('parameter_poweron');
		$this->load->view('header');
		$this->load->view('sidebar', $data2);
		$this->load->view('V_parameteron', $data);
		$this->load->view('footer');
	}

	public function update_data_par_poweroff()
	{
		if ($this->input->post('id_parameter')) {
			$data_parameter = $this->M_crud->get_data('parameter_poweron');

			$id_update = $this->input->post('id_parameter');
			$value = $this->input->post('value_parameter');
			$data_post = array('value' => $value,'id' => $id_update);
			$where = array(array('id', $id_update));
			foreach ($data_parameter as $key) {

				if ($id_update == $key->id) {
					$status = "1";
					$value_baru =  $value;
				} else {
					$status = "0";
					$value_baru = $key->value;
				}

				$where_id = array(
					array('id', $key->id)
				);
				
				$data = array(
					'value' => $value_baru,
					'status' => $status
				);
				// echo '<pre>',print_r($data),'</pre>';
				// echo "<br>";
				$this->M_crud->update_data('parameter_poweron', $data, $where_id);
			}

			echo "success|Update Data Success";
		}else{
			echo "error|Update Data Error";
		}
	}

	public function update_data_par_poweron()
	{
		if ($this->input->post('id_parameter')) {
			$data_parameter = $this->M_crud->get_data('parameter_poweron');
			$id_update = $this->input->post('id_parameter');
			$value = $this->input->post('value_parameter');
			$data_post = array('value' => $value,'id' => $id_update);
			$where = array(array('id', $id_update));
			foreach ($data_parameter as $key) {

				if ($id_update == $key->id) {
					$status = "1";
					$value_baru =  $value;
				} else {
					$status = "0";
					$value_baru = $key->value_on;
				}

				$where_id = array(
					array('id', $key->id)
				);
				
				$data = array(
					'value_on' => $value_baru,
					'status_on' => $status
				);
				// echo '<pre>',print_r($data),'</pre>';
				// echo "<br>";
				$this->M_crud->update_data('parameter_poweron', $data, $where_id);
			}

			echo "success|Update Data Success";
		}else{
			echo "error|Update Data Error";
		}
	}

	

	public function settingparameter()
	{
		if ($this->input->post('submit')) {
			$data_parameter = $this->M_crud->get_data('parameter_poweron');
			$id_update = $this->input->post('id');
			$kode = $this->input->post('kode');
			$parameter = $this->input->post('parameter');
			$value = $this->input->post('value');
			$desc = $this->input->post('desc');

			$data_post = array(
				'value' => $value,
				'id' => $id_update
			);

			$where = array(
				array('id', $id_update)
			);

			foreach ($data_parameter as $key) {

				if ($id_update == $key->id) {
					$status = "1";
					$value_baru =  $value;
				} else {
					$status = "0";
					$value_baru = $key->value;
				}

				$where_id = array(
					array('id', $key->id)
				);
				
				$data = array(
					'value' => $value_baru,
					'status' => $status
				);
				// echo '<pre>',print_r($data),'</pre>';
				// echo "<br>";
				$this->M_crud->update_data('parameter_poweron', $data, $where_id);
			}
			
			// echo '<pre>',print_r($data_post),'</pre>';
			// $this->M_crud->update_data('parameter_poweron', $data, $where);
			redirect(base_url()."powercontrol/settingparameter");
		} else {
			$where = array(array('status','1'));
			$data['data_parameter'] = $this->M_crud->get_data('parameter_poweron');
			$data['data_parameter_aktif'] = $this->M_crud->get_data('parameter_poweron', $where);
			$this->load->view('header');
			$this->load->view('sidebar');
			$this->load->view('V_setting_parameter', $data);
			$this->load->view('footer');
		}
		
		
	}

	public function settingurutan()
	{
		$data['data_urutan'] = $this->M_crud->get_data('sop_urutan');
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_setting_urutan', $data);
		$this->load->view('footer');
	}

	//////////////////// ajax section ////////////////////////////////////
	public function get_data_parameter()
	{
		$id = $this->input->post('id');
		$where = array(array('id',$id));
		$data_parameter = $this->M_crud->get_data('parameter_poweron', $where);
		echo $data_parameter[0]->id."|".$data_parameter[0]->parameter."|".$data_parameter[0]->kode."|".$data_parameter[0]->value."|".$data_parameter[0]->desc."|".$data_parameter[0]->status;
		// print_r($data_parameter);
	}


	public function monitor($host)
	{
		// $host = '';
		// if (isset($_GET['host'])) {
		//   $host = trim($_GET['host']);
		// }

		$data = '';
		if ($host) {
		  $host = preg_replace('/[^\w-_\.]/', '', $host);
		  if ($host) {
			$data = shell_exec('ping  '.$host);
		  }
		}

		header("Access-Control-Allow-Origin: *");  
		header('Content-Type: application/json');
		die(json_encode($data));  
	}





	//////////////////// ajax section ////////////////////////////////////

}
