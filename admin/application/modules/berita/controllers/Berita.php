<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('MENU_AKTIF', 'berita');

class berita extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->load->library('pagination');
	}

	public function index($page = null)
	{
		if ($page == null) {
			$page = 0;
		}

		$where = array(array('status','1'));

		$total_berita = $this->M_crud->cek_data('man_berita',$where); //ambil total berita
		$limit_per_page = 2;
		$offset = $page;

		$config["base_url"] = base_url().'/berita/index';
		$config["total_rows"] = $total_berita;
		$config["per_page"] = $limit_per_page;
		$config['full_tag_open'] = '<ul class="pagination pull-left">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'prev';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$order_by = array('upload_date', 'desc');
		$data['modul'] = 'Berita';
		$data['data_berita'] = $this->M_crud->get_data('man_berita',$where, $order_by,null, $limit_per_page,$offset );
		$data['sidebar'] = $this->load->view('V_sidebar', NULL, TRUE);

		//print_r($data2['data2']);
		//print("<pre>");print_r($data2['data2']);print("</pre>");

		$this->load->view('header');
		$this->load->view('V_berita',$data);
		$this->load->view('footer');

	}


	public function read($id = null)
	{
		$id = $this->Crypt->de($id);
		$where = array(array('id',$id));
		$cek_id = $this->M_crud->cek_data('man_berita', $where);
		if ($cek_id) {
			$data['modul'] = 'Berita';
			$data['data_berita'] =  $this->M_crud->get_data('man_berita', $where);
			$data['sidebar'] = $this->load->view('V_sidebar', NULL, TRUE);
			$this->load->view('header');
			$this->load->view('V_berita_read', $data);
			$this->load->view('footer');
		}else {
			redirect(base_url().'error','refresh');
		}

	}




}
