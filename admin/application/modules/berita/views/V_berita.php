<br>
<!-- Page-Head -->
<div class="page-head">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3><?php echo ucwords($modul); ?></h3>
			</div>
			<div class="col-md-6">
				<ol class="breadcrumb">
					<li>Perpustakaan B.J. Habibie </li>
					<li class="active"><a href="#"><?php echo ucwords($modul); ?></a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<!-- Page-Head -->
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-8 no-padding">
			<?php foreach ($data_berita as $data_berita) {  ?>
				<article class="post col-md-12">
					<div class="post-thumb">
						<img class="img-responsive" src="<?php echo base_url().'admin/uploads/gambar_sampul/'. $data_berita->gambar_sampul; ?>" alt=""/>
						<div class="shadow-left-big"></div>
					</div>

					<style media="screen">
					#titleberita{color: #444444 !important;}
					</style>
					<div class="post-info">
						<h3><a id="titleberita" href="<?php echo base_url() ?>berita/read/<?php echo $this->Crypt->en($data_berita->id) ?>"><?php echo ucwords($data_berita->judul) ?></a></h3>
						<p id="deskripsi_berita" align="justify"><?php $text = character_limiter($data_berita->deskripsi, 450); echo strip_tags($text);?></p>
					</div>

					<?php
						$where = array(array('id',$data_berita->id_ref_kat_berita));
						$kategori = $this->M_crud->get_row('ref_kat_berita','kat_berita',$where);
						$waktu = $data_berita->upload_date;
					?>
					<div class="post-meta">
						<div class="meta-right">
							<div class="meta-info">
								<span>Oleh <a href="#">Admin</a></span> <em>/</em>
								<span><a href="#">Kategori <?php echo $kategori ?></a></span> <em>/</em>
								<span><?php  echo $this->tanggal_indo(date('Y-m-d', strtotime($waktu))); ?> </span>
							</div>
							<div class="post-more">
                <a href="<?php echo base_url() ?>berita/read/<?php echo $this->Crypt->en($data_berita->id) ?>">Selengkapnya &rarr;</a>
							</div>
						</div>
				</div>
			</article>
			<?php } ?>

		</div>

		<?php
			echo $this->view('V_sidebar');
		?>

	</div>

  <?php
    echo $this->pagination->create_links();
  ?>
</div>
