<br>
<!-- Page-Head -->
<div class="page-head">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3><?php echo $modul; ?></h3>
			</div>
			<div class="col-md-6">
				<ol class="breadcrumb">
					<li>Perpustakaan B.J. Habibie </li>
					<li class="active"><a href="#"><?php echo $modul; ?></a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<!-- Page-Head -->
<br>
<!-- Blogpost -->
<div class="container">
    <div class="row">
        <div class="col-md-8 no-padding">
          <?php foreach ($data_berita as $data_berita) { ?>
            <article class="post col-md-12">
                <div class="content-img">
                    <img src="<?php echo base_url().'admin/uploads/gambar_sampul/'. $data_berita->gambar_sampul; ?>" class="img-responsive"  alt=""/>
                    <div class="shadow-left-big"></div>
                </div>
                <div class="post-info">
                    <h3><?php echo ucwords($data_berita->judul) ?></h3>
                    <hr>
                    <div class="deskripsi">
                      <?php echo $data_berita->deskripsi; ?>
                    </div>
                </div>
								<?php
									$where = array(array('id',$data_berita->id_ref_kat_berita));
									$kategori = $this->M_crud->get_row('ref_kat_berita','kat_berita',$where);
								?>
                <div class="post-meta">
                    <div class="meta-right">
                        <div class="meta-info">
                            <span>Oleh <a href="#">Admin</a></span> <em>/</em>
                            <span><a href="#">Kategori</a> <?php echo $kategori; ?></span> <em>/</em>
                            <span><a href="#"><?php  echo $this->tanggal_indo(date('Y-m-d', strtotime($data_berita->upload_date))); ?></a></span>
                        </div>
                    </div>
                </div>

            </article>

            <?php } ?>
        </div>

				<?php
					echo $this->view('V_sidebar');
				?>


    </div>
</div>
<!-- Blogpost -->
