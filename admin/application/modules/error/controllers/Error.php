<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Error extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		//$this->require_login(TRUE);
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('V_error');
		$this->load->view('footer');
	}

}
