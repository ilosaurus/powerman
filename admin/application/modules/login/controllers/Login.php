<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('TABEL', 'admin');

class Login extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$ses = $this->session->userdata('admin_powerman');
		if($ses){
			redirect(base_url().'home');
		}
	}

	public function index()
	{
		$submit = $this->input->post('submit');
		if($submit){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			//echo $username." - ".$password;
			$where = array(
					array('username', $username),
					array('and', 'password', md5($password))
					
			);
			$cek = $this->M_crud->cek_data(TABEL, $where);
			//echo $cek;
			if($cek == '1'){
				$data_user = $this->M_crud->get_data(TABEL, $where);
				foreach($data_user as $data_user){
					$sess_array = array(
						'username' => $data_user->username,
						'name' => $data_user->name,
						'email' => $data_user->email,
						'phone' => $data_user->phone,
						'status' => $data_user->status
					);
					$this->session->set_userdata('admin_powerman', $sess_array);
					redirect(base_url().'home', 'refresh');
				}
			}else{
				$data['error'] = TRUE;
				$this->load->view('V_login', $data);
			}

		}else{
			$data['error'] = FALSE;
			$this->load->view('V_login', $data);
		}
	}
}
