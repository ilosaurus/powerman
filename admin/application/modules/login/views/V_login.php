<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In</title>
    <!-- <link rel="icon" href="<?php echo base_url() ?>assets/backend/favicon.ico" type="image/x-icon"> -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/backend/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/backend/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/backend/plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/backend/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
          <a>IT Center PNUP</a>
          <small>Data Center Power Control & Monitoring</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" action="<?php echo base_url() ?>login" method="POST">
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input autocomplete="off" type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input  autocomplete="off"  type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                        <?php if($error){ ?>
                          <i id="erorrMsg" style="color:red">Username/Password Salah!</i>
                        <?php } ?>
                        </div>
                        <div class="col-xs-4">
                            <input type="submit" class="btn btn-block bg-blue  waves-effect" name="submit" value="submit">
                        </div>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    
     <script src="<?php echo base_url() ?>assets/backend/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
     <script src="<?php echo base_url() ?>assets/backend/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
     <script src="<?php echo base_url() ?>assets/backend/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
     <script src="<?php echo base_url() ?>assets/backend/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
     <script src="<?php echo base_url() ?>assets/backend/js/admin.js"></script>
     <script src="<?php echo base_url() ?>assets/backend/js/pages/examples/sign-in.js"></script>
</body>

</html>