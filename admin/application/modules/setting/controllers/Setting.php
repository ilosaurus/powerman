<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('MENU_AKTIF', 'server');

class Powercontrol extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		// $this->require_login(TRUE);
	}

	public function index()
	{
		$data['data_server'] = $this->M_crud->get_data('server');
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_index', $data);
		$this->load->view('footer');
	}

	public function parameter()
	{
		$data['data_parameter'] = $this->M_crud->get_data('parameter_poweron');
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_parameter', $data);
		$this->load->view('footer');
	}

	public function method()
	{
		$data['data_method'] = $this->M_crud->get_data('method_poweron');
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_method', $data);
		$this->load->view('footer');
	}

	public function category()
	{
		$data['server_category'] = $this->M_crud->get_data('server_category');
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_server_category', $data);
		$this->load->view('footer');
	}

	public function settingparameter()
	{
		$data['data_parameter'] = $this->M_crud->get_data('parameter_poweron');
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_setting_parameter', $data);
		$this->load->view('footer');
	}

	public function settingurutan()
	{
		$data['data_urutan'] = $this->M_crud->get_data('sop_urutan');
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view('V_setting_urutan', $data);
		$this->load->view('footer');
	}


}
