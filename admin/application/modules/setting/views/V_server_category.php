
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix m-b-0 m-t-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>Server Category</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Server Category</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $count = 1;
                                        foreach ($server_category as $data ) {
                                    ?>
                                    <tr>
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $data->category ?></td>
                                        <td>Action</td>
                                    </tr>
                                    <?php
                                      $count++;  }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": true,
            "iDisplayLength": 50
        });

        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
</script>


