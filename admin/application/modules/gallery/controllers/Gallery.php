<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('text');
		$this->load->library('pagination');
	}

	public function index()
	{
		redirect(base_url().'gallery/p/1');
	}

	public function p($page = null)
	{
		if ($page == null) {$page = 0;}
		$where = array(array('status','1'));
		$total_berita = $this->M_crud->cek_data('man_gallery',$where); //ambil total berita
		$limit_per_page = 12;
		$offset = $page;

		$config["base_url"] = base_url().'/gallery/p';
  	$config["total_rows"] = $total_berita;
    $config["per_page"] = $limit_per_page;
		$config['full_tag_open'] = '<ul class="pagination pull-left">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
    $config['last_link'] = false;
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'prev';
    $config['prev_tag_open'] = '<li class="prev">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = 'next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$order_by = array('upload_date', 'desc');
		$data['modul'] = 'Gallery';
		$data['data_gallery'] = $this->M_crud->get_data('man_gallery',$where, $order_by,null, $limit_per_page,$offset );

		$this->load->view('header');
		$this->load->view('V_gallery',$data);
		$this->load->view('footer');

		// if ($url != null) {
		// 	$where = array(array('url',$url));
		// 	$cek_url = $this->M_crud->cek_data('man_gallery', $where);
		// 	if ($cek_url) {
		// 		$data['modul'] = 'gallery';
		// 		$data['title'] = $this->M_crud->get_row('man_gallery','judul', $where);
		// 		$data['waktu'] = $this->M_crud->get_row('man_gallery','upload_date', $where);
		// 		$data['data_gallery'] = $this->M_crud->get_data('man_gallery', $where);
		// 		$this->load->view('header');
		// 		$this->load->view('V_gallery', $data);
		// 		$this->load->view('recentnews');
		// 		$this->load->view('footer');
		// 	}else {
		// 		redirect(base_url().'error','refresh');
		// 	}
		// }else {
		// 	redirect(base_url().'error','refresh');
		// }
	}






}
