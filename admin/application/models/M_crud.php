<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model
{

  var $table_log = 'system_log';
  var $colum_order_log = array(null, 'log','log_type','datetime'); //set column field database for datatable orderable
  var $column_search_log = array('log','log_type','datetime'); //set column field database for datatable searchable 
  var $order_log = array('id' => 'asc'); // default order 

  private function _get_datatables_query(){
    $this->db->from($this->table_log);
    $i = 0;
    foreach ($this->column_search_log as $item) { // loop column 
      if($_POST['search']['value']){ // if datatable send POST for search
        if($i===0) // first loop
        {
            $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
            $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
            $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search_log) - 1 == $i) //last loop
            $this->db->group_end(); //close bracket
      }
      $i++;
    }
      
    if(isset($_POST['order'])) // here order processing
    {
        $this->db->order_by($this->colum_order_log[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order_log))
    {
        $order_log = $this->order_log;
        $this->db->order_by(key($order_log), $order[key($order_log)]);
    }
  }


  public function get_data($table, $where = '', $order_by = '', $group_by = '', $limit = '', $offset = '')
  {
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////

    if ($order_by) {
      $field = $order_by[0];
      $condition = $order_by[1];
      $this->db->order_by($field, $condition);
    }
    if ($group_by) {
        $this->db->group_by($group_by);
    }
    if ($limit) {
      $this->db->limit($limit);
    }
    if ($offset) {
      $this->db->offset($offset);
    }

    $query = $this->db->get($table);
		return $query->result();
  }

  public function insert_data($table, $data, $req = FALSE)
  {
    $this->db->insert($table, $data);
    if ($req) {
      return $this->db->insert_id();
    }
  }

  public function update_data($table, $data ,$where = '')
  {
    $this->db->set($data);
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////
    $this->db->update($table);
  }

  public function delete_data($table, $where = '')
  {
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////
    $this->db->delete($table);
  }

  public function cek_data($table, $where = ''){
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////
    $query = $this->db->get($table);
    return $query->num_rows();
  }

  public function get_row($tabel, $row, $where = '', $order_by = '', $group_by = '', $limit = '', $offset = ''){
    ////////////////////////////// where section ///////////////////////////////
    if ($where){
      $i = 1;
      foreach ($where as $where) {
        if ($i == 1) {
          $field = $where[0];
          $value = $where[1];
          $this->db->where($field, $value);
        }else {
          $condition = $where[0];
          $field = $where[1];
          $value = $where[2];
          if ($condition == 'and' ) {
            $this->db->where($field, $value);
          }else {
            $this->db->or_where($field, $value);
          }
        }
        $i++;
      }
    }
    ////////////////////////////// where section ///////////////////////////////
    if ($order_by) {
      $field = $order_by[0];
      $condition = $order_by[1];
      $this->db->order_by($field, $condition);
    }
    if ($group_by) {
        $this->db->group_by($group_by);
    }
    if ($limit) {
      $this->db->limit($limit);
    }
    if ($offset) {
      $this->db->offset($offset);
    }
    
    $query = $this->db->get($tabel);
    if($query->num_rows() > 0){
      return $query->row()->$row;
    }else{
      return "data tidak ada";
    }
  }

  public function query($query, $num_rows = FALSE){
    $query = $this->db->query($query);

    if($num_rows){
      return $query->num_rows();
    }else{
      return $query->result();
    }
  }

  // Custom Model Function //
  public function get_data_trafik($param)
  {
    $dari = '2018-01-06 10:00:01';
    $ke = '2018-01-06 10:30:02';
    // $this->db->where("update_date", $value);
    $this->db->select($param);
    $this->db->where('update_date >= ',$dari);
    $this->db->where('update_date <= ',$ke);
    $query = $this->db->get('data_ups');
		return $query->result();
  }

  public function get_row_order($table, $colum_order, $method, $limit) // method -> desc or asc
  {
    $this->db->order_by($colum_order, $method);
    $this->db->limit($limit);
    $query = $this->db->get($table);
		return $query->result();
  }

  public function get_last_row($table, $row, $colum_order, $method)
  {
    $limit = 1;
    // $this->db->select($param);
    $this->db->limit($limit);
    $this->db->order_by($colum_order, $method);
    $query = $this->db->get($table);
    if($query->num_rows() > 0){
      return $query->row()->$row;
    }else{
      return "data tidak ada";
    }
  }

  public function get_data_grafik_perwaktu($param, $dari, $ke)
  {

    $this->db->select($param);
    $this->db->where('update_date >= ',$dari);
    $this->db->where('update_date <= ',$ke);
    $query = $this->db->get('data_ups');
    return $query->result();
    
    // $waktu_terakhir = date( "Y-m-d H:i:s", strtotime("$data_waktu_terakhir"));
		// $waktu_lalu = date( "Y-m-d H:i:s", strtotime("$waktu_terakhir")-(60*$interval) );
  }

public function get_data_current_date($table, $row)
{
  $this->db->where('date_format('.$row.',"%Y-%m-%d")', 'CURDATE()', FALSE);
  $query = $this->db->get('data_ups');
  return $query->result();
}

public function get_data_spec_date($table, $row, $dari, $ke, $status)
{
  $this->db->where('update_date >= ',$dari);
  $this->db->where('update_date <= ',$ke);
  $this->db->where('status',$status);
  $query = $this->db->get('data_ups');
  return $query->result();
}

public function get_data_spec_date_all($table, $row, $dari, $ke)
{
  $this->db->where('update_date >= ',$dari);
  $this->db->where('update_date <= ',$ke);
  $query = $this->db->get($table);
  return $query->result();
}

public function get_data_last_week($table, $row,  $status)
{
  $query = $this->db->query("SELECT * FROM data_ups WHERE `update_date` BETWEEN NOW() - INTERVAL DAYOFWEEK(NOW())+6 DAY AND NOW() - INTERVAL DAYOFWEEK(NOW())-1 DAY and `status` = '".$status."' ");
  // $query = $this->db->get($table);
  return $query->result();
}

public function get_data_last_week_all($table, $row)
{
  $query = $this->db->query("SELECT * FROM ".$table." WHERE `update_date` BETWEEN NOW() - INTERVAL DAYOFWEEK(NOW())+6 DAY AND NOW() - INTERVAL DAYOFWEEK(NOW())-1 DAY ");
  return $query->result();
}


public function get_data_average($table, $row, $dari, $ke, $status)
{
  $this->db->select('AVG('.$row.') as average');
  $this->db->where('update_date >= ',$dari);
  $this->db->where('update_date <= ',$ke);
  $this->db->where('status',$status);
  $query = $this->db->get($table);
  return $query->result();
}

}
